(global-set-key [f10] 'compile)		;; Use <F10>-Key to compile
(global-font-lock-mode t)
(custom-set-variables
 '(compile-command "")		;; Add your desired compile command here (between double quotas)
 '(c-basic-offset 8)		;; Standart offset when pressing <tab>
 '(show-paren-mode t nil (paren))	;; enable Show-Paren-Mode
 '(transient-mark-mode t))	;; enable Transient-Mark-Mode
(custom-set-faces
  ;; Your init file should contain only one such instance.
 )

;;--- support european keys ------------------------------- 
(set-input-mode  (car (current-input-mode))
		 (nth 1 (current-input-mode))
		 0)
; `standard-display-european' is semi-obsolete and conflicts
; with multibyte characters. `set-language-environment' is
; a substitute.
; (standard-display-european t)

; don't use non-ascii (i.e. german umlauts) as word delimiter
(if (>= emacs-major-version 21)
    (progn
      (set-language-environment "Latin-9")
      (setq selection-coding-system 'compound-text-with-extensions)
    )
    (if (>= emacs-major-version 20)
        (set-language-environment "Latin-1")
        (require 'iso-syntax)))
(require 'disp-table)

;;--- redefine some keys ----------------------------------
;(global-set-key [backspace] 'backward-delete-char-untabify)
; the following line should not break delete char during incremental
; search - has this other disadvantages?
(global-set-key "\177" 'backward-delete-char-untabify)
(global-set-key [delete] 'delete-char)
(global-set-key [home] 'beginning-of-line)
(global-set-key [end] 'end-of-line)
;(global-set-key [C-home] 'beginning-of-buffer)
;(global-set-key [C-end] 'end-of-buffer)
; entries needed by XEmacs:
(global-set-key [(control home)] 'beginning-of-buffer)
(global-set-key [(control end)] 'end-of-buffer)
;;--- Names for calendar command -------------------------
(defvar calendar-day-name-array
  ["Son" "Mon" "Die" "Mit" "Don" "Fre" "Sam"])
(defvar calendar-month-name-array
  ["Januar" "Februar" "M�rz" "April" "Mai" "Juni" 
   "Juli" "August" "September" "Oktober" "November" "Dezember"])

;;--- Load mmix-mode and use it for .mms files -----------
(autoload 'mmix-mode "mmix-mode" "Major mode for editing MMIX files" t)
(setq auto-mode-alist (cons '("\\.mms" . mmix-mode)
                                  auto-mode-alist))
