(unless (package-installed-p 'evil)
  (global-unset-key (kbd "<f2>"))
  (global-unset-key (kbd "M-s"))
  (global-set-key (kbd "M-s") 'isearch-forward)
  (global-set-key (kbd "<f2>") 'helm-M-x)
  ;; (spacemacs/toggle-holy-mode-off)
  (global-set-key (kbd "C-M-f") 'sp-forward-sexp)
  (global-set-key (kbd "C-M-b") 'sp-backward-sexp)
  (global-set-key (kbd "M-,") 'evil-backward-arg)
  (global-set-key (kbd "M-.") 'evil-backward-arg)

  (global-set-key (kbd "M-k") 'next-line)
  (global-set-key (kbd "M-j") 'backward-char)
  (global-set-key (kbd "M-i") 'previous-line)
  ;; (global-set-key (kbd "M-J") 'forward-sentence)
  ;; (global-set-key (kbd "M-L") 'backward-sentence)
  (global-set-key (kbd "M-J") 'evil-join)
  (global-set-key (kbd "M-l") 'forward-char)
  (global-set-key (kbd "M-h") 'smarter-move-beginning-of-line)
  ;; (global-set-key (kbd "M-J") 'ergoemacs-backward-open-bracket)
  ;; (global-set-key (kbd "M-L") 'ergoemacs-forward-close-bracket)
  (global-set-key (kbd "M-U") 'sp-backward-sexp)
  (global-set-key (kbd "M-O") 'sp-forward-sexp)
  (global-set-key (kbd "C-M-u") 'beginning-of-defun)
  (global-set-key (kbd "C-M-o") 'end-of-defun)
  (global-set-key (kbd "M-K") 'golden-ratio-scroll-screen-up)
  (global-set-key (kbd "M-I") 'golden-ratio-scroll-screen-down)
  ;; (global-set-key (kbd "M-t") (kbd "C-g"))
  (global-set-key (kbd "M-;") 'end-of-line)
  (global-set-key (kbd "M-b") 'avy-goto-word-or-subword-1)
  (global-set-key (kbd "M-n g") 'goto-line)
  (global-set-key (kbd "M-n c") 'goto-char)
  (global-set-key (kbd "M-n n") 'next-error)
  (global-set-key (kbd "M-n p") 'previous-error)
  (global-set-key (kbd "M-n TAB") 'move-to-column)
  ;; (global-set-key (kbd "M-s s") 'isearch-forward)
  ;; (global-set-key (kbd "M-s r") 'isearch-backward)
  ;; (global-set-key (kbd "M-.") 'isearch-forward)
  ;; (global-set-key (kbd "M-,") 'isearch-backward)
  ;; (global-set-key (kbd "C-v") 'helm-show-kill-ring)
  ;; (global-set-key (kbd "M-s M-s") 'isearch-forward-symbol-at-point)
  (global-set-key (kbd "M-p") 'recenter-top-bottom)
  (global-set-key (kbd "C-s") 'save-buffer)
  (global-set-key (kbd "C-S-s") 'write-file)
  ;; (global-set-key (kbd "C-S") 'write-file)

  (global-set-key (kbd "M-/") 'hippie-expand)
  (global-set-key (kbd "<M-return>") 'open-next-line)
  (global-set-key (kbd "<M-S-return>") 'open-previous-line)
  (global-set-key (kbd "M-'") 'er/expand-region)
  (global-set-key (kbd "C-t") 'transpose-words)
  (global-set-key (kbd "M-SPC") 'set-mark-command)
  (global-set-key (kbd "M-v") 'yank)
  (global-set-key (kbd "M-V") 'evil-paste-after)
  (global-set-key (kbd "M-x") 'evil-delete)
  (global-set-key (kbd "M-X") 'evil-delete-line)
  (global-set-key (kbd "M-c") 'evil-yank)
  (global-set-key (kbd "M-w") 'evil-change)
  (global-set-key (kbd "C-/") 'zzz-to-char)
  (global-set-key (kbd "M-y") 'set-mark-command)
  (global-set-key (kbd "M-\\") 'my-snippet-expand)
  (global-set-key (kbd "M-z") 'undo)
  (global-set-key (kbd "C-r") 'redo)
  (global-set-key (kbd "M-d") 'kill-word)
  (global-set-key (kbd "M-f") 'delete-forward-char)
  (global-set-key (kbd "M-t") 'helm-M-x)
  ;; (global-set-key (kbd "M-T") 'evil-snipe-T)
  (global-set-key (kbd "M-r") 'isearch-backward)
  ;; (global-set-key (kbd "M-R") 'evil-replace-state)
  ;; (global-set-key (kbd "M-F") 'sp-kill-hybrid-sexp)
  (global-set-key (kbd "C-c ;") 'evilnc-comment-or-uncomment-lines)
  ;; (global-set-key (kbd "M-r") 'kill-word)
  (define-key isearch-mode-map (kbd "<up>") 'isearch-repeat-backward)
  (define-key isearch-mode-map (kbd "<down>") 'isearch-repeat-forward)
  (define-key isearch-mode-map (kbd "M-.") 'isearch-repeat-forward)
  (define-key isearch-mode-map (kbd "M-,") 'isearch-repeat-backward)
  (define-key emacs-lisp-mode-map (kbd "C-e") 'eval-last-sexp)

  (global-set-key (kbd "<f10>") 'terminal-here)
  ;; (global-set-key (kbd "C-o") 'jump-to-register)
  ;; (global-set-key (kbd "C-k") 'point-to-register)
  ;; (global-set-key (kbd "C-n") 'copy-to-register)
  (spacemacs/set-leader-keys
    "M-b" 'helm-mini
    "M-j" 'spacemacs/helm-jump-in-buffer
    "M-f" 'helm-find-files
    "xx" 'copy-to-register
    "xv" 'insert-register

    ;; convert edge list to graph
    "cf" 'edge-uv-to-graph
    "cg" 'edge-uvw-to-graph
    )
  (global-set-key (kbd "RET") 'my-fancy-newline))

;; (global-unset-key (kbd "C-x"))
;; (global-unset-key (kbd "C-c"))
;; (global-unset-key (kbd "C-v"))
;; (global-set-key (kbd "C-x") 'evil-delete)
;; (global-set-key (kbd "C-c") 'evil-yank)
;; (global-set-key (kbd "C-v") 'evil-paste-before)
;; (global-set-key (kbd "M-t") 'helm-M-x)
;; (global-set-key (kbd "M-v") 'yank)
;; (global-set-key (kbd "M-V") 'evil-paste-after)
;; (global-set-key (kbd "M-x") 'evil-delete)
;; (global-set-key (kbd "M-X") 'evil-delete-line)
;; (global-set-key (kbd "M-c") 'evil-yank)
(global-set-key (kbd "M-b") 'avy-goto-word-or-subword-1)
(global-set-key (kbd "M-y") 'set-mark-command)
(global-set-key (kbd "C-f") 'isearch-forward)
(define-key isearch-mode-map (kbd "C-f") 'isearch-repeat-forward)
(global-set-key (kbd "M-\\") 'my-snippet-expand)
(global-set-key (kbd "M-f") 'delete-forward-char)
(global-set-key (kbd "M-x") 'evil-delete)
(global-set-key (kbd "M-X") 'evil-delete-line)
(global-set-key (kbd "M-c") 'evil-yank)
(global-set-key (kbd "M-v") 'yank)
(global-set-key (kbd "M-V") 'evil-paste-after)
(global-set-key (kbd "M-l") 'forward-char)
(global-set-key (kbd "M-k") 'next-line)
(global-set-key (kbd "M-j") 'backward-char)
(global-set-key (kbd "M-i") 'previous-line)
(global-set-key (kbd "M-h") 'smarter-move-beginning-of-line)
(global-set-key (kbd "M-;") 'end-of-line)
(global-set-key (kbd "M-X") 'evil-delete-line)
(global-set-key (kbd "M-c") 'evil-yank)
(global-set-key (kbd "M-z") 'undo)
(global-set-key (kbd "C-r") 'redo)
(global-set-key (kbd "M-t") 'helm-M-x)
(global-set-key (kbd "M-u") 'backward-word)
(global-set-key (kbd "M-o") 'forward-word)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "M-s") 'save-buffer)
(global-set-key (kbd "<M-return>") 'evil-open-below)
(global-set-key (kbd "<M-S-return>") 'evil-open-above)
(global-set-key (kbd "<f5>") 'compile-command-auto)
(global-set-key (kbd "<f6>") 'my-recompile)
(global-set-key (kbd "<f7>") 'toggle-debug-enable)
(global-set-key (kbd "<f8>") 'copy-whole-buffer)
(global-set-key (kbd "<f9>") 'my-magit-stage-all-and-commit)
;; (global-set-key (kbd "M-[") 'evil-force-normal-state)
;; (cua-mode 1)
(define-key evil-insert-state-map (kbd "C-v") 'evil-paste-before)
(define-key evil-normal-state-map (kbd "C-v") 'evil-paste-before)

(define-key evil-insert-state-map (kbd "RET") 'my-fancy-newline)
(define-key evil-motion-state-map (kbd "y") 'evil-visual-char)
(define-key evil-motion-state-map (kbd "Y") 'evil-visual-line)
(define-key evil-motion-state-map (kbd "C-y") 'evil-visual-block)
(define-key evil-motion-state-map (kbd "i") 'evil-previous-line)
(define-key evil-motion-state-map (kbd "k") 'evil-next-line)
(define-key evil-motion-state-map (kbd "j") 'evil-backward-char)
(define-key evil-motion-state-map (kbd "l") 'evil-forward-char)
(define-key evil-motion-state-map (kbd "u") 'backward-word)
(define-key evil-motion-state-map (kbd "o") 'forward-word)
(define-key evil-motion-state-map (kbd "I") 'golden-ratio-scroll-screen-down)
(define-key evil-motion-state-map (kbd "K") 'golden-ratio-scroll-screen-up)
(define-key evil-motion-state-map (kbd "U") 'sp-backward-sexp)
(define-key evil-motion-state-map (kbd "O") 'sp-forward-sexp)
(define-key evil-motion-state-map (kbd "h") 'smarter-move-beginning-of-line)
(define-key evil-motion-state-map (kbd ";") 'evil-end-of-line)
(define-key evil-motion-state-map (kbd "\'") 'evil-repeat-find-char)
(define-key evil-motion-state-map (kbd "\\") 'evil-goto-mark-line)
;; (define-key evil-motion-state-map (kbd "w") 'sp-backward-sexp)
;; (define-key evil-motion-state-map (kbd "e") 'sp-forward-sexp)
(define-key evil-motion-state-map (kbd "b") 'evil-avy-goto-word-or-subword-1)
;; (define-key evil-motion-state-map (kbd "[[") 'beginning-of-defun)
;; (define-key evil-motion-state-map (kbd "]]") 'end-of-defun)

(define-key evil-normal-state-map (kbd "U") 'sp-backward-sexp)
(define-key evil-normal-state-map (kbd "O") 'sp-forward-sexp)
(define-key evil-normal-state-map (kbd "C-d") 'zz-scroll-half-page-down)
(define-key evil-normal-state-map (kbd "C-u") 'zz-scroll-half-page-up)
(define-key evil-normal-state-map (kbd "i") 'evil-previous-line)
(define-key evil-normal-state-map (kbd "k") 'evil-next-line)
(define-key evil-normal-state-map (kbd "j") 'evil-backward-char)
(define-key evil-normal-state-map (kbd "l") 'evil-forward-char)
(define-key evil-normal-state-map (kbd "u") 'backward-word)
(define-key evil-normal-state-map (kbd "o") 'forward-word)
(define-key evil-normal-state-map (kbd "I") 'golden-ratio-scroll-screen-down)
(define-key evil-normal-state-map (kbd "K") 'golden-ratio-scroll-screen-up)
;; (define-key evil-normal-state-map (kbd "U") 'evil-backward-WORD-begin)
;; (define-key evil-normal-state-map (kbd "O") 'evil-forward-WORD-end)
(define-key evil-normal-state-map (kbd "h") 'smarter-move-beginning-of-line)
(define-key evil-normal-state-map (kbd ";") 'evil-end-of-line)
(define-key evil-normal-state-map (kbd "\'") 'evil-repeat)
(define-key evil-normal-state-map (kbd "\\") 'evil-goto-mark-line)
(define-key evil-normal-state-map (kbd ".") 'evil-repeat-find-char)

;; (define-key evil-normal-state-map (kbd "O") 'sp-forward-sexp)
(define-key evil-normal-state-map (kbd "w") 'evil-change)
(define-key evil-normal-state-map (kbd "e") 'evil-insert)
(define-key evil-normal-state-map (kbd "z") 'undo)
(define-key evil-normal-state-map (kbd "c") 'evil-yank)
(define-key evil-normal-state-map (kbd "v") 'evil-paste-before)
(define-key evil-normal-state-map (kbd "V") 'evil-paste-after)
(define-key evil-normal-state-map (kbd "y") 'evil-visual-char)
(define-key evil-normal-state-map (kbd "Y") 'evil-visual-line)
(define-key evil-normal-state-map (kbd "C-y") 'evil-visual-block)
;; (define-key evil-normal-state-map (kbd "p") 'recenter-top-bottom)
(define-key evil-normal-state-map (kbd "b") 'evil-avy-goto-word-or-subword-1)
;; (define-key evil-normal-state-map (kbd "b") 'evil-escape)

(define-key evil-operator-state-map "e" evil-inner-text-objects-map)
(define-key evil-visual-state-map "e" evil-inner-text-objects-map)
(define-key evil-visual-state-map (kbd "i") 'evil-previous-line)
(define-key evil-visual-state-map (kbd "k") 'evil-next-line)
(define-key evil-visual-state-map (kbd "j") 'evil-backward-char)
(define-key evil-visual-state-map (kbd "l") 'evil-forward-char)
(define-key evil-visual-state-map (kbd "u") 'evil-backward-word-begin)
(define-key evil-visual-state-map (kbd "o") 'evil-forward-word-end)
(define-key evil-visual-state-map (kbd "I") 'evil-scroll-up)
(define-key evil-visual-state-map (kbd "K") 'evil-scroll-down)
;; (define-key evil-visual-state-map (kbd "U") 'evil-backward-WORD-begin)
;; (define-key evil-visual-state-map (kbd "O") 'evil-forward-WORD-end)
(define-key evil-visual-state-map (kbd "w") 'evil-change)
(define-key evil-visual-state-map (kbd "E") 'evil-insert)
(define-key evil-visual-state-map (kbd "h") 'smarter-move-beginning-of-line)
(define-key evil-visual-state-map (kbd "\'") 'evil-repeat)
(define-key evil-visual-state-map (kbd "\\") 'evil-goto-mark-line)
(define-key evil-visual-state-map (kbd ".") 'evil-repeat-find-char)
(define-key evil-visual-state-map (kbd ";") 'evil-end-of-line)
(define-key evil-visual-state-map (kbd "U") 'sp-backward-sexp)
(define-key evil-visual-state-map (kbd "O") 'sp-forward-sexp)
;; (define-key evil-visual-state-map (kbd "M-x") 'evil-delete)
(define-key evil-visual-state-map (kbd "b") 'evil-avy-goto-word-or-subword-1)

(with-eval-after-load 'helm
  (define-key helm-map (kbd "M-i") 'helm-previous-line)
  (define-key helm-map (kbd "M-k") 'helm-next-line)
  (define-key helm-map (kbd "M-I") 'helm-previous-page)
  (define-key helm-map (kbd "M-K") 'helm-next-page)
  (with-eval-after-load 'helm-files
    (define-key helm-find-files-map (kbd "M-i") 'helm-previous-line)
    (define-key helm-find-files-map (kbd "M-k") 'helm-next-line)
    (define-key helm-find-files-map (kbd "M-I") 'helm-previous-page)
    (define-key helm-find-files-map (kbd "M-K") 'helm-next-page)))

(with-eval-after-load 'compile
  (define-key compilation-mode-map (kbd "h") 'smarter-move-beginning-of-line))

(spacemacs/set-leader-keys
  "sv" 'c-jump-to-my-var-declaration)

(spacemacs/set-leader-keys-for-major-mode 'c++-mode
  "1" 'my-jump-var-declaration
  "2" 'my-jump-main-vars
  "3" 'my-jump-input
  "4" 'my-jump-solve
  "5" 'my-jump-output
  "6" 'my-jump-debug-enable
  "m1" 'my-move-var-to-global-scope
  "m2" 'my-move-line-above-function
  "m3" 'my-move-var-to-top-local-function
  "m4" 'my-move-var-to-higher-scope
  "i2[" 'insert-2sqr-brackets
  "i3[" 'insert-3sqr-brackets
  "i4[" 'insert-4sqr-brackets
  "i3c" 'insert-cin3
  "ii" 'insert-inf
  "if" 'insert-if
  "iw" 'insert-while
  "irc" 'insert-repc
  "iri" 'insert-repi
  "irj" 'insert-repj
  "irk" 'insert-repk
  "im" 'insert-c-macro
  "ia" 'insert-and-op
  "io" 'insert-or-op
  "ie" 'insert-existency-symbols
  )


(provide 'evil-ergoemacs)
