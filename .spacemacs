;; -*- mode: dotspacemacs -*-
;; This file is loaded by Spacemacs at startup.
;; It must be stored in your home directory.

(defun dotspacemacs/layers ()
  "Configuration Layers declaration."
  (setq-default
   dotspacemacs-distribution 'spacemacs
   ;; List of additional paths where to look for configuration layers.
   ;; Paths must have a trailing slash (ie. `~/.mycontribs/')
   dotspacemacs-configuration-layer-path '()
   ;; List of configuration layers to load. If it is the symbol `all' instead
   ;; of a list then all discovered layers will be installed.
   dotspacemacs-configuration-layers `((git :variables
                                            git-use-magit-next t
                                            git-enable-github-support t)
                                       osx
                                       helm
                                       ;; go
                                       ;; yaml
                                       ;; markdown
                                       ;; csv
                                       ;; html
                                       ;; asm
                                       auto-completion
                                       ;; (version-control :variables
                                       ;;                  version-control-global-margin t
                                       ;;                  version-control-diff-tool 'diff-hl)
                                       ;; graphviz
                                       ;; latex
                                       dash
                                       ;; better-defaults
                                       ;; syntax-checking
                                       javascript
                                       emacs-lisp
                                       semantic
                                       tuhdo
                                       c-c++
                                       swift
                                       python
                                       ;; d
                                       ;; nim
                                       (gtags :variables gtags-enable-by-default nil)
                                       ;; eyebrowse
                                       ;; (colors :variables
                                       ;;         colors-enable-nyan-cat-progress-bar t)
                                       themes-megapack
                                       ;; common-lisp
                                       ;; ruby
                                       ;; csharp
                                       ;; xwidget
                                       org
                                       lua
                                       ;; finance
                                       )
   ;; List of additional packages that will be installed wihout being
   ;; wrapped in a layer. If you need some configuration for these
   ;; packages then consider to create a layer, you can also put the
   ;; configuration in `dotspacemacs/config'.
   dotspacemacs-additional-packages '(doom-themes)
   ;; A list of packages and/or extensions that will not be install and loaded.
   dotspacemacs-excluded-packages `(popwin
                                    skewer-mode
                                    evil-escape
                                    ace-jump-helm-line
                                    fancy-battery
                                    ;; flx-ido
                                    git-link
                                    git-messenger
                                    git-timemachine
                                    gnuplot
                                    google-translate
                                    helm-gitignore
                                    helm-make
                                    helm-mode-manager
                                    org-journal
                                    org-present
                                    all-the-icons
                                    ace-link
                                    ace-window
                                    rainbow-delimiters
                                    rainbow-identifiers
                                    winum
                                    easy-kill
                                    disaster
                                    clang-format
                                    adaptive-wrap
                                    indent-guide
                                    link-hint
                                    org-download
                                    org-pomodoro
                                    neotree
                                    highlight-parentheses
                                    ;; eyebrowse
                                    ;; persp-mode
                                    ;; evil-anzu
                                    evil-ediff
                                    evil-exchange
                                    evil-indent-plus
                                    evil-lisp-state
                                    evil-magit
                                    evil-mc
                                    evil-numbers
                                    evil-search-highlight-persist
                                    evil-tutor
                                    ;; evil-unimpaired
                                    golden-ratio
                                    spaceline
                                    window-numbering
                                    ace-jump-mode
                                    evil-org
                                    elisp-slime-nav
                                    ;; neotree
                                    git-gutter
                                    git-gutter-fringe
                                    git-gutter+
                                    git-gutter-fringe+
                                    vi-tilde-fringe
                                    paradox
                                    helm-flx
                                    ;; ws-butler messes with tabs and spaces, sad but I have to remove
                                    ws-butler)
   ;; If non-nil spacemacs will delete any orphan packages, i.e. packages that
   ;; are declared in a layer which is not a member of
   ;; the list `dotspacemacs-configuration-layers'
   dotspacemacs-delete-orphan-packages t))

(defun dotspacemacs/init ()
  "Initialization function.
This function is called at the very startup of Spacemacs initialization
before layers configuration."
  ;; This setq-default sexp is an exhaustive list of all the supported
  ;; spacemacs settings.
  (setq-default
   ;; If non-nil ELPA repositories are contacted via HTTPS whenever it's
   ;; possible. Set it to nil if you have no way to use HTTPS in your
   ;; environment, otherwise it is strongly recommended to let it set to t.
   ;; This variable has no effect if Emacs is launched with the parameter
   ;; `--insecure' which forces the value of this variable to nil.
   ;; (default t)
   dotspacemacs-elpa-https t
   ;; Maximum allowed time in seconds to contact an ELPA repository.
   ;; (default 5)
   dotspacemacs-elpa-timeout 5
   ;; If non-nil then spacemacs will check for updates at startup
   ;; when the current branch is not `develop'. Note that checking for
   ;; new versions works via git commands, thus it calls GitHub services
   ;; whenever you start Emacs. (default nil)
   dotspacemacs-check-for-update nil
   ;; If non-nil, a form that evaluates to a package directory. For example, to
   ;; use different package directories for different Emacs versions, set this
   ;; to `emacs-version'. (default nil)
   dotspacemacs-elpa-subdirectory nil
   ;; One of `vim', `emacs' or `hybrid'.
   ;; `hybrid' is like `vim' except that `insert state' is replaced by the
   ;; `hybrid state' with `emacs' key bindings. The value can also be a list
   ;; with `:variables' keyword (similar to layers). Check the editing styles
   ;; section of the documentation for details on available variables.
   ;; (default 'vim)
   dotspacemacs-editing-style 'vim
   ;; If non-nil output loading progress in `*Messages*' buffer. (default nil)
   dotspacemacs-verbose-loading nil
   ;; Specify the startup banner. Default value is `official', it displays
   ;; the official spacemacs logo. An integer value is the index of text
   ;; banner, `random' chooses a random text banner in `core/banners'
   ;; directory. A string value must be a path to an image format supported
   ;; by your Emacs build.
   ;; If the value is nil then no banner is displayed. (default 'official)
   dotspacemacs-startup-banner 'official
   ;; List of items to show in startup buffer or an association list of
   ;; the form `(list-type . list-size)`. If nil then it is disabled.
   ;; Possible values for list-type are:
   ;; `recents' `bookmarks' `projects' `agenda' `todos'.
   ;; List sizes may be nil, in which case
   ;; `spacemacs-buffer-startup-lists-length' takes effect.
   dotspacemacs-startup-lists '((bookmarks . 5)
                                (recents . 5)
                                (projects . 7))
   ;; True if the home buffer should respond to resize events. (default t)
   dotspacemacs-startup-buffer-responsive t
   ;; Default major mode of the scratch buffer (default `text-mode')
   dotspacemacs-scratch-mode 'text-mode
   ;; List of themes, the first of the list is loaded when spacemacs starts.
   ;; Press `SPC T n' to cycle to the next theme in the list (works great
   ;; with 2 themes variants, one dark and one light)
   dotspacemacs-themes `(tango-plus)

   ;; If non nil the cursor color matches the state color.
   dotspacemacs-colorize-cursor-according-to-state t
   ;; Default font. `powerline-scale' allows to quickly tweak the mode-line
   ;; size to make separators look not too crappy.
   dotspacemacs-default-font '("Essential PragmataPro"
                               :size 14
                               :weight normal
                               :width normal
                               :powerline-scale 1.1)
   ;; The leader key (default "SPC")
   dotspacemacs-leader-key "SPC"
   ;; The leader key accessible in `emacs state' and `insert state'
   dotspacemacs-emacs-leader-key "C-c"
   ;; Major mode leader key is a shortcut key which is the equivalent of
   ;; pressing `<leader> m`. Set it to `nil` to disable it. (default ",")
   dotspacemacs-major-mode-leader-key ","
   ;; Major mode leader key accessible in `emacs state' and `insert state'.
   ;; (default "C-M-m")
   dotspacemacs-major-mode-emacs-leader-key "C-M-m"
   ;; These variables control whether separate commands are bound in the GUI to
   ;; the key pairs `C-i', `TAB' and `C-m', `RET'.
   ;; Setting it to a non-nil value, allows for separate commands under `C-i'
   ;; and TAB or `C-m' and `RET'.
   ;; In the terminal, these pairs are generally indistinguishable, so this only
   ;; works in the GUI. (default nil)
   dotspacemacs-distinguish-gui-tab nil
   ;; If non-nil `Y' is remapped to `y$' in Evil states. (default nil)
   dotspacemacs-remap-Y-to-y$ nil
   ;; If non-nil, the shift mappings `<' and `>' retain visual state if used
   ;; there. (default t)
   dotspacemacs-retain-visual-state-on-shift t
   ;; If non-nil, `J' and `K' move lines up and down when in visual mode.
   ;; (default nil)
   dotspacemacs-visual-line-move-text nil
   ;; If non-nil, inverse the meaning of `g' in `:substitute' Evil ex-command.
   ;; (default nil)
   dotspacemacs-ex-substitute-global nil
   ;; Name of the default layout (default "Default")
   dotspacemacs-default-layout-name "Default"
   ;; If non-nil the default layout name is displayed in the mode-line.
   ;; (default nil)
   dotspacemacs-display-default-layout nil
   ;; If non-nil then the last auto saved layouts are resumed automatically upon
   ;; start. (default nil)
   dotspacemacs-auto-resume-layouts nil
   ;; If non-nil, auto-generate layout name when creating new layouts. Only has
   ;; effect when using the "jump to layout by number" commands. (default nil)
   dotspacemacs-auto-generate-layout-names nil
   ;; Size (in MB) above which spacemacs will prompt to open the large file
   ;; literally to avoid performance issues. Opening a file literally means that
   ;; no major mode or minor modes are active. (default is 1)
   dotspacemacs-large-file-size 1
   ;; Location where to auto-save files. Possible values are `original' to
   ;; auto-save the file in-place, `cache' to auto-save the file to another
   ;; file stored in the cache directory and `nil' to disable auto-saving.
   ;; (default 'cache)
   dotspacemacs-auto-save-file-location 'cache
   ;; Maximum number of rollback slots to keep in the cache. (default 5)
   dotspacemacs-max-rollback-slots 5
   ;; If non-nil, `helm' will try to minimize the space it uses. (default nil)
   dotspacemacs-helm-resize nil
   ;; if non-nil, the helm header is hidden when there is only one source.
   ;; (default nil)
   dotspacemacs-helm-no-header nil
   ;; define the position to display `helm', options are `bottom', `top',
   ;; `left', or `right'. (default 'bottom)
   dotspacemacs-helm-position 'bottom
   ;; Controls fuzzy matching in helm. If set to `always', force fuzzy matching
   ;; in all non-asynchronous sources. If set to `source', preserve individual
   ;; source settings. Else, disable fuzzy matching in all sources.
   ;; (default 'always)
   dotspacemacs-helm-use-fuzzy 'always
   ;; If non-nil, the paste transient-state is enabled. While enabled, pressing
   ;; `p' several times cycles through the elements in the `kill-ring'.
   ;; (default nil)
   dotspacemacs-enable-paste-transient-state nil
   ;; Which-key delay in seconds. The which-key buffer is the popup listing
   ;; the commands bound to the current keystroke sequence. (default 0.4)
   dotspacemacs-which-key-delay 0.4
   ;; Which-key frame position. Possible values are `right', `bottom' and
   ;; `right-then-bottom'. right-then-bottom tries to display the frame to the
   ;; right; if there is insufficient space it displays it at the bottom.
   ;; (default 'bottom)
   dotspacemacs-which-key-position 'bottom
   ;; Control where `switch-to-buffer' displays the buffer. If nil,
   ;; `switch-to-buffer' displays the buffer in the current window even if
   ;; another same-purpose window is available. If non-nil, `switch-to-buffer'
   ;; displays the buffer in a same-purpose window even if the buffer can be
   ;; displayed in the current window. (default nil)
   dotspacemacs-switch-to-buffer-prefers-purpose nil
   ;; If non-nil a progress bar is displayed when spacemacs is loading. This
   ;; may increase the boot time on some systems and emacs builds, set it to
   ;; nil to boost the loading time. (default t)
   dotspacemacs-loading-progress-bar t
   ;; If non-nil the frame is fullscreen when Emacs starts up. (default nil)
   ;; (Emacs 24.4+ only)
   dotspacemacs-fullscreen-at-startup nil
   ;; If non-nil `spacemacs/toggle-fullscreen' will not use native fullscreen.
   ;; Use to disable fullscreen animations in OSX. (default nil)
   dotspacemacs-fullscreen-use-non-native nil
   ;; If non-nil the frame is maximized when Emacs starts up.
   ;; Takes effect only if `dotspacemacs-fullscreen-at-startup' is nil.
   ;; (Emacs 24.4+ only)
   dotspacemacs-maximized-at-startup nil
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's active or selected.
   ;; Transparency can be toggled through `toggle-transparency'.
   dotspacemacs-active-transparency 90
   ;; A value from the range (0..100), in increasing opacity, which describes
   ;; the transparency level of a frame when it's inactive or deselected.
   ;; Transparency can be toggled through `toggle-transparency'.
   dotspacemacs-inactive-transparency 90
   ;; If non-nil show the titles of transient states. (default t)
   dotspacemacs-show-transient-state-title t
   ;; If non-nil show the color guide hint for transient state keys. (default t)
   dotspacemacs-show-transient-state-color-guide t
   ;; If non-nil unicode symbols are displayed in the mode line. (default t)
   dotspacemacs-mode-line-unicode-symbols t
   ;; If non-nil smooth scrolling (native-scrolling) is enabled. Smooth
   ;; scrolling overrides the default behavior of Emacs which recenters point
   ;; when it reaches the top or bottom of the screen. (default t)
   dotspacemacs-smooth-scrolling t
   ;; Control line numbers activation.
   ;; If set to `t' or `relative' line numbers are turned on in all `prog-mode' and
   ;; `text-mode' derivatives. If set to `relative', line numbers are relative.
   ;; This variable can also be set to a property list for finer control:
   ;; '(:relative nil
   ;;   :disabled-for-modes dired-mode
   ;;                       doc-view-mode
   ;;                       markdown-mode
   ;;                       org-mode
   ;;                       pdf-view-mode
   ;;                       text-mode
   ;;   :size-limit-kb 1000)
   ;; (default nil)
   dotspacemacs-line-numbers nil
   ;; Code folding method. Possible values are `evil' and `origami'.
   ;; (default 'evil)
   dotspacemacs-folding-method 'evil
   ;; If non-nil `smartparens-strict-mode' will be enabled in programming modes.
   ;; (default nil)
   dotspacemacs-smartparens-strict-mode nil
   ;; If non-nil pressing the closing parenthesis `)' key in insert mode passes
   ;; over any automatically added closing parenthesis, bracket, quote, etc…
   ;; This can be temporary disabled by pressing `C-q' before `)'. (default nil)
   dotspacemacs-smart-closing-parenthesis nil
   ;; Select a scope to highlight delimiters. Possible values are `any',
   ;; `current', `all' or `nil'. Default is `all' (highlight any scope and
   ;; emphasis the current one). (default 'all)
   dotspacemacs-highlight-delimiters 'all
   ;; If non-nil, advise quit functions to keep server open when quitting.
   ;; (default nil)
   dotspacemacs-persistent-server nil
   ;; List of search tool executable names. Spacemacs uses the first installed
   ;; tool of the list. Supported tools are `rg', `ag', `pt', `ack' and `grep'.
   ;; (default '("rg" "ag" "pt" "ack" "grep"))
   dotspacemacs-search-tools '("rg" "ag" "pt" "ack" "grep")
   ;; The default package repository used if no explicit repository has been
   ;; specified with an installed package.
   ;; Not used for now. (default nil)
   dotspacemacs-default-package-repository nil
   ;; Format specification for setting the frame title.
   ;; %a - the `abbreviated-file-name', or `buffer-name'
   ;; %t - `projectile-project-name'
   ;; %I - `invocation-name'
   ;; %S - `system-name'
   ;; %U - contents of $USER
   ;; %b - buffer name
   ;; %f - visited file name
   ;; %F - frame name
   ;; %s - process status
   ;; %p - percent of buffer above top of window, or Top, Bot or All
   ;; %P - percent of buffer above bottom of window, perhaps plus Top, or Bot or All
   ;; %m - mode name
   ;; %n - Narrow if appropriate
   ;; %z - mnemonics of buffer, terminal, and keyboard coding systems
   ;; %Z - like %z, but including the end-of-line format
   ;; (default "%I@%S")
   dotspacemacs-frame-title-format "%I@%S"
   ;; Format specification for setting the icon title format
   ;; (default nil - same as frame-title-format)
   dotspacemacs-icon-title-format nil
   ;; Delete whitespace while saving buffer. Possible values are `all'
   ;; to aggressively delete empty line and long sequences of whitespace,
   ;; `trailing' to delete only the whitespace at end of lines, `changed' to
   ;; delete only whitespace for changed lines or `nil' to disable cleanup.
   ;; (default nil)
   dotspacemacs-whitespace-cleanup 'all
   )
  ;; User initialization goes here
  ;;(global-unset-key (kbd "M-g"))
  (global-unset-key (kbd "M-b"))
  (setq smartparens-strict-mode nil)
  )

(defun dotspacemacs/user-config ()
  "Configuration function.
 This function is called at the very end of Spacemacs initialization after
layers configuration."
  (setq mac-option-modifier 'meta)
  ;; (setq-default line-spacing 0.1)
  (when (eq system-type 'darwin)
    (setq mac-command-modifier 'meta)
    (setq mac-option-modifier nil))
  ;; yasnippet
  ;; (add-hook 'prog-mode-hook 'yas-minor-mode)

  ;; Isearch convenience, space matches anything (non-greedy)
  ;; (setq search-whitespace-regexp ".*?")
  ;; linum
  ;; (add-hook 'prog-mode-hook #'linum-mode)

  ;; (add-hook 'emacs-lisp-mode-hook (lambda () (semantic-mode -1)) t)

  ;; enable company globally
  ;; (global-company-mode 1)
  ;; (define-key company-active-map (kbd "C-s") 'company-filter-candidates)
  ;; (define-key company-search-map (kbd "C-n") 'company-select-next)
  ;; (define-key company-search-map (kbd "C-p") 'company-select-previous)
  (setq completion-ignore-case t
        company-show-numbers t)

  ;; enable mode for non-standard extensions
  (add-to-list 'auto-mode-alist '("\\.inc" . nasm-mode))
  
  (global-set-key (kbd "C-x K") 'kill-buffer-and-window)
  (global-set-key (kbd "C-x k") 'kill-this-buffer)

  (delete-selection-mode 1)

  (setq dired-listing-switches "-lha")

  ;; (when (string-equal spacemacs--cur-theme "plain")
  ;;   ;; (set-face-attribute 'font-lock-type-face nil :weight 'bold)
  ;;   (set-face-attribute 'font-lock-keyword-face nil :weight 'bold)
  ;;   (set-face-foreground 'font-lock-comment-face "grey60")
  ;;   (set-face-foreground 'font-lock-comment-delimiter-face "grey40")
  ;;   (set-face-foreground 'font-lock-string-face "blue3")
  ;;   (set-face-foreground 'dired-directory "blue2"))
  (set-face-attribute 'font-lock-keyword-face nil
                      :foreground nil
                      :weight 'bold
                      )
  (set-face-attribute 'font-lock-type-face nil
                      :foreground nil
                      :background nil
                      ;; :underline nil
                      :weight 'normal
                      )
  (set-face-attribute 'font-lock-function-name-face nil
                      :foreground (face-attribute 'font-lock-keyword-face :foreground)
                      :foreground nil
                      ;; :backgrond nil
                      ;; :background nil
                      ;; :background (face-attribute 'font-lock-keyword-face :background)
                      :weight 'bold
                      )
  (set-face-attribute 'font-lock-preprocessor-face nil
                      ;; :foreground (face-attribute 'font-lock-keyword-face :foreground)
                      ;; :foreground (face-attribute 'font-lock-keyword-face :background)
                      :foreground nil
                      :weight 'normal
                      :inherit nil
                      )

  ;; (setq helm-semantic-display-style 'semantic-format-tag-summarize)

  ;; (setq guide-key/idle-delay 1)

  ;; (define-key spacemacs-mode-map (kbd "RET") 'widget-button-press)

  (with-eval-after-load 'magit-gitflow
    (define-key magit-gitflow-mode-map (kbd "C-f") 'forward-char))

  (setq projectile-indexing-method 'alien)
  (setq projectile-enable-caching t)

  (global-hl-line-mode -1)
  (which-key-mode -1)
  (add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
  (add-hook 'lisp-mode-hook #'aggressive-indent-mode)
  (add-hook 'scheme-mode-hook #'aggressive-indent-mode)

  (windmove-default-keybindings)

  (setf dotspacemacs-whitespace-cleanup 'trailing)

  ;; (setf git-gutter-fr+-side 'left-fringe)
  ;; (setf diff-hl-side 'left)
  ;; (diff-hl-flydiff-mode)

  (setq sp-highlight-pair-overlay nil)

  (helm-mode)
  (with-eval-after-load 'helm-mode
    (helm-projectile-on)
    (global-set-key (kbd "C-c h g") 'spacemacs/helm-google-suggest)
    (global-set-key (kbd "C-c h w") 'spacemacs/helm-wikipedia-suggest)
    (global-set-key (kbd "C-x b") 'helm-buffers-list)
    (global-set-key (kbd "C-x C-f") 'helm-find-files)
    (setq helm-projectile-sources-list '(helm-source-projectile-files-list
                                         helm-source-projectile-directories-list
                                         helm-source-projectile-projects
                                         helm-source-recentf
                                         helm-source-file-cache
                                         ;; helm-source-files-in-current-dir
                                         ;; helm-source-locate
                                         ))

    (evil-leader/set-key
      "pp" 'helm-projectile)


    (setq helm-google-suggest-use-curl-p t
          helm-scroll-amount 4 ; scroll 4 lines other window using M-<next>/M-<prior>
          ;; helm-quick-update t ; do not display invisible candidates
          helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.

          ;; you can customize helm-do-grep to execute ack-grep
          ;; helm-grep-default-command "ack-grep -Hn --smart-case --no-group --no-color %e %p %f"
          ;; helm-grep-default-recurse-command "ack-grep -H --smart-case --no-group --no-color %e %p %f"
          helm-split-window-in-side-p t ;; open helm buffer inside current window, not occupy whole other window

          helm-echo-input-in-header-line t

          ;; helm-candidate-number-limit 500 ; limit the number of displayed canidates
          helm-ff-file-name-history-use-recentf t
          helm-move-to-line-cycle-in-source t ; move to end or beginning of source when reaching top or bottom of source.
          helm-buffer-skip-remote-checking t

          helm-mode-fuzzy-match t

          helm-buffers-fuzzy-matching t ; fuzzy matching buffer names when non-nil
                                        ; useful in helm-mini that lists buffers
          helm-org-headings-fontify t
          ;; helm-find-files-sort-directories t
          ;; ido-use-virtual-buffers t
          ;; helm-semantic-fuzzy-match t
          ;; helm-M-x-fuzzy-match t
          ;; helm-imenu-fuzzy-match t
          ;; helm-lisp-fuzzy-completion t
          ;; helm-apropos-fuzzy-match t
          helm-buffer-skip-remote-checking t
          ;; helm-locate-fuzzy-match t
          helm-display-header-line nil)

    (setq helm-ag-insert-at-point 'symbol)

    (setq helm-autoresize-max-height 0)
    (setq helm-autoresize-min-height 20)
    (helm-autoresize-mode 1)

    (remove-hook 'helm-after-initialize-hook #'spacemacs//helm-prepare-display)
    (remove-hook 'helm-cleanup-hook #'spacemacs//helm-restore-display)
    (remove-hook 'helm-cleanup-hook #'spacemacs//helm-cleanup)
    (remove-hook 'helm-mode-hook #'simpler-helm-bookmark-keybindings)

    (setq helm-display-function #'helm-default-display-buffer)

    (with-eval-after-load 'helm-semantic
      (push '(c-mode . semantic-format-tag-summarize) helm-semantic-display-style)
      (push '(c++-mode . semantic-format-tag-summarize) helm-semantic-display-style))
    (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebihnd tab to do persistent action
    (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
    (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

    (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
    (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
    (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)
    ;; (define-key global-map [remap find-tag] 'helm-etags-select)
    ;; (define-key global-map [remap jump-to-register] 'helm-register)
    (define-key global-map [remap list-buffers] 'helm-buffers-list)
    (spacemacs/set-leader-keys
      "sj"   'spacemacs/helm-jump-in-buffer)
    ;; (define-key global-map [remap find-tag] 'helm-gtags-select)
    (add-hook 'ielm-mode-hook
              #'(lambda ()
                  (define-key ielm-map [remap completion-at-point] 'helm-lisp-completion-or-file-name-at-point)))

    ;; tramp
    ;; (add-to-list 'tramp-default-proxies-alist
    ;;              '("10.51.58.80" nil "/ssh:xtuudoo@jogvan:"))

    (setq cursor-type 't)
    (setq server-raise-frame t)
    (when (eq window-system 'w32)
      (setq w32-pipe-read-delay 0
            w32-pipe-buffer-size 50000))

    ;; (if window-system
    ;;     (add-hook 'server-done-hook
    ;;               (lambda () (shell-command "stumpish 'eval (stumpwm::return-es-called-win stumpwm::*es-win*)'"))))

    (setq-default gc-cons-threshold 800000)
    (ido-mode -1)
    (defun my-minibuffer-setup-hook ()
      (setq gc-cons-threshold most-positive-fixnum))

    (defun my-minibuffer-exit-hook ()
      (setq gc-cons-threshold 800000))

    (add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
    (add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)

    ;; (spacemacs|disable-company eshell-mode)
    ;; (elscreen-start)
    ;; (add-to-list 'custom-theme-load-path (concat user-emacs-directory "private/tuhdo"))
    (global-undo-tree-mode -1)
    )

  (setq gdb-many-windows nil)
  ;; (global-set-key (kbd "<f7>") 'gdb)

  (add-hook 'prog-mode-hook 'subword-mode)
  (add-hook 'prog-mode-hook 'evil-local-mode)
  (show-smartparens-global-mode -1)
  (add-hook 'magit-status-mode-hook 'evil-emacs-state)
  ;; (evil-escape-mode 1)
  (setq evil-want-fine-undo t
        evil-move-cursor-back nil)
  (setq bookmark-default-file "~/.emacs.d/private/tuhdo/bookmarks")
  (setq spacemacs-show-trailing-whitespace nil)
  (setq auto-save-interval 10)
  ;; (add-to-list 'load-path (concat user-emacs-directory "private/tuhdo/local/hemingway-emacs/"))
  ;; (add-to-list 'custom-theme-load-path (concat user-emacs-directory "private/tuhdo/local/hemingway-emacs/"))
  ;; (disable-theme 'twilight-bright)
  ;; (load-theme 'hemingway-light t)
  (add-hook 'emacs-lisp-mode-hook 'hungry-delete-mode)
  (add-hook 'lisp-mode-hook 'hungry-delete-mode)

  ;; (global-set-key (kbd "M-x") 'evil-delete)
  (global-set-key (kbd "RET") 'my-fancy-newline)
  (add-hook 'dired-mode-hook 'evil-emacs-state)
  
  ;; (sp-pair "{" "}" :wrap "M-}")
  ;; (dolist (m '(c++-mode c-mode swift-mode python-mode))
  ;;   )
  (sp-pair "(" ")" :wrap "M-)")
  (sp-pair "{" nil :post-handlers nil)
  (sp-pair "[" nil :wrap "M-]")
  (setq-default bidi-display-reordering nil)
  (setq ls-lisp-use-insert-directory-program nil)
  (require 'ls-lisp)
  ;; python
  (setq python-shell-interpreter "python3")
  ;; (server-start)
  ;; (disable-theme 'wombat)
  (global-set-key (kbd "<backtab>") (function company-complete))
  (setq company-idle-delay nil)
  ;; (disable-theme 'spacemacs-dark)
  ;; (load-theme 'doom-tomorrow-night t)
  )



(defun dotspacemacs/emacs-custom-settings ()
  "Emacs custom settings.
This is an auto-generated function, do not modify its content directly, use
Emacs customize menu instead.
This function is called at the very end of Spacemacs initialization."
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (smartparens zonokai-theme zenburn-theme zen-and-art-theme yapfify yankpad white-sand-theme which-key web-beautify volatile-highlights uuidgen use-package underwater-theme ujelly-theme twilight-theme twilight-bright-theme twilight-anti-bright-theme toxi-theme toc-org tao-theme tangotango-theme tango-plus-theme tango-2-theme symon swift-mode sunny-day-theme sublime-themes subatomic256-theme subatomic-theme string-inflection stickyfunc-enhance srefactor spacegray-theme soothe-theme solarized-theme soft-stone-theme soft-morning-theme soft-charcoal-theme smyx-theme smeargle shell-pop seti-theme reverse-theme reveal-in-osx-finder restart-emacs request region-state redo+ rebox2 rebecca-theme realgud railscasts-theme pyvenv pytest pyenv-mode py-isort purple-haze-theme professional-theme planet-theme pip-requirements phoenix-dark-pink-theme phoenix-dark-mono-theme persp-mode pcre2el pbcopy password-generator paredit osx-trash osx-dictionary orgit organic-green-theme org-projectile org-bullets org-brain open-junk-file omtose-phellack-theme oldlace-theme occidental-theme obsidian-theme noctilux-theme nlinum-relative naquadah-theme mustang-theme move-text monokai-theme monochrome-theme molokai-theme moe-theme minimal-theme material-theme majapahit-theme magit-gitflow madhat2r-theme macrostep lush-theme lorem-ipsum livid-mode live-py-mode linum-relative light-soap-theme launchctl kotlin-mode json-mode js2-refactor js-doc js-comint jbeans-theme jazz-theme ir-black-theme inkpot-theme info+ hydra hy-mode hungry-delete htmlize hl-todo highlight-numbers highlight-indentation hide-comnt heroku-theme hemisu-theme help-fns+ helm-themes helm-swoop helm-pydoc helm-purpose helm-projectile helm-gtags helm-descbinds helm-dash helm-company helm-c-yasnippet helm-ag hc-zenburn-theme gruvbox-theme gruber-darker-theme grandshell-theme gotham-theme golden-ratio-scroll-screen gitignore-mode gitconfig-mode gitattributes-mode ggtags gandalf-theme fuzzy flx-ido flatui-theme flatland-theme fill-column-indicator farmhouse-theme eyebrowse expand-region exotica-theme exec-path-from-shell evil-visualstar evil-visual-mark-mode evil-unimpaired evil-textobj-anyblock evil-surround evil-snipe evil-nerd-commenter evil-matchit evil-lion evil-iedit-state evil-args evil-anzu eval-sexp-fu espresso-theme editorconfig dumb-jump dtrt-indent dracula-theme doom-themes django-theme describe-number dash-at-point darktooth-theme darkokai-theme darkmine-theme darkburn-theme dakrone-theme cython-mode cyberpunk-theme company-tern company-statistics company-lua company-c-headers company-anaconda column-enforce-mode color-theme-sanityinc-tomorrow color-theme-sanityinc-solarized color-identifiers-mode coffee-mode cmake-mode cmake-ide clues-theme clean-aindent-mode cherry-blossom-theme cedit busybee-theme bubbleberry-theme birds-of-paradise-plus-theme bind-map badwolf-theme avy auto-yasnippet auto-highlight-symbol auto-compile apropospriate-theme anti-zenburn-theme ample-zen-theme ample-theme alect-themes aggressive-indent afternoon-theme ac-ispell))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
)
