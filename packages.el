;;; packages.el --- tuhdo Layer packages File for Spacemacs
;;
;; Copyright (c) 2012-2014 Sylvain Benner
;; Copyright (c) 2014-2015 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;; List of all packages to install and/or initialize. Built-in packages
;; which require an initialization must be listed explicitly in the list.
(defvar tuhdo-packages
  `(
    ;; package tuhdos go here
    ;; two-column
    ;; auto-highlight-symbol
    ;; avy
    ;; auctex
    ;; asm-mode
    ;; caps-lock
    cc-mode
    cedit
    company-mode
    color-identifiers-mode
    ;; csharp-mode
    ;; clean-aindent-mode
    dtrt-indent
    ;; disaster
    describe-number
    eyebrowse
    ;; electric-operator
    eshell
    evil-nerd-commenter
    evil-args
    ;; evil-exchange
    evil-textobj-anyblock
    ;; evil-escape
    evil-snipe
    (evil-ergoemacs :location local)
    ;; easy-escape
    expand-region
    js-comint
    ;; eww
    ;; elscreen
    ;; firestarter
    ;; flycheck
    ;; flyspell-lazy
    ;; graphviz-dot-mode
    ;; key-chord
    ;; goto-chg
    ;; nasm-mode
    nlinum
    nlinum-relative
    lua-mode
    (lua-eldoc-mode :location local)
    kotlin-mode
    ;; iasm-mode
    magit
    ;; (mark-lines :location local)
    ;; minizinc-mode
    ;; habitica
    (org-recipes :location local)
    (mmix-mode :location local)
    hippie-exp
    hideshow
    ggtags
    golden-ratio-scroll-screen
    shell-pop
    ;; swift-mode
    ;; eshell-prompt-extras
    ;; slime                           ;
    ;; smart-mode-line
    ;; syntax-subword
    ;; slime-company
    persp-mode
    ;; puppet-mode
    ;; popwin
    ;; paredit
    ;; prolog
    ;; pabbrev
    ;; isearch-dabbrev
    ;; irony
    ;; irony-eldoc
    ;; lispy
    ;; company-irony
    ;; company-irony-c-headers
    ;; flycheck-irony
    ;; semantic
    ;; skeletor
    ;; srecode
    region-state
    ;; register-channel
    ;; rich-minority
    rebox2
    redo+
    ;; rtags
    ;; helm-rtags
    ;; company-rtags
    ;; sx
    org
    (org-wiki :location local)
    ;; omnisharp
    ;; ,(unless (eq window-system 'w32)
    ;;    'pdf-tools)
    ;; vimish-fold
    yasnippet
    yankpad
    ;; vhdl-mode
    ;; quickrun
    ;; wiki-summary
    ;; wordsmith-mode
    ;; worf
    ;; (pamparam :location local)
    ;; x86-lookup
    ;; (prettify-utils :location local)
    ))

;; List of packages to exclude.
(defvar tuhdo-excluded-packages '())

;; For each package, define a function tuhdo/init-<package-tuhdo>
;;
;; (defun tuhdo/init-my-package ()
;;   "Initialize my package"
;;   )
;;
;; Often the body of an initialize function uses `use-package'
;; For more info on `use-package', see readme:
;; https://github.com/jwiegley/use-package
;; (defun tuhdo/init-two-column ()
;;   (use-package two-column
;;     :init
;;     (setq 2C-window-width 100)))

(defun tuhdo/post-init-auto-highlight-symbol ()
  (global-auto-highlight-symbol-mode)
  (global-set-key (kbd "C-c ;") 'ahs-edit-mode)
  (global-unset-key (kbd "C-x C-a"))
  ;; (define-key auto-highlight-symbol-mode-map (kbd "M-<left>") nil)
  ;; (define-key auto-highlight-symbol-mode-map (kbd "M-<right>") nil)
  (define-key auto-highlight-symbol-mode-map (kbd "M-<left>") 'ahs-backward)
  (define-key auto-highlight-symbol-mode-map (kbd "M-<right>") 'ahs-forward)
  (setq ahs-idle-interval 1.0)
  ;; (setq ahs-idle-timer nil)
  ;; (ahs-start-timer)
  (setq ahs-default-range 'ahs-range-whole-buffer)
  ;; (setq ahs-idle-interval 0.2)
  (set-face-attribute 'ahs-definition-face
                      nil
                      :underline '(:color "black" :stye line)
                      :bold nil
                      :box nil
                      :foreground nil
                      :background nil
                      )
  (set-face-attribute 'ahs-face
                      nil
                      :underline '(:color "grey80" :stye line)
                      :box nil
                      :foreground nil
                      :background nil
                      :inherit nil)
  (set-face-attribute 'ahs-plugin-whole-buffer-face
                      nil
                      :underline '(:color "black" :style line)
                      :foreground nil
                      :background nil
                      :inherit nil)
  (setq ahs-inhibit-face-list '(font-lock-comment-delimiter-face
                                font-lock-comment-face
                                font-lock-doc-face
                                font-lock-doc-string-face
                                font-lock-string-face))
  (spacemacs/toggle-automatic-symbol-highlight-on))

(defun tuhdo/post-init-expand-region ()
  (use-package expand-region
    :bind (("M--" . er/expand-region))
    ))

(defun tuhdo/init-js-comint ()
  (use-package js-comint
    :init
    (defun inferior-js-mode-hook-setup ()
      (add-hook 'comint-output-filter-functions 'js-comint-process-output))
    (add-hook 'inferior-js-mode-hook 'inferior-js-mode-hook-setup t)
    (spacemacs/set-leader-keys-for-major-mode 'js2-mode
      "ee" 'js-send-last-sexp
      "sb" 'js-send-buffer
      "sr" 'js-send-region
      "sl" 'js-load-file)
    ))

(defun tuhdo/post-init-yasnippet ()
  (with-eval-after-load 'yasnippet
    (add-to-list 'yas-snippet-dirs (concat user-emacs-directory "private/tuhdo/snippets/"))))

;; Color identifer

(defun tuhdo/init-color-identifiers-mode ()
  (defun tuhdo/conditional-color-identifiers-mode ()
    (when (< (buffer-size) 1000000)
      (color-identifiers-mode)))
  (use-package color-identifiers-mode
    :init
    (add-hook 'c-mode-hook 'tuhdo/conditional-color-identifiers-mode)
    (add-hook 'c++-mode-hook 'tuhdo/conditional-color-identifiers-mode)))

(defun tuhdo/post-init-persp-mode ()
  (setq persp-save-dir spacemacs-cache-directory)
  (setq persp-lighter ""))

(defun tuhdo/post-init-hippie-exp ()
  (setq hippie-expand-try-functions-list
        '(;; yas-hippie-try-expand
          try-expand-dabbrev
          try-expand-dabbrev-from-kill
          ;; my/he-try-expand-flx
          try-expand-dabbrev-all-buffers
          try-complete-file-name-partially
          try-complete-file-name
          try-expand-all-abbrevs
          try-expand-list
          try-expand-line
          try-complete-lisp-symbol-partially
          try-complete-lisp-symbol
          try-expand-flx
          )))

(defun tuhdo/init-hideshow ()
  (use-package hideshow
    :init
    (add-hook 'prog-mode-hook 'hs-minor-mode)
    (spacemacs/set-leader-keys
      "zz" 'hs-hide-level
      "zs" 'hs-show-block
      "zr" 'hs-hide-all
      "za" 'hs-show-all)))

(defun tuhdo/init-golden-ratio-scroll-screen ()
  (use-package golden-ratio-scroll-screen
    :defer t
    :init
    (global-set-key [remap scroll-down-command] 'golden-ratio-scroll-screen-down)
    (global-set-key [remap scroll-up-command] 'golden-ratio-scroll-screen-up)
    ;; (global-set-key [remap evil-scroll-down] 'golden-ratio-scroll-screen-up)
    ;; (global-set-key [remap evil-scroll-up] 'golden-ratio-scroll-screen-down)
    (setq golden-ratio-scroll-highlight-flag nil)))

(defun tuhdo/post-init-magit ()
  (with-eval-after-load 'magit
    (define-key magit-mode-map (kbd "C-n") 'next-line))
  (setq-default git-magit-status-fullscreen t)
  (setq magit-completing-read-function 'magit-builtin-completing-read
        magit-push-always-verify nil)
  (require 'with-editor)
  (shell-command-with-editor-mode 1)
  (add-hook 'shell-mode-hook  'with-editor-export-editor)
  (add-hook 'term-exec-hook   'with-editor-export-editor)
  (add-hook 'eshell-mode-hook 'with-editor-export-editor))

(defun tuhdo/init-shell-pop ()
  (use-package shell-pop
    :bind (("C-c '" . shell-pop))
    :config
    (setq shell-pop-window-position "bottom"
          shell-pop-window-size     30
          shell-pop-full-span       t)
    (shell-pop--set-shell-type 'shell-pop-shell-type
                               '("eshell" "*eshell*" (lambda nil  (eshell))))))

(defun tuhdo/post-init-swift-mode ()
  (with-eval-after-load 'swift-mode
    (define-key swift-mode-map (kbd "M-j") 'backward-char))
  (setq swift-mode:repl-executable "swift"))

(defun tuhdo/init-eshell-prompt-extras ()
  (use-package eshell-prompt-extras
    :commands epe-theme-lambda
    :init
    (setq eshell-highlight-prompt nil
          eshell-prompt-function 'epe-theme-lambda)
    :config
    (require 'cl)))

(defun tuhdo/init-syntax-subword ()
  (use-package syntax-subword
    :init
    (global-syntax-subword-mode)))

;; (defun tuhdo/init-minizinc-mode ()
;;   (use-package minizinc-mode
;;     :init
;;     (add-to-list 'auto-mode-alist '("\\.mzn\\'" . minizinc-mode))))

(defun tuhdo/init-prolog ()
  (use-package prolog
    :defer t
    :init
    (setq prolog-system 'swi)
    (setq auto-mode-alist (append '(("\\.pl$" . prolog-mode)
                                    ("\\.m$" . mercury-mode))
                                  auto-mode-alist)))
  )


(defun tuhdo/init-region-state ()
  (use-package region-state
    :init
    (region-state-mode 1)))

;; (defun tuhdo/init-rich-minority ()
;;   (use-package rich-minority
;;     :init
;;     (rich-minority-mode 1)
;;     (setq rm-blacklist "")))

;; (defmacro tuhdo/define-eyebrowse-binding (key)
;;   `(define-key window-numbering-keymap (kbd ,(concat "M-" key))
;;      (lambda ()
;;        (interactive)
;;        (funcall ',(intern (concat "eyebrowse-switch-to-window-config-" key)))
;;        (funcall ',(intern (concat "eyebrowse-switch-to-window-config-" key))))))

(defun tuhdo/post-init-eyebrowse ()
  (global-set-key (kbd "C-1") 'eyebrowse-switch-to-window-config-1)
  (global-set-key (kbd "C-2") 'eyebrowse-switch-to-window-config-2)
  (global-set-key (kbd "C-3") 'eyebrowse-switch-to-window-config-3)
  (global-set-key (kbd "C-4") 'eyebrowse-switch-to-window-config-4)
  (global-set-key (kbd "C-5") 'eyebrowse-switch-to-window-config-5)
  (global-set-key (kbd "C-6") 'eyebrowse-switch-to-window-config-6)
  (global-set-key (kbd "C-7") 'eyebrowse-switch-to-window-config-7)
  (global-set-key (kbd "C-8") 'eyebrowse-switch-to-window-config-8)
  (global-set-key (kbd "C-9") 'eyebrowse-switch-to-window-config-9)
  (global-set-key (kbd "C-0") 'eyebrowse-switch-to-window-config-0)
  (global-set-key (kbd "C-1") 'eyebrowse-switch-to-window-config-1)
  (setq eyebrowse-new-workspace t)
  ;; (use-package eyebrowse                ;
  ;;   :bind (("C-1" . eyebrowse-switch-to-window-config-1)
  ;;          ("C-2" . eyebrowse-switch-to-window-config-2)
  ;;          ("C-3" . eyebrowse-switch-to-window-config-3)
  ;;          ("C-4" . eyebrowse-switch-to-window-config-4)
  ;;          ("C-5" . eyebrowse-switch-to-window-config-5)
  ;;          ("C-6" . eyebrowse-switch-to-window-config-6)
  ;;          ("C-7" . eyebrowse-switch-to-window-config-7)
  ;;          ("C-8" . eyebrowse-switch-to-window-config-8)
  ;;          ("C-9" . eyebrowse-switch-to-window-config-9)
  ;;          ("C-0" . eyebrowse-switch-to-window-config-0))
  ;;   :config
  ;;   ;; (eyebrowse-mode)
  ;;   )
  )

(defun tuhdo/init-evil-ergoemacs ()
  (use-package evil-ergoemacs))

(defun tuhdo/init-electric-operator ()
  (use-package electric-operator
    :init
    (add-hook 'lua-mode-hook 'electric-operator-mode)
    (add-hook 'swift-mode-hook 'electric-operator-mode)
    (add-hook 'c-mode-hook 'electric-operator-mode)
    (add-hook 'c++-mode-hook 'electric-operator-mode)
    :config
    (electric-operator-add-rules-for-mode 'swift-mode (cons ">" nil))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "<" nil))
    (electric-operator-add-rules-for-mode 'swift-mode (cons ":" ": "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "=" " = "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "+=" " += "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "-=" " -= "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "*=" " *= "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "/=" " /= "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "!=" " != "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons "<=" " <= "))
    (electric-operator-add-rules-for-mode 'swift-mode (cons ">=" " >= "))
    (dolist (m '(c-mode c++-mode))
      (electric-operator-add-rules-for-mode m (cons "){" ") {"))
      (electric-operator-add-rules-for-mode m (cons "--" "--"))
      (electric-operator-add-rules-for-mode m (cons "++" "++"))
      (electric-operator-add-rules-for-mode m (cons ">>" nil))
      (electric-operator-add-rules-for-mode m (cons "<<" nil))
      (electric-operator-add-rules-for-mode m (cons ">" nil))
      (electric-operator-add-rules-for-mode m (cons "<" nil))
      ;; (electric-operator-add-rules-for-mode m (cons "return" "return "))
      (electric-operator-add-rules-for-mode m (cons "cin" "cin "))
      ;; (electric-operator-add-rules-for-mode m (cons " +" " + "))
      ;; (electric-operator-add-rules-for-mode m (cons " -" " - "))
      (electric-operator-add-rules-for-mode m (cons "*" nil))
      (electric-operator-add-rules-for-mode m (cons "&" nil))
      ;; (electric-operator-add-rules-for-mode m (cons " /" " / "))
      (electric-operator-add-rules-for-mode m
                                            (cons "for(" "for (")
                                            (cons "rep(" "rep (")
                                            (cons "repc" "repc ")
                                            (cons "repi" "repi ")
                                            (cons "repj" "repj ")
                                            (cons "repk" "repk ")
                                            (cons "loop" "loop ")
                                            ;; (cons "else" " else ")
                                            (cons "until" "until ")
                                            (cons "((" " ((")
                                            (cons "if(" "if (")
                                            ;; (cons " ( " " ( ")
                                            ;; (cons ")" ") ")
                                            (cons "{{" " {{")
                                            ;; (cons "{" " {")
                                            (cons " { " " { ")
                                            (cons "}" "} ")
                                            (cons "unless" "unless ")
                                            (cons "while" "while ")
                                            (cons ";" "; ")
                                            )
      )
    (define-key c++-mode-map (kbd "M-j") 'backward-char)))

(defun tuhdo/post-init-eshell ()
  (defalias 'edf 'eshell-directory-files)
  (with-eval-after-load 'eshell
    (remove-hook 'eshell-mode-hook 'spacemacs//eshell-switch-company-frontend)))

(defun tuhdo/post-init-evil-nerd-commenter ()
  (spacemacs/set-leader-keys
    ";" 'evilnc-comment-or-uncomment-lines
    )

  (define-key evil-normal-state-map "gc" 'avy-goto-char-in-line)
  (define-key evil-normal-state-map (kbd "gl") 'evilnc-quick-comment-or-uncomment-to-the-line)
  (define-key evil-normal-state-map (kbd "gy") 'evilnc-copy-and-comment-lines)
  (define-key evil-normal-state-map (kbd "gr") 'evilnc--comment-or-uncomment-region)
  )

(defun tuhdo/post-init-evil-args ()
  ;; bind evil-args text objects
  (define-key evil-inner-text-objects-map "a" 'evil-inner-arg)
  (define-key evil-outer-text-objects-map "a" 'evil-outer-arg)

  ;; bind evil-forward/backward-args
  (define-key evil-visual-state-map ">" 'evil-forward-arg)
  (define-key evil-visual-state-map "<" 'evil-backward-arg)
  (define-key evil-normal-state-map ">" 'evil-forward-arg)  
  (define-key evil-normal-state-map "<" 'evil-backward-arg)
  (define-key evil-motion-state-map ">" 'evil-forward-arg)
  (define-key evil-motion-state-map "<" 'evil-backward-arg)

  ;; bind evil-jump-out-args
  (define-key evil-normal-state-map "go" 'evil-jump-out-args)
  )

(defun tuhdo/init-evil-exchange ()
  (use-package evil-exchange
    :init
    (evil-exchange-install)))

(defun tuhdo/init-evil-textobj-anyblock ()
  (use-package evil-textobj-anyblock
    :init
    (define-key evil-inner-text-objects-map "b" 'evil-textobj-anyblock-inner-block)
    (define-key evil-outer-text-objects-map "b" 'evil-textobj-anyblock-a-block)
    ))

(defun tuhdo/post-init-evil-escape ()
  (global-unset-key (kbd "M-g"))
  ;; (setq-default evil-escape-key-sequence "jj"
  ;;               evil-esc-delay 0.2)
  (global-set-key (kbd "M-g") 'evil-escape)
  )

(defun tuhdo/post-init-evil-surround()
  (setq evil-surround-pairs-alist
        '((?\( . ("(" . ")"))
          (?\[ . ("[" . "]"))
          (?\{ . ("{" . "}"))

          (?\) . ("( " . " )"))
          (?\] . ("[ " . " ]"))
          (?\} . ("{ " . " }"))

          (?# . ("#{" . "}"))
          (?b . ("(" . ")"))
          (?B . ("{" . "}"))
          (?> . ("<" . ">"))
          (?t . evil-surround-read-tag)
          (?< . evil-surround-read-tag)
          (?f . evil-surround-function))))

;; (defun tuhdo/init-eww ()
;;   (use-package eww
;;     :defer t
;;     :init
;;     (evil-leader/set-key
;;       "ol" 'eww-list-bookmarks)))

(defun tuhdo/init-describe-number ()
  (use-package describe-number
    :defer t
    :init
    (progn
      (spacemacs/set-leader-keys
        "dn" 'describe-number-at-point))))

(defun tuhdo/init-dtrt-indent ()
  (use-package dtrt-indent
    :defer t
    :init
    (add-hook 'prog-mode-hook 'dtrt-indent-mode)))

(defun tuhdo/init-nlinum ()
  (use-package nlinum
    :init
    (add-hook 'prog-mode-hook 'nlinum-mode)
    (setq nlinum-format " %d "))
  )

(defun tuhdo/init-kotlin-mode ()
  (use-package kotlin-mode
    :init
    (setq default-tab-width 4)))

(defun tuhdo/init-nlinum-relative ()
  (use-package nlinum-relative
    :init
    (add-hook 'prog-mode-hook 'nlinum-relative-on)
    (setq nlinum-relative-redisplay-delay 0.1))
  )

(defun tuhdo/post-init-lua-mode ()
  (setq company-lua-interpreter "luajit")
  (setq company-lua-executable "luajit")
  (setq lua-default-application "luajit"))

(defun tuhdo/init-lua-eldoc-mode ()
  (use-package lua-eldoc-mode
    :init
    (add-hook 'lua-mode-hook 'lua-eldoc-mode)))
;; (defun tuhdo/init-bookmark+()
;;   (use-package bookmark+))

;; (defun tuhdo/init-bm ()
;;   (use-package bm
;;     :bind (("<f9>" . bm-next)
;;            ("<f10>" . bm-previous)))
;;   )

(defun tuhdo/post-init-cc-mode ()
  (defun enable-coding-practice ()
    ;; (flycheck-mode -1)
    ;; (smartparens-mode -1)
    ;; (electric-layout-mode -1)
    ;; (add-to-list 'electric-layout-rules '(?{ . after))
    (electric-indent-mode -1)
    (setq-default c-auto-newline nil)
    ;; (auto-highlight-symbol-mode -1)
    (semantic-idle-summary-mode -1)
    ;; (company-mode -1)
    ;; (yas-minor-mode -1)
    ;; (font-lock-mode -1)
    (when (eq major-mode 'c++-mode)
      (setq-local flycheck-clang-args "-std=c++11"))
    ;; (define-key evil-normal-state-map (kbd "b") 'cedit-up-block-backward)
    ;; (dolist (m (list c-mode-map c++-mode-map))
    ;; (define-key m (kbd "M-h") 'c-beginning-of-statement)
    ;; (define-key m (kbd "M-;") 'c-end-of-statement)
    ;; (define-key m (kbd "M-f") 'c-electric-delete-forward)
    ;; (define-key m (kbd "C-e") 'smarter-move-beginning-of-line))
    ;; (local-set-key (kbd "<backtab>") 'company-irony-c-headers)
    (define-key c++-mode-map (kbd "M-j") 'backward-char)
    (c-toggle-auto-hungry-state 1)
    ;; (setq-default c-electric-flag t)
    )

  (defun my-c-style ()
    (setq c-basic-offset 4
          c-hungry-delete-key t
          c-default-style "linux"))

  (add-hook 'c++-mode-hook
            '(lambda ()
               (require 'yankpad)
               (c-set-style "stroustrup")
               (setq c-basic-offset 2)))
  (add-hook 'c-mode-hook 'enable-coding-practice)
  (add-hook 'c++-mode-hook 'enable-coding-practice)
  (add-hook 'c-mode-hook 'my-c-style)
  (add-hook 'c++-mode-hook 'my-c-style)
  ;; (add-hook 'c++-mode-hook 'prettify-cc-modes)
  ;; (add-hook 'c-mode-hook 'prettify-cc-modes)

  ;; (add-hook 'c-mode-hook 'er/add-cc-mode-expansions)
  ;; (add-hook 'c++-mode-hook 'er/add-cc-mode-expansions)
  (font-lock-add-keywords 'c++-mode
                          '(("vvpii" . font-lock-type-face)
                            ("vvi" . font-lock-type-face)
                            ("pii" . font-lock-type-face)
                            ("vii" . font-lock-type-face)
                            ("vi" . font-lock-type-face)
                            ("unless" . font-lock-keyword-face)
                            ("until" . font-lock-keyword-face)
                            ("all" . font-lock-keyword-face)
                            ("dbg" . font-lock-keyword-face)
                            ("elif" . font-lock-keyword-face)
                            ("loop" . font-lock-keyword-face)
                            ("fastio" . font-lock-keyword-face)
                            ("rrepc" . font-lock-keyword-face)
                            ("mapc" . font-lock-keyword-face)
                            ("#define" . font-lock-keyword-face)
                            ("rrepc" . font-lock-keyword-face)
                            ("rrep" . font-lock-keyword-face)
                            ("repc" . font-lock-keyword-face)
                            ("repi" . font-lock-keyword-face)
                            ("repj" . font-lock-keyword-face)
                            ("repk" . font-lock-keyword-face)
                            ("rep" . font-lock-keyword-face)))
  (add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
  (setq c-basic-offset 4
        c-hungry-delete-key t
        c-default-style "linux"))

(defun tuhdo/init-cedit ()
  (use-package cedit
    :init
    (global-set-key (kbd "M-[") 'cedit-wrap-brace)
    (with-eval-after-load 'cc-mode
      (dolist (m (list c-mode-map c++-mode-map))
        (define-key m (kbd "M-U") 'cedit-up-block-backward)
        (define-key m (kbd "M-O") 'cedit-up-block-forward))))
  )

(defun tuhdo/post-init-company-mode ()
  (define-key company-active-map (kbd "<tab>") 'company-complete-selection)
  (define-key company-active-map (kbd "M-i") 'company-select-previous)
  (define-key company-active-map (kbd "M-k") 'company-select-next)
  (define-key company-active-map (kbd "M-I") 'company-previous-page)
  (define-key company-active-map (kbd "M-K") 'company-next-page)
  (define-key company-active-map (kbd "M-s") 'company-filter-candidates)
  )

(defun tuhdo/init-yankpad ()
  (use-package yankpad
    :defer t
    :init
    (setq yankpad-file "~/Documents/wiki/C++.org")
    ;; :config
    ;; (bind-key "<f7>" 'yankpad-map)
    ;; (bind-key "C-f" 'yankpad-expand)
    ;; If you want to complete snippets using company-mode
    )
  )

(defun tuhdo/post-init-org ()
  (global-unset-key (kbd "C-c a"))
  (global-unset-key (kbd "C-c c"))
  (spacemacs/set-leader-keys
    "oa" 'org-agenda
    "oc" 'org-capture
    "or" 'org-refile
    "os" 'org-store-link)
  (with-eval-after-load 'org
    (define-key org-mode-map (kbd "C-<") 'org-begin-template)
    ;; (use-package org-pomodoro
    ;;   :config
    ;;   (setq org-pomodoro-length 30)
    ;;   (setq org-pomodoro-short-break-length 2)
    ;;   (setq org-pomodoro-audio-player "mplayer")
    ;;   (setq org-pomodoro-finished-sound-args "-volume 0.3")
    ;;   (setq org-pomodoro-long-break-sound-args "-volume 0.3")
    ;;   (setq org-pomodoro-short-break-sound-args "-volume 0.3"))

    ;; (use-package org-journal
    ;;   :bind (("C-c C-j" . org-journal))
    ;;   :init
    ;;   (setq org-journal-dir "~/Dropbox_home/journal/daily_logs/"))

    ;; (setq org-ellipsis " ▼ ")
    ;; (require 'org-drill)
    ;; (require 'org-depend)
    ;; (require 'ox-beamer)
    ;; (require 'org-habit)

    (add-to-list 'auto-mode-alist '("\\`[^.].*\\.org'\\|^.[0-9]+" . org-mode))
    (add-hook 'org-mode-hook 'auto-fill-mode)
    (add-hook 'org-mode-hook (lambda () (setq fill-column 80)))
    (define-key org-mode-map (kbd "C-c j") 'helm-org-in-buffer-headings)

    ;; (global-set-key (kbd "C-c a") 'org-agenda)
    ;; (global-set-key (kbd "C-c l") 'org-store-link)

    ;; (global-set-key "\C-cb" 'org-iswitchb)
    (setq org-log-done t)

    (add-hook 'org-mode-hook 'org-indent-mode)

    ;; (setq org-default-notes-file (concat org-directory "~/org-data/tasks.org"))
    (defvar custom--org-project-file-path "~/org-data/")

    ;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
    (setq org-capture-templates
          (quote (("t" "todo" entry (file "~/org-data/refile.org")
                   "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                  ("l" "link" entry (file "~/org-data/refile.org")
                   "* %?\n%A\n" :clock-in t :clock-resume t)
                  ("r" "respond" entry (file "~/org-data/refile.org")
                   "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
                  ("n" "note" entry (file "~/org-data/refile.org")
                   "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
                  ("j" "Journal" entry (file+datetree "~/org-data/diary.org")
                   "* %?\n%U\n" :clock-in t :clock-resume t)
                  ("w" "org-protocol" entry (file "~/org-data/refile.org")
                   "* TODO Review %c\n%U\n" :immediate-finish t)
                  ("m" "Meeting" entry (file "~/org-data/refile.org")
                   "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
                  ("p" "Phone call" entry (file "~/org-data/refile.org")
                   "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
                  ("h" "Habit" entry (file "~/org-data/refile.org")
                   "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"<%Y-%m-%d %a .+1d/3d>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

    (setq org-clock-persist 'history)
    (org-clock-persistence-insinuate)
    (setq org-replace-disputed-keys nil)

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((sh . t)
       (lisp . t)))

    (setq org-agenda-files (list
                            "~/Documents/wiki/tasks.org"
                            "~/Dropbox_home/journal/tasks.org"
                            "~/Dropbox_home/books/todos.org"
                            ;; "~/Dropbox_home/journal/daily_logs/"
                            ))
    ;; (setq org-agenda-file-regexp "\\`[^.].*\\.org'\\|[0-9]+")
    (add-hook 'org-mode-hook
              (lambda ()
                (local-set-key "\M-\C-n" 'outline-next-visible-heading)
                (local-set-key "\M-\C-p" 'outline-previous-visible-heading)
                (local-set-key "\M-\C-u" 'outline-up-heading)
                ;; table
                (local-set-key "\M-\C-w" 'org-table-copy-region)
                (local-set-key "\M-\C-y" 'org-table-paste-rectangle)
                (local-set-key "\M-\C-l" 'org-table-sort-lines)
                ;; display images
                (local-set-key "\M-I" 'org-toggle-iimage-in-org)))

    (setq org-use-speed-commands t)
    (setq org-confirm-babel-evaluate nil)
    (setq org-src-fontify-natively nil)
    (setq org-src-tab-acts-natively t)

    (setq org-todo-keywords
          (quote ((sequence "TODO(t)" "NEXT(n)" "PROGRESS(p)" "|" "DONE(d)")
                  (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

    (setq org-todo-keyword-faces
          (quote (("TODO" :foreground "red" :weight bold)
                  ("NEXT" :foreground "blue" :weight bold)
                  ("PROGRESS" :foreground "dark blue" :weight bold)
                  ("DONE" :foreground "forest green" :weight bold)
                  ("HOLD" :foreground "magenta" :weight bold)
                  ("WAITING" :foreground "orange" :weight bold)
                  ("CANCELLED" :foreground "forest green" :weight bold)
                  ("MEETING" :foreground "forest green" :weight bold)
                  ("PHONE" :foreground "forest green" :weight bold))))

    ;; ,----
    ;; | Refilling Tasks
    ;; `----
    ;; Targets include this file and any file contributing to the agenda - up to 9 levels deep
    (setq org-refile-targets (quote ((nil :maxlevel . 9)
                                     (org-agenda-files :maxlevel . 9))))

    ;; Use full outline paths for refile targets - we file directly with IDO
    (setq org-refile-use-outline-path t)

    ;; Targets complete directly with IDO
    (setq org-outline-path-complete-in-steps nil)

    ;; Allow refile to create parent tasks with confirmation
    (setq org-refile-allow-creating-parent-nodes (quote confirm))

    ;; Use IDO for both buffer and file completion and ido-everywhere to t
    ;; (setq org-completion-use-ido t)
    ;; (setq ido-everywhere t)
    ;; (setq ido-max-directory-size 100000)
    ;; (ido-mode (quote both))
    ;; Use the current window when visiting files and buffers with ido
    ;; (setq ido-default-file-method 'selected-window)
    ;; (setq ido-default-buffer-method 'selected-window)
    ;; Use the current window for indirect buffer display
    ;; (setq org-indirect-buffer-display 'current-window)

;;;; Refile settings
    ;; Exclude DONE state tasks from refile targets
    (setq org-refile-target-verify-function 'bh/verify-refile-target)

    ;; ,----
    ;; | Agenda setup
    ;; `----
    ;; Do not dim blocked tasks
    (setq org-agenda-dim-blocked-tasks nil)

    ;; Compact the block agenda view
    (setq org-agenda-compact-blocks t)

    ;; Custom agenda command definitions
    (setq org-agenda-custom-commands
          (quote (("N" "Notes" tags "NOTE"
                   ((org-agenda-overriding-header "Notes")
                    (org-tags-match-list-sublevels t)))
                  ("h" "Habits" tags-todo "STYLE=\"habit\""
                   ((org-agenda-overriding-header "Habits")
                    (org-agenda-sorting-strategy
                     '(todo-state-down effort-up category-keep))))
                  (" " "Agenda"
                   ((agenda "" nil)
                    (tags "REFILE"
                          ((org-agenda-overriding-header "Tasks to Refile")
                           (org-tags-match-list-sublevels nil)))
                    (tags-todo "-CANCELLED/!"
                               ((org-agenda-overriding-header "Stuck Projects")
                                (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-HOLD-CANCELLED/!"
                               ((org-agenda-overriding-header "Projects")
                                (org-agenda-skip-function 'bh/skip-non-projects)
                                (org-tags-match-list-sublevels 'indented)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-CANCELLED/!NEXT"
                               ((org-agenda-overriding-header (concat "Project Next Tasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                                (org-tags-match-list-sublevels t)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(todo-state-down effort-up category-keep))))
                    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                               ((org-agenda-overriding-header (concat "Project Subtasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-non-project-tasks)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                               ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-project-tasks)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-CANCELLED+WAITING|HOLD/!"
                               ((org-agenda-overriding-header "Waiting and Postponed Tasks")
                                (org-agenda-skip-function 'bh/skip-stuck-projects)
                                (org-tags-match-list-sublevels nil)
                                (org-agenda-todo-ignore-scheduled t)
                                (org-agenda-todo-ignore-deadlines t)))
                    (tags "-REFILE/"
                          ((org-agenda-overriding-header "Tasks to Archive")
                           (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                           (org-tags-match-list-sublevels nil))))
                   nil))))

    (add-hook 'org-mode-hook (lambda () (semantic-mode -1)))
    (add-hook 'org-mode-hook 'flyspell-mode)

    ;; Make windmove work in org-mode:
    (add-hook 'org-shiftup-final-hook 'windmove-up)
    (add-hook 'org-shiftleft-final-hook 'windmove-left)
    (add-hook 'org-shiftdown-final-hook 'windmove-down)
    (add-hook 'org-shiftright-final-hook 'windmove-right)

    ;; (add-hook 'org-mode-hook
    ;;           (lambda ()
    ;;             (add-hook 'before-save-hook 'my/org-add-ids-to-headlines-in-file nil 'local)))

    ;; (setq org-hide-emphasis-markers t)

    (require 'ox-html)
    ;; (require 'ox-rs)			;
    (require 'ox-publish)

    (setq org-publish-project-alist
          '(("blog-content"
             :base-directory "~/Public/emacs-tutor/emacs-tutor/"
             :html-extension "html"
             :base-extension "org"
             :publishing-directory "~/Public/emacs-tutor/"
             :publishing-function (org-html-publish-to-html)
             :recursive t               ; descend into sub-folders?
             :section-numbers nil       ; don't create numbered sections
             :with-toc t                ; don't create a table of contents
             :with-latex t              ; do use MathJax for awesome formulas!
             :html-preamble
             (lambda (info)
               (concat "<!-- Start of StatCounter Code for Default Guide -->
<script type=\"text/javascript\">
var sc_project=9874755;
var sc_invisible=1;
var sc_security=\"c2028bb7\";
var scJsHost = ((\"https:\" == document.location.protocol) ?
\"https://secure.\" : \"http://www.\");
document.write(\"<sc\"+\"ript type='text/javascript' src='\" + scJsHost+
\"statcounter.com/counter/counter.js'></\"+\"script>\");
</script>
<noscript><div class=\"statcounter\"><a title=\"hit counter\"
href=\"http://statcounter.com/free-hit-counter/\" target=\"_blank\"><img
class=\"statcounter\" src=\"http://c.statcounter.com/9874755/0/c2028bb7/1/\"
alt=\"hit counter\"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->"
                (cond ((and (listp (plist-get info :title))
                            (or (string= (car (plist-get info :title)) "Table of Contents")
                                (string= (car (plist-get info :title)) "Contents")))
                       "")
                      (t "
<h2><a href=\"index.html\">Back to Table of Contents</a></h2>
"))))                                   ; this stuff is put before your post
             :html-postamble
             (lambda (info)
               (cond ((and (listp (plist-get info :title))
                           (or (string= (car (plist-get info :title)) "Table of Contents")
                               (string= (car (plist-get info :title)) "Contents")))
                      "")
                     (t "
    <div id=\"disqus_thread\"></div>
    <script type=\"text/javascript\">
  /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
  var disqus_shortname = 'emacs-tutor'; // required: replace example with your forum shortname

  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function() {
      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
  })();
    </script>
    <noscript>Please enable JavaScript to view the <a href=\"http://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>
    <a href=\"http://disqus.com\" class=\"dsq-brlink\">comments powered by <span class=\"logo-disqus\">Disqus</span></a>
")
                     ;; (t "<div id=\"archive\"><a href=\"index.html\">index</a></div>")
                     ))
             :html-head-extra "<link rel=\"stylesheet\" href=\"./static/worg.css\">"
             ;; :auto-sitemap t
             ;; :sitemap-filename "index.org"
             ;; :sitemap-title "Table of Content"
             ;; :sitemap-sort-files chronologically
             ;; :sitemap-style list
             ;; :makeindex t
             :html-head-extra "<link rel=\"stylesheet\" href=\"./static/worg.css\">")
            ("blog-static"
             :base-directory "~/Public/emacs-tutor/emacs-tutor/static/"
             :base-extension "gif\\|png\\|jpg\\|css"
             :publishing-directory "~/Public/emacs-tutor/static"
             :recursive t
             :publishing-function org-publish-attachment)
            ("blog"
             :components ("blog-content" "blog-static"))))

    (setcar (nthcdr 2 org-emphasis-regexp-components) " \t\r\n\"'")
    (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)

    (setq org-use-speed-commands t)
    (add-to-list 'org-structure-template-alist '("n" "#+NAME:"))))

(defun tuhdo/init-org-recipes ()
  (use-package org-recipes
    :bind (("C-c o r" . org-recipes)
           ("C-c o f" . org-recipes-dwim)
           )
    :config
    (use-package org-wiki)))

(defun tuhdo/init-mmix-mode ()
  (use-package mmix-mode
    :defer t
    :init
    (autoload 'mmix-mode "mmix-mode" "Major mode for editing MMIX files" t)
    (setq auto-mode-alist (cons '("\\.mms" . mmix-mode)
                                auto-mode-alist))))
(defun tuhdo/init-rebox2 ()
  (use-package rebox2
    :defer t
    :init
    ;; (global-set-key [(meta q)] 'rebox-dwim)
    (global-set-key [(shift meta q)] 'rebox-cycle)
    (defun c-rebox-style ()
      (setq rebox-style-loop '(223 241 245)))
    (add-hook 'c-mode-hook #'c-rebox-style)
    (add-hook 'c++-mode-hook #'c-rebox-style)
    )
  )

(defun tuhdo/init-org-wiki ()
  (use-package org-wiki
    :bind (("C-c o p" . org-wiki-panel)
           ("C-c o w" . org-wiki-index)
           ("C-c o l" . org-wiki-link)
           ("C-c o i" . org-wiki-insert)
           ("C-c o h" . org-wiki-header)
           ("C-c o j" . org-wiki-helm)
           ("C-c o d" . org-wiki-dired)
           ("C-c o D" . org-wiki-dired-all)
           ("C-c o A" . org-wiki-asset-insert))
    :config
    (setq org-wiki-location "~/Documents/wiki")))

(defun tuhdo/init-redo+ ()
  (use-package redo+))

(defun tuhdo/post-init-helm-gtags ()
  (setenv "GTAGSLIBPATH" (concat (getenv "HOME") "/.gtags.d/")))

(defun tuhdo/post-init-ggtags ()
  (with-eval-after-load 'ggtags
    (define-key ggtags-mode-map (kbd "M-.") 'helm-gtags-dwim))
  (add-hook 'c-mode-hook (lambda ()
                           (ggtags-mode 1)
                           (helm-gtags-mode 1)))
  (add-hook 'c++-mode-hook (lambda ()
                             (ggtags-mode 1)
                             (helm-gtags-mode 1)))
  (add-hook 'd-mode-hook (lambda ()
                           (helm-gtags-mode 1)
                           (ggtags-mode 1)
                           (eldoc-mode 1))))
(defun tuhdo/init-evil-snipe ()
  (use-package evil-snipe
    :init
    (evil-define-key 'motion evil-snipe-override-mode-map "f" 'evil-snipe-f)
    (evil-define-key 'motion evil-snipe-override-mode-map "F" 'evil-snipe-F)
    (evil-define-key 'motion evil-snipe-override-mode-map "t" 'evil-snipe-t)
    (evil-define-key 'motion evil-snipe-override-mode-map "T" 'evil-snipe-T)
    (evil-define-key 'normal evil-snipe-override-mode-map "s" 'evil-substitute)
    (evil-define-key 'normal evil-snipe-override-mode-map "S" 'evil-change-whole-line)

    (setq evil-snipe-override-evil-repeat-keys nil)
    (setq evil-snipe-scope 'buffer)     ;
    (evil-snipe-override-mode)
    ))

(defun tuhdo/init-rtags ()
  (defun rtags-evil-standard-keybindings (mode)
    (spacemacs/declare-prefix-for-mode mode "mg" "goto")
    (spacemacs/declare-prefix-for-mode mode "mr" "rtags")
    (spacemacs/set-leader-keys-for-major-mode mode
      "ga" 'projectile-find-other-file
      "gA" 'projectile-find-other-file-other-window
      "." 'rtags-find-symbol-at-point
      "tr" 'rtags-find-references-at-point
      "tv" 'rtags-find-virtuals-at-point
      "tV" 'rtags-print-enum-value-at-point
      "t/" 'rtags-find-all-references-at-point
      "tY" 'rtags-cycle-overlays-on-screen
      "t>" 'rtags-find-symbol
      "t<" 'rtags-find-references
      "," 'rtags-location-stack-back
      "t]" 'rtags-location-stack-forward
      "tD" 'rtags-diagnostics
      "tG" 'rtags-guess-function-at-point
      "tp" 'rtags-set-current-project
      "tP" 'rtags-print-dependencies
      "te" 'rtags-reparse-file
      "tE" 'rtags-preprocess-file
      "tR" 'rtags-rename-symbol
      "tM" 'rtags-symbol-info
      "tS" 'rtags-display-summary
      "tO" 'rtags-goto-offset
      "tf" 'rtags-find-file
      "tF" 'rtags-fixit
      "tL" 'rtags-copy-and-print-current-location
      "tX" 'rtags-fix-fixit-at-point
      "tB" 'rtags-show-rtags-buffer
      "ti" 'rtags-imenu
      "tT" 'rtags-taglist
      "th" 'rtags-print-class-hierarchy
      "ta" 'rtags-print-source-arguments
      "tj" 'rtags-next-match
      "tk" 'rtags-previous-match
      )
    )
  ;; (define-key evil-normal-state-map (kbd "M-.") 'rtags-find-symbol-at-point)
  ;; (define-key c++-mode-map (kbd "M-.") 'rtags-find-symbol-at-point)
  ;; (define-key c++-mode-map (kbd "M-,") 'rtags-location-stack-back)
  
  (defun rtags-eldoc-function ()
    (let* ((symbol (rtags-symbol-info-internal :location (or (rtags-target-declaration-first) (rtags-current-location)) :silent t))
           (symbol-text (cdr (assoc 'contents (rtags-get-file-contents :info symbol :maxlines (or 1 5))))))
      (when symbol
        (message symbol-text))))

  (defun rtags-eldoc-mode ()
    (interactive)
    (setq-local eldoc-documentation-function #'rtags-eldoc-function)
    (eldoc-mode 1))

  (use-package helm-rtags
    :init
    (setq rtags-display-result-backend 'helm))

  (use-package rtags
    :init
    (add-to-list 'company-backends-c-mode-common 'company-rtags)
    (setq company-rtags-begin-after-member-access t)
    (setq rtags-autostart-diagnostics t) ;; added after checking rtags docs
    (rtags-diagnostics)
    (setq rtags-completions-enabled t)
    (add-hook 'c-mode-hook 'rtags-eldoc-mode)
    (add-hook 'c++-mode-hook 'rtags-eldoc-mode)
    (rtags-evil-standard-keybindings 'c-mode)
    (rtags-evil-standard-keybindings 'c++-mode)))

(defun tuhdo/init-company-rtags ()
  (use-package company-rtags)
  )
