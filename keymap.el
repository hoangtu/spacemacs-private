(spacemacs/toggle-holy-mode-off)
(global-set-key (kbd "C-M-f") 'sp-forward-sexp)
(global-set-key (kbd "C-M-b") 'sp-backward-sexp)

(global-set-key (kbd "M-k") 'next-line)
(global-set-key (kbd "M-i") 'previous-line)
(global-set-key (kbd "M-J") 'forward-sentence)
(global-set-key (kbd "M-L") 'backward-sentence)
(global-set-key (kbd "M-j") 'backward-char)
(global-set-key (kbd "M-l") 'forward-char)
(global-set-key (kbd "M-l") 'forward-char)
(global-set-key (kbd "M-u") 'backward-word)
(global-set-key (kbd "M-o") 'forward-word)
(global-set-key (kbd "M-h") 'mwim-end-of-code-or-line)
(global-set-key (kbd "M-H") 'mwim-beginning-of-code-or-line)
(global-set-key (kbd "M-J") 'ergoemacs-backward-open-bracket)
(global-set-key (kbd "M-L") 'ergoemacs-forward-close-bracket)
(global-set-key (kbd "M-U") 'sp-backward-sexp)
(global-set-key (kbd "M-O") 'sp-forward-sexp)
(global-set-key (kbd "C-M-u") 'beginning-of-defun)
(global-set-key (kbd "C-M-o") 'end-of-defun)
(global-set-key (kbd "M-K") 'golden-ratio-scroll-screen-up)
(global-set-key (kbd "M-I") 'golden-ratio-scroll-screen-down)
(global-set-key (kbd "M-t") (kbd "C-g"))
(global-set-key (kbd "M-;") 'mwim-end-of-code-or-line)
(global-set-key (kbd "M-h") 'mwim-beginning-of-code-or-line)
(global-set-key (kbd "M-n g") 'goto-line)
(global-set-key (kbd "M-n c") 'goto-char)
(global-set-key (kbd "M-n n") 'next-error)
(global-set-key (kbd "M-n p") 'previous-error)
(global-set-key (kbd "M-n TAB") 'move-to-column)
;; (global-set-key (kbd "M-s s") 'isearch-forward)
;; (global-set-key (kbd "M-s r") 'isearch-backward)
(global-set-key (kbd "M-.") 'isearch-forward)
(global-set-key (kbd "M-,") 'isearch-backward)
;; (global-set-key (kbd "C-v") 'helm-show-kill-ring)
;; (global-set-key (kbd "M-s M-s") 'isearch-forward-symbol-at-point)
(global-set-key (kbd "M-p") 'recenter-top-bottom)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-S-s") 'write-file)
;; (global-set-key (kbd "C-S") 'write-file)

(global-set-key (kbd "<M-return>") 'open-next-line)
(global-set-key (kbd "<M-S-return>") 'open-previous-line)
(global-set-key (kbd "M-'") 'er/expand-region)
(global-set-key (kbd "C-t") 'transpose-words)
(global-set-key (kbd "M-SPC") 'set-mark-command)
(global-set-key (kbd "M-v") 'evil-paste-after)
(global-set-key (kbd "M-V") 'evil-paste-before)
(global-set-key (kbd "M-x") 'evil-delete)
(global-set-key (kbd "M-c") 'evil-yank)
(global-set-key (kbd "M-w") 'helm-M-x)
(global-set-key (kbd "C-/") 'zzz-to-char)
(global-set-key (kbd "M-y") 'yas-expand)
(global-set-key (kbd "M-z") 'undo)
;; (global-set-key (kbd "M-d") 'delete-backward-char)
(global-set-key (kbd "M-f") 'delete-forward-char)
(global-set-key (kbd "M-r") 'kill-line)
(global-set-key (kbd "M-R") 'kill-whole-line)
(global-set-key (kbd "M-D") 'kill-sexp)
(global-set-key (kbd "M-F") 'sp-kill-hybrid-sexp)
(global-set-key (kbd "M-g") 'smart-comment)
;; (global-set-key (kbd "M-r") 'kill-word)
(define-key isearch-mode-map (kbd "<up>") 'isearch-repeat-backward)
(define-key isearch-mode-map (kbd "<down>") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "M-.") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "M-,") 'isearch-repeat-backward)
(define-key emacs-lisp-mode-map (kbd "C-e") 'eval-last-sexp)

(global-set-key (kbd "<f7>") 'compile)
(global-set-key (kbd "<f8>") 'recompile)
(global-set-key (kbd "<f9>") 'terminal-here)
;; (global-set-key (kbd "C-o") 'jump-to-register)
;; (global-set-key (kbd "C-k") 'point-to-register)
;; (global-set-key (kbd "C-n") 'copy-to-register)
(spacemacs/set-leader-keys
  "M-b" 'helm-mini
  "M-j" 'spacemacs/helm-jump-in-buffer
  "M-f" 'helm-find-files
  "xx" 'copy-to-register
  "xv" 'insert-register

  ;; convert edge list to graph
  "cf" 'edge-uv-to-graph
  "cg" 'edge-uvw-to-graph
  )

(define-key evil-motion-state-map (kbd "i") 'evil-previous-line)
(define-key evil-motion-state-map (kbd "k") 'evil-next-line)
(define-key evil-motion-state-map (kbd "j") 'evil-backward-char)
(define-key evil-motion-state-map (kbd "l") 'evil-forward-char)
(define-key evil-motion-state-map (kbd "u") 'evil-backward-word-begin)
(define-key evil-motion-state-map (kbd "o") 'evil-forward-word-end)
(define-key evil-motion-state-map (kbd "I") 'golden-ratio-scroll-screen-down)
(define-key evil-motion-state-map (kbd "K") 'golden-ratio-scroll-screen-up)
(define-key evil-motion-state-map (kbd "U") 'evil-backward-WORD-begin)
(define-key evil-motion-state-map (kbd "O") 'evil-forward-WORD-end)
(define-key evil-motion-state-map (kbd "h") 'mwim-beginning-of-code-or-line)
(define-key evil-motion-state-map (kbd ";") 'mwim-end-of-code-or-line)
(define-key evil-motion-state-map (kbd "\'") 'evil-repeat-find-char)
(define-key evil-motion-state-map (kbd "\\") 'evil-goto-mark-line)
(define-key evil-motion-state-map (kbd "w") 'sp-backward-sexp)
(define-key evil-motion-state-map (kbd "e") 'sp-forward-sexp)

(define-key evil-normal-state-map (kbd "i") 'evil-previous-line)
(define-key evil-normal-state-map (kbd "k") 'evil-next-line)
(define-key evil-normal-state-map (kbd "j") 'evil-backward-char)
(define-key evil-normal-state-map (kbd "l") 'evil-forward-char)
(define-key evil-normal-state-map (kbd "u") 'evil-backward-word-begin)
(define-key evil-normal-state-map (kbd "o") 'evil-forward-word-end)
(define-key evil-normal-state-map (kbd "I") 'golden-ratio-scroll-screen-down)
(define-key evil-normal-state-map (kbd "K") 'golden-ratio-scroll-screen-up)
(define-key evil-normal-state-map (kbd "U") 'evil-backward-WORD-begin)
(define-key evil-normal-state-map (kbd "O") 'evil-forward-WORD-end)
(define-key evil-normal-state-map (kbd "h") 'mwim-beginning-of-code-or-line)
(define-key evil-normal-state-map (kbd ";") 'mwim-end-of-code-or-line)
(define-key evil-normal-state-map (kbd "\'") 'evil-repeat-find-char)
(define-key evil-normal-state-map (kbd "\\") 'evil-goto-mark-line)
(define-key evil-normal-state-map (kbd "w") 'sp-backward-sexp)
(define-key evil-normal-state-map (kbd "e") 'sp-forward-sexp)

(define-key evil-normal-state-map (kbd "y") 'evil-insert)
(define-key evil-normal-state-map (kbd "z") 'undo)
(define-key evil-normal-state-map (kbd "y") 'evil-insert)
(define-key evil-normal-state-map (kbd "v") 'evil-paste-after)
(define-key evil-normal-state-map (kbd "V") 'evil-paste-before)
(define-key evil-normal-state-map (kbd "b") 'evil-visual-char)
(define-key evil-normal-state-map (kbd "B") 'evil-visual-line)
(define-key evil-normal-state-map (kbd "C-b") 'evil-visual-block)

(define-key evil-visual-state-map (kbd "i") 'evil-previous-line)
(define-key evil-visual-state-map (kbd "k") 'evil-next-line)
(define-key evil-visual-state-map (kbd "j") 'evil-backward-char)
(define-key evil-visual-state-map (kbd "l") 'evil-forward-char)
(define-key evil-visual-state-map (kbd "u") 'evil-backward-word-begin)
(define-key evil-visual-state-map (kbd "o") 'evil-forward-word-end)
(define-key evil-visual-state-map (kbd "I") 'golden-ratio-scroll-screen-down)
(define-key evil-visual-state-map (kbd "K") 'golden-ratio-scroll-screen-up)
(define-key evil-visual-state-map (kbd "U") 'evil-backward-WORD-begin)
(define-key evil-visual-state-map (kbd "O") 'evil-forward-WORD-end)
(define-key evil-visual-state-map (kbd "y") 'evil-insert)
(define-key evil-visual-state-map (kbd "h") 'mwim-beginning-of-code-or-line)
(define-key evil-normal-state-map (kbd "\'") 'evil-repeat-find-char)
(define-key evil-normal-state-map (kbd "\\") 'evil-goto-mark-line)
(define-key evil-visual-state-map (kbd ";") 'mwim-end-of-code-or-line)
(define-key evil-visual-state-map (kbd "w") 'sp-backward-sexp)
(define-key evil-visual-state-map (kbd "e") 'sp-forward-sexp)
