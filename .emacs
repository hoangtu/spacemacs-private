(defun my-minibuffer-setup-hook ()
  (setq gc-cons-threshold most-positive-fixnum))

(defun my-minibuffer-exit-hook ()
  (setq gc-cons-threshold 800000))

(add-hook 'minibuffer-setup-hook #'my-minibuffer-setup-hook)
(add-hook 'minibuffer-exit-hook #'my-minibuffer-exit-hook)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(unless (package-installed-p 'use-package)
  (package-install 'use-package))

(setq use-package-always-ensure t)

;; editing
;; enable evil-ergoemacs-mode
(defun zz-scroll-half-page (direction)
  "Scrolls half page up if `direction' is non-nil, otherwise will scroll half page down."
  (let ((opos (cdr (nth 6 (posn-at-point)))))
    ;; opos = original position line relative to window
    (move-to-window-line nil)  ;; Move cursor to middle line
    (if direction
        (recenter-top-bottom -1)  ;; Current line becomes last
      (recenter-top-bottom 0))  ;; Current line becomes first
    (move-to-window-line opos)))  ;; Restore cursor/point position

(defun zz-scroll-half-page-down ()
  "Scrolls exactly half page down keeping cursor/point position."
  (interactive)
  (zz-scroll-half-page nil))

(defun zz-scroll-half-page-up ()
  "Scrolls exactly half page up keeping cursor/point position."
  (interactive)
  (zz-scroll-half-page t))

(defun open-next-line (arg)
  "Move to the next line and then opens a line.
    See also `newline-and-indent'."
       (interactive "p")
       (end-of-line)
       (open-line arg)
       (next-line 1)
       (indent-according-to-mode))

(defun open-previous-line (arg)
  "Open a new line before the current one. 
     See also `newline-and-indent'."
       (interactive "p")
       (beginning-of-line)
       (open-line arg)
       (indent-according-to-mode))

(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
       (interactive "^p")
       (setq arg (or arg 1))

       ;; Move lines first
       (when (/= arg 1)
         (let ((line-move-visual nil))
           (forward-line (1- arg))))

       (let ((orig-point (point)))
         (back-to-indentation)
         (when (= orig-point (point))
           (move-beginning-of-line 1))))

(defun copy-whole-buffer ()
  (interactive)
  (save-excursion
    (kill-ring-save (point-min) (point-max))))

;; Variables
(setq cursor-type 't)
(setq server-raise-frame t)
(when (eq window-system 'w32)
  (setq w32-pipe-read-delay 0
        w32-pipe-buffer-size 50000))
(setq mac-option-modifier 'meta)
(setq-default line-spacing 0.1)
(setq dired-listing-switches "-lha")
(windmove-default-keybindings)

(delete-selection-mode 1)

(use-package avy
  :init
  (setq avy-background nil))
(use-package redo+
  :init
  (global-set-key (kbd "C-r") 'redo+))

(use-package yankpad)

(use-package smartparens
  :init
  (setq sp-highlight-pair-overlay nil)
  (smartparens-global-mode)
  (sp-pair "(" nil :wrap "M-)")
  ;; (sp-pair "{" "}" :wrap "M-}")
  (sp-pair "[" nil :wrap "M-]"))

(use-package evil
  :init
  (setq evil-move-cursor-back nil
        evil-want-find-undo t)
  (setq evil-insert-state-cursor '(box "skyblue")
	evil-normal-state-cursor '(box "goldenrod"))
  (add-hook 'prog-mode-hook 'evil-local-mode))

;; (use-package evil-leader
;;   :init
;;   (require 'evil-leader)
;;   (evil-leader/set-leader "<SPC>")
;;   (global-evil-leader-mode 1))

(use-package evil-nerd-commenter)

(use-package evil-textobj-anyblock
  :init
  (define-key evil-inner-text-objects-map "b" 'evil-textobj-anyblock-inner-block)
  (define-key evil-outer-text-objects-map "b" 'evil-textobj-anyblock-a-block))

(use-package evil-escape
  :init
  (global-unset-key (kbd "M-g"))
  ;; (setq-default evil-escape-key-sequence "jj"
  ;;               evil-esc-delay 0.2)
  (global-set-key (kbd "M-g") 'evil-escape))

(use-package evil-matchit
  :init
  (evil-matchit-mode 1))

(use-package evil-surround
  :init
  (global-evil-surround-mode 1))

(use-package evil-args
    :init
    ;; bind evil-args text objects
    (define-key evil-inner-text-objects-map "a" 'evil-inner-arg)
    (define-key evil-outer-text-objects-map "a" 'evil-outer-arg)

    ;; bind evil-forward/backward-args
    (define-key evil-normal-state-map ">" 'evil-forward-arg)  
    (define-key evil-normal-state-map "<" 'evil-backward-arg)
    (define-key evil-motion-state-map ">" 'evil-forward-arg)
    (define-key evil-motion-state-map "<" 'evil-backward-arg)

    ;; bind evil-jump-out-args
    (define-key evil-normal-state-map "go" 'evil-jump-out-args)
    )

(use-package dtrt-indent
  :defer t
  :init
  (add-hook 'prog-mode-hook 'dtrt-indent-mode))

(use-package move-dup
  :init
  (global-set-key (kbd "M-<up>") 'md/move-lines-up)
  (global-set-key (kbd "M-<down>") 'md/move-lines-down)
  (global-set-key (kbd "C-M-<up>") 'md/duplicate-up)
  (global-set-key (kbd "C-M-<down>") 'md/duplicate-down))

(use-package region-state
  :init
  (region-state-mode 1))

;; utilities
(defun indent-buffer ()
  (interactive)
  (save-excursion
    (indent-region (point-min) (point-max) nil)))

(use-package nlinum
  :init
  (add-hook 'prog-mode-hook 'nlinum-mode)
  (setq nlinum-format " %d ")
  (use-package nlinum-relative
    :init
    (add-hook 'prog-mode-hook 'nlinum-relative-on)
    (setq nlinum-relative-redisplay-delay 0.1)))

(use-package magit
  :defer t
  :init
  (global-set-key (kbd "C-c g s") 'magit-status))

(use-package helm
  :init
  (helm-mode)
  ;;(helm-projectile-on)
  ;; (global-set-key (kbd "C-c h g") 'spacemacs/helm-google-suggest)
  ;; (global-set-key (kbd "C-c h w") 'spacemacs/helm-wikipedia-suggest)
  (global-set-key (kbd "C-x b") 'helm-buffers-list)
  (global-set-key (kbd "C-x C-f") 'helm-find-files)
  ;; (setq helm-projectile-sources-list '(helm-source-projectile-files-list
  ;;                                      helm-source-projectile-directories-list
  ;;                                      helm-source-projectile-projects
  ;;                                      helm-source-recentf
  ;;                                      helm-source-file-cache
  ;;                                      ;; helm-source-files-in-current-dir
  ;;                                      ;; helm-source-locate
  ;;                                      ))
  (defun helm-hide-minibuffer-maybe ()
    (when (with-helm-buffer helm-echo-input-in-header-line)
      (let ((ov (make-overlay (point-min) (point-max) nil nil t)))
	(overlay-put ov 'window (selected-window))
	(overlay-put ov 'face (let ((bg-color (face-background 'default nil)))
				`(:background ,bg-color :foreground ,bg-color)))
	(setq-local cursor-type nil))))
  (add-hook 'helm-minibuffer-set-up-hook 'helm-hide-minibuffer-maybe)
  (setq helm-google-suggest-use-curl-p t
        helm-scroll-amount 4 ; scroll 4 lines other window using M-<next>/M-<prior>
        ;; helm-quick-update t ; do not display invisible candidates
        helm-ff-search-library-in-sexp t ; search for library in `require' and `declare-function' sexp.

        ;; you can customize helm-do-grep to execute ack-grep
        ;; helm-grep-default-command "ack-grep -Hn --smart-case --no-group --no-color %e %p %f"
        ;; helm-grep-default-recurse-command "ack-grep -H --smart-case --no-group --no-color %e %p %f"
        helm-split-window-in-side-p t ;; open helm buffer inside current window, not occupy whole other window

        helm-echo-input-in-header-line t

        ;; helm-candidate-number-limit 500 ; limit the number of displayed canidates
        helm-ff-file-name-history-use-recentf t
        helm-move-to-line-cycle-in-source t ; move to end or beginning of source when reaching top or bottom of source.
        helm-buffer-skip-remote-checking t

        helm-mode-fuzzy-match t

        helm-buffers-fuzzy-matching t ; fuzzy matching buffer names when non-nil
                                        ; useful in helm-mini that lists buffers
        helm-org-headings-fontify t
        ;; helm-find-files-sort-directories t
        ;; ido-use-virtual-buffers t
        ;; helm-semantic-fuzzy-match t
        ;; helm-M-x-fuzzy-match t
        ;; helm-imenu-fuzzy-match t
        ;; helm-lisp-fuzzy-completion t
        ;; helm-apropos-fuzzy-match t
        helm-buffer-skip-remote-checking t
        ;; helm-locate-fuzzy-match t
        helm-display-header-line nil)

  (setq helm-ag-insert-at-point 'symbol)

  (setq helm-autoresize-max-height 0)
  (setq helm-autoresize-min-height 20)
  (helm-autoresize-mode 1)

  (with-eval-after-load 'helm-semantic
    (push '(c-mode . semantic-format-tag-summarize) helm-semantic-display-style)
    (push '(c++-mode . semantic-format-tag-summarize) helm-semantic-display-style))
  (define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action) ; rebihnd tab to do persistent action
  (define-key helm-map (kbd "C-i") 'helm-execute-persistent-action) ; make TAB works in terminal
  (define-key helm-map (kbd "C-z")  'helm-select-action) ; list actions using C-z

  (define-key helm-grep-mode-map (kbd "<return>")  'helm-grep-mode-jump-other-window)
  (define-key helm-grep-mode-map (kbd "n")  'helm-grep-mode-jump-other-window-forward)
  (define-key helm-grep-mode-map (kbd "p")  'helm-grep-mode-jump-other-window-backward)
  ;; (define-key global-map [remap find-tag] 'helm-etags-select)
  ;; (define-key global-map [remap jump-to-register] 'helm-register)
  (define-key global-map [remap list-buffers] 'helm-buffers-list)
  ;; (define-key global-map [remap find-tag] 'helm-gtags-select)
  (add-hook 'ielm-mode-hook
            #'(lambda ()
                (define-key ielm-map [remap completion-at-point] 'helm-lisp-completion-or-file-name-at-point)))
  )

(use-package eyebrowse
  :bind (("C-1" . eyebrowse-switch-to-window-config-1)
         ("C-2" . eyebrowse-switch-to-window-config-2)
         ("C-3" . eyebrowse-switch-to-window-config-3)
         ("C-4" . eyebrowse-switch-to-window-config-4)
         ("C-5" . eyebrowse-switch-to-window-config-5)
         ("C-6" . eyebrowse-switch-to-window-config-6)
         ("C-7" . eyebrowse-switch-to-window-config-7)
         ("C-8" . eyebrowse-switch-to-window-config-8)
         ("C-9" . eyebrowse-switch-to-window-config-9)
         ("C-0" . eyebrowse-switch-to-window-config-0))
  :config
  (eyebrowse-mode)
  (setq eyebrowse-new-workspace t))

(use-package electric-operator
  :init
  (add-hook 'c-mode-hook 'electric-operator-mode)
  (add-hook 'c++-mode-hook 'electric-operator-mode)
  :config
  
  (dolist (m '(c-mode c++-mode))
    (electric-operator-add-rules-for-mode m (cons "){" ") {"))
    (electric-operator-add-rules-for-mode m (cons "--" "--"))
    (electric-operator-add-rules-for-mode m (cons "++" "++"))
    ;; (electric-operator-add-rules-for-mode m (cons ">>" ">> "))
    (electric-operator-add-rules-for-mode m (cons " >" " > "))
    (electric-operator-add-rules-for-mode m (cons " <" " < "))
    ;; (electric-operator-add-rules-for-mode m (cons "return" "return "))
    (electric-operator-add-rules-for-mode m (cons "cin" "cin "))
    ;; (electric-operator-add-rules-for-mode m (cons "+" nil))
    ;; (electric-operator-add-rules-for-mode m (cons "-" nil))
    ;; (electric-operator-add-rules-for-mode m (cons "*" nil))
    ;; (electric-operator-add-rules-for-mode m (cons "/" nil))
    (electric-operator-add-rules-for-mode m
                                          (cons "for" "for ")
                                          (cons "rep" "rep ")
                                          (cons "repc" "repc ")
                                          (cons "repi" "repi ")
                                          (cons "repj" "repj ")
                                          (cons "repk" "repk ")
                                          (cons "loop" "loop ")
                                          ;; (cons "else" " else ")
                                          (cons "until" "until ")
                                          (cons "((" " ((")
                                          (cons "if(" "if (")
                                          ;; (cons " ( " " ( ")
                                          ;; (cons ")" ") ")
                                          (cons "{{" " {{")
                                          ;; (cons "{" " {")
                                          (cons " { " " { ")
                                          (cons "}" "} ")
                                          (cons "unless" "unless ")
                                          (cons "while" "while ")
                                          (cons ";" "; ")
                                          )
    )
  (define-key c++-mode-map (kbd "M-j") 'backward-char))

;; look and feel
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)
(set-frame-font "Menlo-12")
(use-package oldlace-theme)
(set-face-attribute 'font-lock-keyword-face nil
                    :foreground nil
                    ;; :weight 'bold
                    )
(set-face-attribute 'font-lock-type-face nil
                    :foreground nil
                    :background nil
                    ;; :underline t
                    ;; :weight 'bold
                    )
(set-face-attribute 'font-lock-function-name-face nil
                    ;; :foreground (face-attribute 'font-lock-keyword-face :foreground)
                    :foreground nil
                    :background (face-attribute 'font-lock-keyword-face :background)
                    :weight 'bold
                    )
(set-face-attribute 'font-lock-preprocessor-face nil
                    ;; :foreground (face-attribute 'font-lock-keyword-face :foreground)
                    ;; :foreground (face-attribute 'font-lock-keyword-face :background)
                    :foreground nil
                    :weight 'bold
                    :inherit nil
                    )

(use-package golden-ratio-scroll-screen
  :defer t
  :init
  (global-set-key [remap scroll-down-command] 'golden-ratio-scroll-screen-down)
  (global-set-key [remap scroll-up-command] 'golden-ratio-scroll-screen-up)
  ;; (global-set-key [remap evil-scroll-down] 'golden-ratio-scroll-screen-up)
  ;; (global-set-key [remap evil-scroll-up] 'golden-ratio-scroll-screen-down)
  (setq golden-ratio-scroll-highlight-flag nil))

(use-package color-identifiers-mode)

;; C++
(defun my-recompile()
  (interactive)
  (save-buffer)
  (call-interactively 'recompile))

(defun indent-next-line (ignore)
  (save-excursion
    (next-line)
    (indent-region (line-beginning-position) (line-end-position))))
(advice-add 'open-line :after 'indent-next-line)

(defun my-jump-debug-enable ()
  (interactive)
  (jump-to-string "DEBUG" t)
  (end-of-line))

(defun toggle-debug-enable ()
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (if (search-forward "DEBUG 1" nil t)
        (progn
          (backward-char)
          (delete-char 1)
          (insert "0")
          (beginning-of-buffer)
          (insert "#define NDEBUG")
          (newline)
          (message "DEBUG disabled"))
      (beginning-of-line)
      (when (search-forward "NDEBUG" nil t)
        (kill-whole-line))
      (search-forward "DEBUG 0")
      (backward-char)
      (delete-char 1)
      (insert "1")
      (message "DEBUG enabled")
      )))

(defun my-jump-var-declaration (&optional not-forward)
  (interactive)
  (jump-to-string "VARIABLE DECLARATION" not-forward))

(defun my-jump-defvars ()
  (interactive)
  (jump-to-string "defvars"))

(defun my-jump-main-vars ()
  (interactive)
  (my-jump-main)
  (beginning-of-defun)
  (search-forward "Variables")
  (beginning-of-line))

(defun my-jump-input ()
  (interactive)
  (my-jump-main)
  (search-forward "Input")
  (beginning-of-line))

(defun my-jump-output ()
  (interactive)
  (my-jump-main)
  (search-forward "Output")
  (beginning-of-line))

(defun my-jump-solve ()
  (interactive)
  (my-jump-main)
  (search-forward "Solve")
  (beginning-of-line))

(defun my-jump-main ()
  (interactive)
  (jump-to-string "main"))

(defun jump-to-string (string &optional not-forward)
  (interactive)
  (beginning-of-buffer)
  (search-forward string)
  (beginning-of-line)
  (unless not-forward
    (forward-paragraph))
  ;; (next-line line-down)
  )

(defun my-move-var-to-global-scope (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-region (line-beginning-position) (line-end-position)))
  (save-excursion
    (my-jump-var-declaration t)
    (search-forward "***********")
    (next-line)
    (beginning-of-line)
    (yank)
    (newline-and-indent)))

(defun my-move-var-to-top-local-function (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-region (line-beginning-position) (line-end-position)))
  (save-excursion
    (my-jump-main-vars)
    (next-line)
    (beginning-of-line)
    (yank)
    (newline-and-indent)))

(defun my-move-line-above-function (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-region (line-beginning-position) (line-end-position)))
  (save-excursion
    (beginning-of-defun)
    (previous-line)
    (beginning-of-line)
    (newline-and-indent)
    (yank)))

(defun my-move-var-to-higher-scope (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-whole-line))
  (save-excursion
    (cedit-up-block-backward)
    (beginning-of-line)
    (yank)
    (newline-and-indent)))


(defun enable-coding-practice ()
  (color-identifiers-mode 1)
  ;; (smartparens-mode -1)
  (electric-layout-mode 1)
  ;; (add-to-list 'electric-layout-rules '(?{ . after))
  (electric-indent-mode -1)
  (setq-default c-auto-newline nil)
  ;; (auto-highlight-symbol-mode -1)
  ;; (semantic-idle-summary-mode 1)
  ;; (company-mode -1)
  ;; (yas-minor-mode -1)
  ;; (font-lock-mode -1)
  ;; (when (eq major-mode 'c++-mode)
  ;;   (setq-local flycheck-clang-args "-std=c++11"))
  ;; (define-key evil-normal-state-map (kbd "b") 'cedit-up-block-backward)
  ;; (dolist (m (list c-mode-map c++-mode-map))
  ;; (define-key m (kbd "M-h") 'c-beginning-of-statement)
  ;; (define-key m (kbd "M-;") 'c-end-of-statement)
  ;; (define-key m (kbd "M-f") 'c-electric-delete-forward)
  ;; (define-key m (kbd "C-e") 'smarter-move-beginning-of-line))
  (local-set-key (kbd "<backtab>") 'company-irony-c-headers)
  (c-toggle-auto-hungry-state 1)
  ;; (setq-default c-electric-flag t)
  )

(defun my-c-style ()
  (setq c-basic-offset 4
        c-hungry-delete-key t
        c-default-style "linux"))

(add-hook 'c++-mode-hook
          '(lambda ()
             (require 'yankpad)
             (c-set-style "stroustrup")
             (setq c-basic-offset 2)))

(defconst prettify-greek-lower
  '(("alpha" . ?α)
    ("beta" . ?β)
    ("gamma" . ?γ)
    ("delta" . ?δ)
    ("epsilon" . ?ε)
    ("zeta" . ?ζ)
    ("eta" . ?η)
    ("theta" . ?θ)
    ;; ("iota" . ?ι)
    ("kappa" . ?κ)
    ("lambda" . ?λ)
    ;; ("mu" . ?μ)
    ;; ("nu" . ?ν)
    ;; ("xi" . ?ξ)
    ;; ("omicron" . ?ο)
    ;; ("pi" . ?π)
    ;; ("rho" . ?ρ)
    ;; ("sigma" . ?σ)
    ;; ("tau" . ?τ)
    ;; ("upsilon" . ?υ)
    ;; ("phi" . ?φ)
    ;; ("chi" . ?χ)
    ;; ("psi" . ?ψ)
    ("omega" . ?ω))
  "Prettify rules for lower case greek letters.")

(defun cc-pretify ()
  ;; (push '("][" . ?,) prettify-symbols-alist)
  (push '("INF" . ?∞) prettify-symbols-alist)
  (push '("sqrt" . ?√) prettify-symbols-alist)
  (push '("NULL" . ?∅) prettify-symbols-alist)
  (push '("true" . ?⊨) prettify-symbols-alist)
  (push '("false" . ?⊭) prettify-symbols-alist)
  ;; (push '("unless" . ?⁈) prettify-symbols-alist)
  ;; (push '("if" . ?︖) prettify-symbols-alist)
  ;; (push '("while" . ?○) prettify-symbols-alist)
  ;; (push '("until" . ?●) prettify-symbols-alist)
  ;; (push '("loop" . ?ƪ) prettify-symbols-alist)
  (push '("->" . ?➛) prettify-symbols-alist)
  ;; (push '("=" . ?←) prettify-symbols-alist)
  (push '("!=" . ?≠) prettify-symbols-alist)
  (push '("&&" . ?⋏) prettify-symbols-alist)
  (push '("||" . ?⋎) prettify-symbols-alist)
  ;; (push '("repc" . ?∀) prettify-symbols-alist)
  ;; (push '("rep" . ?∀) prettify-symbols-alist)
  ;; (push '("for" . ?∀) prettify-symbols-alist)
  ;; (dolist (v exist-symbols)
  ;;   (push (cons v ?∃) prettify-symbols-alist))
  (dolist (v prettify-greek-lower)
    (push v prettify-symbols-alist))
  ;; (dolist (v prettify-greek-upper)
  ;;   (push v prettify-symbols-alist))
  (push '("!" . ?¬) prettify-symbols-alist)
  (push '("!=" . ?≠) prettify-symbols-alist)
  ;; (push '("=" . ?←) prettify-symbols-alist)
  (push '(">=" . ?≥) prettify-symbols-alist)
  (push '("<=" . ?≤) prettify-symbols-alist)
  ;; (push '("==" . ?=) prettify-symbols-alist)
  (push '(">=" . ?≥) prettify-symbols-alist)
  (push '(">=" . ?≥) prettify-symbols-alist)
  )

(defun prettify-cc-modes ()
  (cc-pretify)
  (prettify-symbols-mode))

(add-hook 'c-mode-hook 'enable-coding-practice)
(add-hook 'c++-mode-hook 'enable-coding-practice)
(add-hook 'c-mode-hook 'my-c-style)
(add-hook 'c++-mode-hook 'my-c-style)
(add-hook 'c++-mode-hook 'prettify-cc-modes)
(add-hook 'c-mode-hook 'prettify-cc-modes)

(font-lock-add-keywords 'c++-mode
                        '(("vvpii" . font-lock-type-face)
                          ("vvi" . font-lock-type-face)
                          ("pii" . font-lock-type-face)
                          ("vii" . font-lock-type-face)
                          ("vi" . font-lock-type-face)
                          ("unless" . font-lock-keyword-face)
                          ("until" . font-lock-keyword-face)
                          ("all" . font-lock-keyword-face)
                          ("dbg" . font-lock-keyword-face)
                          ("elif" . font-lock-keyword-face)
                          ("loop" . font-lock-keyword-face)
                          ("fastio" . font-lock-keyword-face)
                          ("rrepc" . font-lock-keyword-face)
                          ("mapc" . font-lock-keyword-face)
                          ("#define" . font-lock-keyword-face)
                          ("rrepc" . font-lock-keyword-face)
                          ("rrep" . font-lock-keyword-face)
                          ("repc" . font-lock-keyword-face)
                          ("repi" . font-lock-keyword-face)
                          ("repj" . font-lock-keyword-face)
                          ("repk" . font-lock-keyword-face)
                          ("rep" . font-lock-keyword-face)))

(use-package cedit
  :init
  (global-set-key (kbd "M-[") 'cedit-wrap-brace)
  (with-eval-after-load 'cc-mode
    (dolist (m (list c-mode-map c++-mode-map))
      (define-key m (kbd "M-U") 'cedit-up-block-backward)
      (define-key m (kbd "M-O") 'cedit-up-block-forward))))

;; key bindings
(global-unset-key (kbd "<f2>"))
(global-unset-key (kbd "M-s"))
(global-set-key (kbd "M-s") 'isearch-forward)
(global-set-key (kbd "<f2>") 'helm-M-x)
(global-set-key (kbd "C-M-f") 'sp-forward-sexp)
(global-set-key (kbd "C-M-b") 'sp-backward-sexp)
(global-set-key (kbd "M-,") 'evil-backward-arg)
(global-set-key (kbd "M-.") 'evil-backward-arg)

(global-set-key (kbd "M-k") 'next-line)
(global-set-key (kbd "M-i") 'previous-line)
;; (global-set-key (kbd "M-J") 'forward-sentence)
;; (global-set-key (kbd "M-L") 'backward-sentence)
(global-set-key (kbd "M-j") 'backward-char)
(global-set-key (kbd "M-J") 'evil-join)
(global-set-key (kbd "M-l") 'forward-char)
(global-set-key (kbd "M-l") 'forward-char)
(global-set-key (kbd "M-u") 'backward-word)
(global-set-key (kbd "M-o") 'forward-word)
(global-set-key (kbd "M-h") 'smarter-move-beginning-of-line)
;; (global-set-key (kbd "M-J") 'ergoemacs-backward-open-bracket)
;; (global-set-key (kbd "M-L") 'ergoemacs-forward-close-bracket)
(global-set-key (kbd "M-U") 'sp-backward-sexp)
(global-set-key (kbd "M-O") 'sp-forward-sexp)
(global-set-key (kbd "C-M-u") 'beginning-of-defun)
(global-set-key (kbd "C-M-o") 'end-of-defun)
(global-set-key (kbd "M-K") 'golden-ratio-scroll-screen-up)
(global-set-key (kbd "M-I") 'golden-ratio-scroll-screen-down)
;; (global-set-key (kbd "M-t") (kbd "C-g"))
(global-set-key (kbd "M-;") 'end-of-line)
(global-set-key (kbd "M-b") 'avy-goto-word-or-subword-1)
(global-set-key (kbd "M-n g") 'goto-line)
(global-set-key (kbd "M-n c") 'goto-char)
(global-set-key (kbd "M-n n") 'next-error)
(global-set-key (kbd "M-n p") 'previous-error)
(global-set-key (kbd "M-n TAB") 'move-to-column)
;; (global-set-key (kbd "M-s s") 'isearch-forward)
;; (global-set-key (kbd "M-s r") 'isearch-backward)
;; (global-set-key (kbd "M-.") 'isearch-forward)
;; (global-set-key (kbd "M-,") 'isearch-backward)
;; (global-set-key (kbd "C-v") 'helm-show-kill-ring)
;; (global-set-key (kbd "M-s M-s") 'isearch-forward-symbol-at-point)
(global-set-key (kbd "M-p") 'recenter-top-bottom)
(global-set-key (kbd "C-s") 'save-buffer)
(global-set-key (kbd "C-S-s") 'write-file)
(global-set-key (kbd "C-x k") 'kill-this-buffer)
;; (global-set-key (kbd "C-S") 'write-file)

(global-set-key (kbd "<M-return>") 'open-next-line)
(global-set-key (kbd "<M-S-return>") 'open-previous-line)
(global-set-key (kbd "M-'") 'er/expand-region)
(global-set-key (kbd "C-t") 'transpose-words)
(global-set-key (kbd "M-SPC") 'set-mark-command)
(global-set-key (kbd "M-v") 'yank)
(global-set-key (kbd "M-V") 'evil-paste-after)
(global-set-key (kbd "M-x") 'evil-delete)
(global-set-key (kbd "M-X") 'evil-delete-line)
(global-set-key (kbd "M-c") 'evil-yank)
(global-set-key (kbd "M-w") 'evil-change)
(global-set-key (kbd "C-/") 'zzz-to-char)
(global-set-key (kbd "M-y") 'set-mark-command)
(global-set-key (kbd "M-\\") 'my-snippet-expand)
(global-set-key (kbd "M-z") 'undo)
(global-set-key (kbd "C-r") 'redo)
(global-set-key (kbd "M-d") 'kill-word)
(global-set-key (kbd "M-f") 'delete-forward-char)
(global-set-key (kbd "M-t") 'helm-M-x)
(global-set-key (kbd "C-c r") 'helm-recentf)
(global-set-key (kbd "M-r") 'isearch-backward)
(global-set-key (kbd "C-c ;") 'evilnc-comment-or-uncomment-lines)
(define-key isearch-mode-map (kbd "<up>") 'isearch-repeat-backward)
(define-key isearch-mode-map (kbd "<down>") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "M-.") 'isearch-repeat-forward)
(define-key isearch-mode-map (kbd "M-,") 'isearch-repeat-backward)
(define-key emacs-lisp-mode-map (kbd "C-e") 'eval-last-sexp)

(global-set-key (kbd "C-c i") 'indent-buffer)
(global-set-key (kbd "<f5>") 'compile-command-auto)
(global-set-key (kbd "<f6>") 'my-recompile)
(global-set-key (kbd "<f7>") 'toggle-debug-enable)
(global-set-key (kbd "<f8>") 'copy-whole-buffer)
(global-set-key (kbd "<f9>") 'my-magit-stage-all-and-commit)
(global-set-key (kbd "<f10>") 'terminal-here)
;; (global-set-key (kbd "C-o") 'jump-to-register)
;; (global-set-key (kbd "C-k") 'point-to-register)
;; (global-set-key (kbd "C-n") 'copy-to-register)
(define-key evil-motion-state-map (kbd "i") 'evil-previous-line)
(define-key evil-motion-state-map (kbd "k") 'evil-next-line)
(define-key evil-motion-state-map (kbd "j") 'evil-backward-char)
(define-key evil-motion-state-map (kbd "l") 'evil-forward-char)
(define-key evil-motion-state-map (kbd "u") 'backward-word)
(define-key evil-motion-state-map (kbd "o") 'forward-word)
(define-key evil-motion-state-map (kbd "I") 'golden-ratio-scroll-screen-down)
(define-key evil-motion-state-map (kbd "K") 'golden-ratio-scroll-screen-up)
(define-key evil-motion-state-map (kbd "U") 'sp-backward-sexp)
(define-key evil-motion-state-map (kbd "O") 'sp-forward-sexp)
(define-key evil-motion-state-map (kbd "h") 'smarter-move-beginning-of-line)
(define-key evil-motion-state-map (kbd ";") 'evil-end-of-line)
(define-key evil-motion-state-map (kbd "\'") 'evil-repeat-find-char)
(define-key evil-motion-state-map (kbd "\\") 'evil-goto-mark-line)
;; (define-key evil-motion-state-map (kbd "w") 'sp-backward-sexp)
;; (define-key evil-motion-state-map (kbd "e") 'sp-forward-sexp)
(define-key evil-motion-state-map (kbd "b") 'evil-avy-goto-word-or-subword-1)
;; (define-key evil-motion-state-map (kbd "[[") 'beginning-of-defun)
;; (define-key evil-motion-state-map (kbd "]]") 'end-of-defun)

(define-key evil-normal-state-map (kbd "U") 'sp-backward-sexp)
(define-key evil-normal-state-map (kbd "O") 'sp-forward-sexp)
(define-key evil-normal-state-map (kbd "C-d") 'zz-scroll-half-page-down)
(define-key evil-normal-state-map (kbd "C-u") 'zz-scroll-half-page-up)
(define-key evil-normal-state-map (kbd "i") 'evil-previous-line)
(define-key evil-normal-state-map (kbd "k") 'evil-next-line)
(define-key evil-normal-state-map (kbd "j") 'evil-backward-char)
(define-key evil-normal-state-map (kbd "l") 'evil-forward-char)
(define-key evil-normal-state-map (kbd "u") 'backward-word)
(define-key evil-normal-state-map (kbd "o") 'forward-word)
(define-key evil-normal-state-map (kbd "I") 'golden-ratio-scroll-screen-down)
(define-key evil-normal-state-map (kbd "K") 'golden-ratio-scroll-screen-up)
;; (define-key evil-normal-state-map (kbd "U") 'evil-backward-WORD-begin)
;; (define-key evil-normal-state-map (kbd "O") 'evil-forward-WORD-end)
(define-key evil-normal-state-map (kbd "h") 'smarter-move-beginning-of-line)
(define-key evil-normal-state-map (kbd ";") 'evil-end-of-line)
(define-key evil-normal-state-map (kbd "\'") 'evil-repeat)
(define-key evil-normal-state-map (kbd "\\") 'evil-goto-mark-line)
(define-key evil-normal-state-map (kbd ".") 'evil-repeat-find-char)

;; (define-key evil-normal-state-map (kbd "O") 'sp-forward-sexp)
(define-key evil-normal-state-map (kbd "w") 'evil-change)
(define-key evil-normal-state-map (kbd "e") 'evil-insert)
(define-key evil-normal-state-map (kbd "z") 'undo)
(define-key evil-normal-state-map (kbd "c") 'evil-yank)
(define-key evil-normal-state-map (kbd "v") 'evil-paste-before)
(define-key evil-normal-state-map (kbd "V") 'evil-paste-after)
(define-key evil-normal-state-map (kbd "y") 'evil-visual-char)
(define-key evil-normal-state-map (kbd "Y") 'evil-visual-line)
(define-key evil-normal-state-map (kbd "C-y") 'evil-visual-block)
;; (define-key evil-normal-state-map (kbd "p") 'recenter-top-bottom)
(define-key evil-normal-state-map (kbd "b") 'evil-avy-goto-word-or-subword-1)
;; (define-key evil-normal-state-map (kbd "b") 'evil-escape)

(define-key evil-operator-state-map "e" evil-inner-text-objects-map)
(define-key evil-visual-state-map "e" evil-inner-text-objects-map)
(define-key evil-visual-state-map (kbd "i") 'evil-previous-line)
(define-key evil-visual-state-map (kbd "k") 'evil-next-line)
(define-key evil-visual-state-map (kbd "j") 'evil-backward-char)
(define-key evil-visual-state-map (kbd "l") 'evil-forward-char)
(define-key evil-visual-state-map (kbd "u") 'evil-backward-word-begin)
(define-key evil-visual-state-map (kbd "o") 'evil-forward-word-end)
(define-key evil-visual-state-map (kbd "I") 'evil-scroll-up)
(define-key evil-visual-state-map (kbd "K") 'evil-scroll-down)
;; (define-key evil-visual-state-map (kbd "U") 'evil-backward-WORD-begin)
;; (define-key evil-visual-state-map (kbd "O") 'evil-forward-WORD-end)
(define-key evil-visual-state-map (kbd "w") 'evil-change)
(define-key evil-visual-state-map (kbd "E") 'evil-insert)
(define-key evil-visual-state-map (kbd "h") 'smarter-move-beginning-of-line)
(define-key evil-visual-state-map (kbd "\'") 'evil-repeat)
(define-key evil-visual-state-map (kbd "\\") 'evil-goto-mark-line)
(define-key evil-visual-state-map (kbd ".") 'evil-repeat-find-char)
(define-key evil-visual-state-map (kbd ";") 'evil-end-of-line)
(define-key evil-visual-state-map (kbd "U") 'sp-backward-sexp)
(define-key evil-visual-state-map (kbd "O") 'sp-forward-sexp)
(define-key evil-visual-state-map (kbd "M-x") 'evil-delete)
(define-key evil-visual-state-map (kbd "b") 'evil-avy-goto-word-or-subword-1)

(with-eval-after-load 'helm
  (define-key helm-map (kbd "M-i") 'helm-previous-line)
  (define-key helm-map (kbd "M-k") 'helm-next-line)
  (define-key helm-map (kbd "M-I") 'helm-previous-page)
  (define-key helm-map (kbd "M-K") 'helm-next-page)
  (with-eval-after-load 'helm-files
    (define-key helm-find-files-map (kbd "M-i") 'helm-previous-line)
    (define-key helm-find-files-map (kbd "M-k") 'helm-next-line)
    (define-key helm-find-files-map (kbd "M-I") 'helm-previous-page)
    (define-key helm-find-files-map (kbd "M-K") 'helm-next-page)))

(with-eval-after-load 'compile
  (define-key compilation-mode-map (kbd "h") 'smarter-move-beginning-of-line))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (yankpad redo+ color-identifiers-mode color-identifers-mode evil-args use-package smartparens rich-minority region-state oldlace-theme nlinum-relative move-dup magit helm golden-ratio-scroll-screen eyebrowse evil-textobj-anyblock evil-surround evil-nerd-commenter evil-matchit evil-leader evil-escape electric-operator dtrt-indent cedit avy))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
