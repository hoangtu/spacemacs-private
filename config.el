;; (spacemacs|defvar-company-backends verilog-mode)

;; color utilities
(defun spacemacs//check-color (color)
  (cond
   ((> color 1.0)
    1.0)
   ((< color 0.0)
    0.0)
   (t color)))

(defun spacemacs//rgb-add (hex-str number)
  (let* ((rgb-values (color-name-to-rgb hex-str))
         (red (spacemacs//check-color (+ (car rgb-values) number)))
         (green (spacemacs//check-color (+ (cadr rgb-values) number)))
         (blue (spacemacs//check-color (+ (caddr rgb-values) number))))
    (color-rgb-to-hex red green blue)))

;; when we press ':' character, it runs `asm-colon' command in asm-mode.
;; The command automatically removes any indentation, since every
;; non-whitespace character before a colon is a label in asm, and label
;; has to be at the beginning of a line. However, the problem is that when
;; deleting indentation, trailing spaces are left between the colon and
;; point.
;;
;; These functions solve that problem. First, check whether we have any
;; space or tab after point. If so, don't do anything becuase the spaces are
;; there intentionally. If not, we delete all trailing spaces between
;; point and colon.
(defvar asm-colon-has-space nil)
(defun asm-colon-check-space ()
  (setq asm-colon-has-space nil)
  (when (member (string (char-after)) '(" " "\t"))
    (setq asm-colon-has-space t)))
(defun asm-colon-delete-spaces ()
  (unless asm-colon-has-space
    (call-interactively 'delete-horizontal-space)))
(advice-add 'asm-colon :before 'asm-colon-check-space)
(advice-add 'asm-colon :after 'asm-colon-delete-spaces)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; function to wrap blocks of text in org templates                       ;;
;; e.g. latex or src etc                                                  ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun org-begin-template ()
  "Make a template at point."
  (interactive)
  (if (org-at-table-p)
      (call-interactively 'org-table-rotate-recalc-marks)
    (let* ((choices '(("s" . "SRC")
                      ("e" . "EXAMPLE")
                      ("q" . "QUOTE")
                      ("v" . "VERSE")
                      ("c" . "CENTER")
                      ("l" . "LaTeX")
                      ("h" . "HTML")
                      ("a" . "ASCII")))
           (key
            (key-description
             (vector
              (read-key
               (concat (propertize "Template type: " 'face 'minibuffer-prompt)
                       (mapconcat (lambda (choice)
                                    (concat (propertize (car choice) 'face 'font-lock-doc-face)
                                            ": "
                                            (cdr choice)))
                                  choices
                                  ", ")))))))
      (let ((result (assoc key choices)))
        (when result
          (let ((choice (cdr result)))
            (cond
             ((region-active-p)
              (let ((start (region-beginning))
                    (end (region-end)))
                (goto-char end)
                (insert "#+END_" choice "\n")
                (goto-char start)
                (insert "#+BEGIN_" choice "\n")))
             (t
              (insert "#+BEGIN_" choice "\n")
              (save-excursion (insert "#+END_" choice))))))))))


(with-eval-after-load 'dired
  (define-key dired-mode-map "e" 'ora-ediff-files))
;; -*- lexical-binding: t -*-
(defun ora-ediff-files ()
  (interactive)
  (let ((files (dired-get-marked-files))
        (wnd (current-window-configuration)))
    (if (<= (length files) 2)
        (let ((file1 (car files))
              (file2 (if (cdr files)
                         (cadr files)
                       (read-file-name
                        "file: "
                        (dired-dwim-target-directory)))))
          (if (file-newer-than-file-p file1 file2)
              (ediff-files file2 file1)
            (ediff-files file1 file2))
          (add-hook 'ediff-after-quit-hook-internal
                    (lambda ()
                      (setq ediff-after-quit-hook-internal nil)
                      (set-window-configuration wnd))))
      (error "no more than 2 files should be marked"))))

;; Behave like vi's o command
(defun open-next-line (arg)
  "Move to the next line and then opens a line.
    See also `newline-and-indent'."
       (interactive "p")
       (end-of-line)
       (open-line arg)
       (next-line 1)
       (indent-according-to-mode))

;; Behave like vi's O command
(defun open-previous-line (arg)
  "Open a new line before the current one. 
     See also `newline-and-indent'."
       (interactive "p")
       (beginning-of-line)
       (open-line arg)
       (indent-according-to-mode))


;; Functions for opening a terminal in cwd
;; copied from: https://github.com/davidshepherd7/terminal-here
(defgroup terminal-here nil
  "Open external terminal emulators in the current buffer's directory."
  :group 'external
  :prefix "terminal-here-")

(defun terminal-here-default-terminal-command (dir)
  "Pick a good default command to use for DIR."
  (cond
   ((eq system-type 'darwin)
    (list "open" "-a" "Terminal.app" dir))

   ;; From http://stackoverflow.com/a/13509208/874671
   ((memq system-type '(windows-nt ms-dos cygwin))
    (list "cmd.exe" "/C" "start" "cmd.exe"))

   ;; Probably X11!
   (t '("x-terminal-emulator"))))


(defcustom terminal-here-terminal-command
  #'terminal-here-default-terminal-command
  "The command used to start a terminal.
Either a list of strings: (terminal-binary arg1 arg2 ...); or a
function taking a directory and returning such a list."
  :group 'terminal-here
  :type '(choice (repeat string)
                 (function)))

(defcustom terminal-here-project-root-function
  (cl-find-if 'fboundp '(projectile-project-root vc-root-dir))
  "Function called to find the current project root directory.
Good options include `projectile-project-root', which requires
you install the `projectile' package, or `vc-root-dir' which is
available in Emacs >= 25.1.
The function should return nil or signal an error if the current
buffer is not in a project."
  :group 'terminal-here
  :type 'function)



(defun terminal-here-launch-in-directory (dir)
  "Launch a terminal in directory DIR."
  (let* ((term-command (if (functionp terminal-here-terminal-command)
                           (funcall terminal-here-terminal-command dir)
                         terminal-here-terminal-command))
         (process-name (car term-command))
         (default-directory dir)
         (proc (apply #'start-process process-name nil term-command)))
    (set-process-sentinel
     proc
     (lambda (proc _)
       (when (and (eq (process-status proc) 'exit) (/= (process-exit-status proc) 0))
         (message "Error: in terminal here, command `%s` exited with error code %d"
                  (mapconcat #'identity term-command " ")
                  (process-exit-status proc)))))
    ;; Don't close when emacs closes, seems to only be necessary on Windows.
    (set-process-query-on-exit-flag proc nil)))

;;;###autoload
(defun terminal-here-launch ()
  "Launch a terminal in the current working directory.
This is the directory of the current buffer unless you have
changed it by running `cd'."
       (interactive)
       (terminal-here-launch-in-directory default-directory))

;;;###autoload
(defalias 'terminal-here 'terminal-here-launch)

;;;###autoload
(defun terminal-here-project-launch ()
  "Launch a terminal in the current project root.
If projectile is installed the projectile root will be used,
  Otherwise `vc-root-dir' will be used."
       (interactive)
       (when (not terminal-here-project-root-function)
         (signal 'user-error "No `terminal-here-project-root-function' is set."))
       (let ((root (funcall terminal-here-project-root-function)))
         (when (not root)
           (signal 'user-error "Not in any project according to `terminal-here-project-root-function'"))
         (terminal-here-launch-in-directory root)))
;; default compile command
(defun compile-command-auto ()
  (interactive)
  (if (not(and (file-exists-p "makefile")
               (file-exists-p "Makefile")))
      (let ((file-name (if buffer-file-name (file-relative-name (buffer-file-name)))))
        (cond
         ((eq major-mode 'swift-mode)
          (setq compile-command
                (concat "swift "
                        file-name
                        ;; " && ./"
                        ;; (file-name-sans-extension file-name)
                        (when (file-exists-p "tc.txt")
                          " < tc.txt"))))
         ((eq major-mode 'kotlin-mode)
          (setq compile-command
                (concat "kotlinc "
                        file-name
                        " -include-runtime -d "
                        (file-name-sans-extension file-name)
                        ".jar"
                        " && java -jar "
                        (file-name-sans-extension file-name)
                        ".jar"
                        (when (file-exists-p "tc.txt")
                          " < tc.txt"))))
         ((eq major-mode 'c++-mode)
          (setq compile-command
                (concat "g++ -std=c++11 "
                        file-name
                        " -o "
                        (file-name-sans-extension file-name)
                        " && ./"
                        (file-name-sans-extension file-name)
                        (when (file-exists-p "tc.txt")
                          " < tc.txt")))))
        )
    (setq compile-command "make"))
  (call-interactively 'compile))
;; (add-hook 'c++-mode-hook 'compile-command-auto)
(add-hook 'compilation-mode-hook (lambda()
                                   (setq compile-command (car compile-history))))

;; function for converting edge list into graph
(defun edge-uv-to-graph (&optional directed)
  (interactive "P")
  (let* ((start (region-beginning))
         (end (region-end))
         (str (buffer-substring-no-properties start end))
         (graph-type (if directed "digraph" "graph"))
         (conn-type (if directed " -> " " -- "))
         (file (read-string "Enter file name: ")))
    (message "conn-type %s" conn-type)
    (with-current-buffer (find-file-noselect file)
      (insert str)
      (beginning-of-buffer)
      (insert (concat graph-type " {"))
      (newline)
      (replace-regexp "\\([ ]\\)" conn-type)
      (end-of-buffer)
      (newline)
      (insert "}")
      (message (buffer-string))

      (write-file (concat file ".dot"))
      (graphviz-dot-preview)
      )
    ))

(defun edge-uvw-to-graph (&optional directed)
  (interactive "P")
  (let* ((start (region-beginning))
         (end (region-end))
         (str (buffer-substring-no-properties start end))
         (graph-type (if directed "digraph" "graph"))
         (conn-type (if directed " -> " " -- "))
         (file (read-string "Enter file name: ")))
    (with-current-buffer (find-file-noselect file)
      (insert str)
      (beginning-of-buffer)
      (insert (concat graph-type " {"))
      (newline)
      (beginning-of-buffer)
      (while (re-search-forward "\\([ ]+\\)\\([0-9]+\\)\\([ ]+\\)\\([0-9]+\\)" nil t)
        (replace-match (concat "\\1 "
                               conn-type
                               " \\2 [label= "
                               (match-string-no-properties 4)
                               " ,weight = "
                               (match-string-no-properties 4)
                               "]") nil nil ))
      ;; (replace-regexp "\\([ ]\\)" conn-type)
      (end-of-buffer)
      (newline)
      (insert "}")
      (write-file (concat file ".dot"))
      (graphviz-dot-preview)
      )
    ))

(add-hook 'graphviz-dot-mode-hook (lambda () (setq default-tab-width 4)))

;; enable evil-ergoemacs-mode
(defun smarter-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
       (interactive "^p")
       (setq arg (or arg 1))

       ;; Move lines first
       (when (/= arg 1)
         (let ((line-move-visual nil))
           (forward-line (1- arg))))

       (let ((orig-point (point)))
         (back-to-indentation)
         (when (= orig-point (point))
           (move-beginning-of-line 1))))

(defun my-magit-stage-all-and-commit (message)
  (interactive "sCommit Message: ")
  (magit-stage-file (buffer-file-name (current-buffer)))
  (magit-commit (list "-m" message)))

;; copy whole buffer
(defun copy-whole-buffer ()
  (interactive)
  (save-excursion
    (kill-ring-save (point-min) (point-max))))

(defun indent-next-line (ignore)
  (save-excursion
    (next-line)
    (indent-region (line-beginning-position) (line-end-position))))
(advice-add 'open-line :after 'indent-next-line)

(defun my-jump-debug-enable ()
  (interactive)
  (jump-to-string "DEBUG" t)
  (end-of-line))

(defun toggle-debug-enable ()
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (if (search-forward "DEBUG 1" nil t)
        (progn
          (backward-char)
          (delete-char 1)
          (insert "0")
          (beginning-of-buffer)
          (insert "#define NDEBUG")
          (newline)
          (message "DEBUG disabled"))
      (beginning-of-line)
      (when (search-forward "NDEBUG" nil t)
        (kill-whole-line))
      (search-forward "DEBUG 0")
      (backward-char)
      (delete-char 1)
      (insert "1")
      (message "DEBUG enabled")
      )))

(defun my-jump-var-declaration (&optional not-forward)
  (interactive)
  (jump-to-string "VARIABLE DECLARATION" not-forward))

(defun my-jump-defvars ()
  (interactive)
  (jump-to-string "defvars"))

(defun my-jump-main-vars ()
  (interactive)
  (my-jump-main)
  (beginning-of-defun)
  (search-forward "Variables")
  (beginning-of-line))

(defun my-jump-input ()
  (interactive)
  (my-jump-main)
  (search-forward "Input")
  (beginning-of-line))

(defun my-jump-output ()
  (interactive)
  (my-jump-main)
  (search-forward "Output")
  (beginning-of-line))

(defun my-jump-solve ()
  (interactive)
  (my-jump-main)
  (search-forward "Solve")
  (beginning-of-line))

(defun my-jump-main ()
  (interactive)
  (jump-to-string "main"))

(defun jump-to-string (string &optional not-forward)
  (interactive)
  (beginning-of-buffer)
  (search-forward string)
  (beginning-of-line)
  (unless not-forward
    (forward-paragraph))
  ;; (next-line line-down)
  )

(defun my-move-var-to-global-scope (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-region (line-beginning-position) (line-end-position)))
  (save-excursion
    (my-jump-var-declaration t)
    (search-forward "***********")
    (next-line)
    (beginning-of-line)
    (yank)
    (newline-and-indent)))

(defun my-move-var-to-top-local-function (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-region (line-beginning-position) (line-end-position)))
  (save-excursion
    (my-jump-main-vars)
    (next-line)
    (beginning-of-line)
    (yank)
    (newline-and-indent)))

(defun my-move-line-above-function (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-region (line-beginning-position) (line-end-position)))
  (save-excursion
    (beginning-of-defun)
    (previous-line)
    (beginning-of-line)
    (newline-and-indent)
    (yank)))

(defun my-move-var-to-higher-scope (beg end)
  (interactive "r")
  (if (region-active-p)
      (kill-region beg end)
    (kill-whole-line))
  (save-excursion
    (cedit-up-block-backward)
    (beginning-of-line)
    (yank)
    (newline-and-indent)))

;; combine both yas and yankpad expand
(defun my-snippet-expand ()
  (interactive)
  (let ((company-backends '((company-yankpad :with company-yasnippet) )))
    (unless (company-complete)
      (require 'yankpad)
      (unless (call-interactively 'yankpad-expand)
        (call-interactively 'yas-expand)))))

(defun insert-inf ()
  (interactive)
  (insert "INF"))

(defun insert-2sqr-brackets ()
  (interactive)
  (save-excursion
    (insert-square-brackets 2))
  (forward-char 1))

(defun insert-3sqr-brackets ()
  (interactive)
  (save-excursion
    (insert-square-brackets 3)
    )
  (forward-char 1))

(defun insert-4sqr-brackets ()
  (interactive)
  (save-excursion
    (insert-square-brackets 4)
    )
  (forward-char 1))

(defun insert-square-brackets (num)
  (interactive)
  (loop for i from 1 to num
        do (insert "[]"))
  (evil-insert 1)
  )

(defun insert-and-op ()
  (interactive)
  (insert "&&"))

(defun insert-or-op ()
  (interactive)
  (insert "||"))

(defun insert-cin3 ()
  "Expand the yasnippet named `foobar'."
  (interactive)
  (insert "cin3")
  (yankpad-expand)
  (evil-insert 1))

(defun insert-c-macro ()
  (interactive)
  (save-excursion
    (let ((str (concat "#define " (read-string "Macro: "))))
      (beginning-of-defun)
      (newline-and-indent)
      (previous-line)
      (beginning-of-line)
      (insert str))))

(defun insert-existency-symbols()
  (interactive)
  (insert (concat (helm-comp-read "Choice: " exist-symbols) "()"))
  (indent-region (line-beginning-position) (line-end-position))
  (backward-char)
  (evil-insert 1))

(defun insert-if ()
  "Expand the yasnippet named `foobar'."
  (interactive)
  (insert "when")
  (yankpad-expand)
  (evil-insert 1)
  )

(defun insert-while ()
  (interactive)
  (yas-expand-snippet (yas-lookup-snippet "while"))
  (evil-insert 1)
  )

(defun insert-repc ()
  (interactive)
  (insert "repc")
  (yankpad-expand)
  (evil-insert 1)
  )

(defun insert-repi ()
  (interactive)
  (insert "repi")
  (yankpad-expand)
  (evil-insert 1)
  )

(defun insert-repj ()
  (interactive)
  (insert "repj")
  (yankpad-expand)
  (evil-insert 1)
  )

(defun insert-repk ()
  (interactive)
  (insert "repk")
  (yankpad-expand)
  (evil-insert 1)
  )

(defun zz-scroll-half-page (direction)
  "Scrolls half page up if `direction' is non-nil, otherwise will scroll half page down."
  (let ((opos (cdr (nth 6 (posn-at-point)))))
    ;; opos = original position line relative to window
    (move-to-window-line nil)  ;; Move cursor to middle line
    (if direction
        (recenter-top-bottom -1)  ;; Current line becomes last
      (recenter-top-bottom 0))  ;; Current line becomes first
    (move-to-window-line opos)))  ;; Restore cursor/point position

(defun zz-scroll-half-page-down ()
  "Scrolls exactly half page down keeping cursor/point position."
  (interactive)
  (zz-scroll-half-page nil))

(defun zz-scroll-half-page-up ()
  "Scrolls exactly half page up keeping cursor/point position."
  (interactive)
  (zz-scroll-half-page t))

(defun my-recompile()
  (interactive)
  (save-buffer)
  (call-interactively 'recompile))

(defun my-fancy-newline ()
  "Add two newlines and put the cursor at the right indentation
between them if a newline is attempted when the cursor is between
two curly braces, otherwise do a regular newline and indent"
       (interactive)
       (if (and (equal (char-before) 123) ; {
                (equal (char-after) 125)) ; }
           (progn (newline-and-indent)
                  (split-line)
                  (indent-for-tab-command))
         (newline-and-indent)))

;; fuzzy matching with flx for hippie
(defun try-expand-flx (old)
  "Try to complete word using flx matching."
  (if (not old)
      (progn
        (he-init-string (he-lisp-symbol-beg) (point))
        (if (not (he-string-member he-search-string he-tried-table))
            (setq he-tried-table (cons he-search-string he-tried-table)))
        (setq he-expand-list
              (and (not (equal he-search-string ""))
                   (try-expand-flx-collect he-search-string)))))
  (while (and he-expand-list
              (he-string-member (car he-expand-list) he-tried-table))
    (setq he-expand-list (cdr he-expand-list)))
  (if (null he-expand-list)
      (progn
        (if old (he-reset-string)) ())
    (progn
      (he-substitute-string (car he-expand-list))
      (setq he-expand-list (cdr he-expand-list))
      t)))

(defun try-expand-flx-collect (str)
  "Find and collect all words that flex-match str, and sort by flx score"
  (let ((coll '())
        (regexp (try-expand-flx-regexp str)))
    (save-excursion
      (goto-char (point-min))
      (while (search-forward-regexp regexp nil t)
        (setq coll (cons (thing-at-point 'symbol) coll))))
    (setq coll (sort coll #'(lambda (a b)
                              (> (car (flx-score a str))
                                 (car (flx-score b str))))))
    coll))

(defun try-expand-flx-regexp (str)
  "Generate regexp for flexible matching of str."
  (concat "\\b" (mapconcat (lambda (x) (concat "\\w*-*" (list x))) str "")
          "\\w*-*" "\\b"))

(add-to-list 'hippie-expand-try-functions-list 'try-expand-flx)
