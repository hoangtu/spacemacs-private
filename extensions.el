;;; extensions.el --- tuhdo Layer extensions File for Spacemacs
;;
;; Copyright (c) 2012-2014 Sylvain Benner
;; Copyright (c) 2014-2015 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

(setq tuhdo-pre-extensions
      '(
        ;; pre extension tuhdos go here
        ))

(setq tuhdo-post-extensions
      '(
        ;; post extension tuhdos go here
        jdee
        verilog-mode
        org-wiki
        ))

;; For each extension, define a function tuhdo/init-<extension-tuhdo>
;;
;; (defun tuhdo/init-my-extension ()
;;   "Initialize my extension"
;;   )
;;
;; Often the body of an initialize function uses `use-package'
;; For more info on `use-package', see readme:
;; https://github.com/jwiegley/use-package
(add-to-list 'load-path (concat user-emacs-directory "private/tuhdo/extensions/"))
(defun tuhdo/init-jdee ()
  (use-package java-mode
    :defer t
    :init
    (add-to-list 'load-path "/usr/local/jdee/lisp/")
    (setq bsh-jar "/usr/local/jdee/java/lib/bsh.jar")
    (setq jde-complete-function 'jde-complete-minibuf)
    (with-eval-after-load 'jde
      (define-key jde-mode-map (kbd "TAB") 'jde-complete))
    (add-hook 'java-mode-hook (lambda ()
                                (unless (featurep 'jde)
                                  (condition-case nil
                                      (progn
                                        (require 'jde)
                                        (jde-mode))
                                    (error nil)))))))

(defun tuhdo/init-verilog-mode ()
  (use-package verilog-mode
    :defer t
    :init
    (progn
      (with-eval-after-load 'verilog-mode
        (with-eval-after-load 'company-keywords
          (setq verilog-auto-newline nil)
          (add-to-list 'company-keywords-alist (cons 'verilog-mode verilog-keywords))))

      (defun verilog-compile ()
        (interactive)
        (setq verilog-compiler (concat "iverilog -o " (file-name-base (buffer-file-name)) " "))
        (setq verilog-tool 'verilog-compiler)
        (verilog-set-compile-command)
        (verilog-auto-save-compile))

      (defun verilog-simulator ()
        (interactive)
        (setq verilog-simulator (concat "vvp ./" (file-name-base (buffer-file-name)) " "))
        (async-shell-command verilog-simulator))

      (defun verilog-gtkwave ()
        (interactive)
        (async-shell-command (concat "gtkwave ./" (file-name-base (buffer-file-name)) ".vcd")))

      (evil-leader/set-key-for-mode 'verilog-mode
        "mc" 'verilog-compile
        "ms" 'verilog-simulator
        "mv" 'verilog-gtkwave)

      (push 'company-capf company-backends-verilog-mode)
      (add-hook 'verilog-mode-hook 'flycheck-mode)
      (spacemacs|add-company-hook verilog-mode))))

(defun tuhdo/init-org-wiki ()
  (use-package org-wiki
    :init
    (setq org-wiki-location "~/Documents/wiki")))

(load (concat user-emacs-directory "private/tuhdo/extensions/semantic-indexing.el"))

