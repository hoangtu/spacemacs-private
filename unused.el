;; config.el

(defun codeforce-get-problem (url)
  (interactive "sURL: ")
  (require 'shr)
  (let* ((number-regexp (caar (highlight-numbers--get-regexp-for-mode 'c-mode)))

         (var-list '("an" "am" "ai" "aj" "i" "j" "n" "m" "k" "x" "y" ))
         (case-fold-search nil)
         (title nil))
    (eww url)
    (sleep-for 2)
    (read-only-mode -1)
    (beginning-of-buffer)
    (search-forward "Custom test")
    (delete-region (point-min) (match-end 0))
    (insert (concat "Link: " url))
    (dolist (v '(1 2 3 4 5))
      (delete-blank-lines))
    (next-line)
    (setq title (buffer-substring-no-properties (line-beginning-position) (line-end-position)))
    (save-excursion
      (delete-region (line-beginning-position) (line-end-position))
      (insert (propertize title 'face 'org-level-1))
      (search-and-format-requirement "time limit per test")
      (search-and-format-requirement "memory limit per test")
      (search-and-format-requirement "input")
      (search-and-format-requirement "output")
      (search-and-propertize "Examples" nil 'face 'bold)
      (beginning-of-buffer)
      (while (search-and-propertize "Input" nil 'face 'bold))
      (beginning-of-buffer)
      (while (search-and-propertize "Output" nil 'face 'bold))
      (beginning-of-buffer)
      (while (search-and-propertize "≤" nil 'face 'font-lock-doc-face))
      (beginning-of-buffer)
      (while (search-and-propertize "<" nil 'face 'font-lock-doc-face))
      (beginning-of-buffer)
      (while (search-and-propertize "≥" nil 'face 'font-lock-doc-face))
      (beginning-of-buffer)
      (while (search-and-propertize ">" nil 'face 'font-lock-doc-face))
      (beginning-of-buffer)
      (while (search-and-propertize number-regexp t 'face 'highlight-numbers-number))
      (dolist (v var-list)
        (beginning-of-buffer)
        (while (search-and-propertize (concat "\\_<" v "\\_>") t 'face 'font-lock-keyword-face)))
      (search-forward "Copyright")
      (delete-region (line-beginning-position) (point-max))
      (wordsmith-mode)
      (wordsmith-highlight-attribute "adjectives" (current-buffer)))
    (read-only-mode)))


(defun spoj-get-problem (url)
  (interactive "sURL: ")
  (require 'shr)
  (let* ((number-regexp (caar (highlight-numbers--get-regexp-for-mode 'c-mode)))
         (var-list '("an" "am" "ai" "aj" "i" "j" "n" "m" "k" "x" "y" ))
         (case-fold-search nil))
    (eww url)
    (sleep-for 2)
    (read-only-mode -1)
    (beginning-of-buffer)
    (save-excursion
      (beginning-of-buffer)
      (dolist (v '(1 2 3))
        (delete-region (line-beginning-position) (line-end-position)))
      (search-forward "Submit solution")
      (delete-region (match-beginning 0) (point-max))
      (beginning-of-buffer)
      (search-forward "Ranking")
      (delete-region (point-min) (match-end 0))
      (insert "Link: " url)
      (beginning-of-buffer)
      (search-and-propertize "Example" nil 'face 'bold)
      (beginning-of-buffer)
      (search-and-propertize "Input" nil 'face 'bold)
      (beginning-of-buffer)
      (search-and-propertize "Output" nil 'face 'bold)
      (beginning-of-buffer)
      (search-and-propertize "Information" nil 'face 'bold)
      ;; (beginning-of-buffer)
      ;; (while (search-and-propertize "≤" nil 'face 'font-lock-doc-face))
      ;; (beginning-of-buffer)
      ;; (while (search-and-propertize "<" nil 'face 'font-lock-doc-face))
      ;; (beginning-of-buffer)
      ;; (while (search-and-propertize "≥" nil 'face 'font-lock-doc-face))
      ;; (beginning-of-buffer)
      ;; (while (search-and-propertize ">" nil 'face 'font-lock-doc-face))
      (beginning-of-buffer)
      (while (search-and-propertize number-regexp t 'face 'highlight-numbers-number))
      (dolist (v var-list)
        (beginning-of-buffer)
        (while (search-and-propertize (concat "\\_<" v "\\_>") t 'face 'font-lock-keyword-face)))
      (wordsmith-mode)
      (wordsmith-highlight-attribute "adjectives" (current-buffer)))
    (read-only-mode)
    (goto-address-mode)))
;; (defalias 'qrr 'quick-run-multiples-redirect)
;; (defalias 'edf 'eshell-directory-files)
;; (with-eval-after-load 'eshell
;;   (add-hook 'eshell-mode-hook (lambda () (company-mode -1))))

;; (defun quickrun-all-test-cases ()
;;   (interactive)
;;   (qrr (buffer-name (current-buffer)) (edf ".*.txt")))
(defun search-and-format-requirement (string)
  (save-excursion
    (search-forward string)
    (delete-region (line-beginning-position) (line-end-position))
    (save-excursion
      (insert (propertize string 'face 'term-underline)))
    (end-of-line)
    (kill-line)
    (delete-horizontal-space)
    (insert ": ")))

(defun search-and-propertize (string regexp &rest args)
  (let ((found))
    (condition-case nil
        (setq found (if regexp
                        (re-search-forward string)
                      (search-forward string)))
      (error nil))
    (if regexp
        (progn
          (setq string (match-string 0))
          (replace-match (apply #'propertize string args)))
      (replace-match (apply #'propertize string args) t))
    found))

(defun codeforce-get-color-without-hash (color)
  (message "color is %s" color)
  (substring color 1 (length color)))
;; fuzzy for flx
(defun try-expand-flx (old)
  "Try to complete word using flx matching."
  (if (not old)
      (progn
        (he-init-string (he-lisp-symbol-beg) (point))
        (if (not (he-string-member he-search-string he-tried-table))
            (setq he-tried-table (cons he-search-string he-tried-table)))
        (setq he-expand-list
              (and (not (equal he-search-string ""))
                   (try-expand-flx-collect he-search-string)))))
  (while (and he-expand-list
              (he-string-member (car he-expand-list) he-tried-table))
    (setq he-expand-list (cdr he-expand-list)))
  (if (null he-expand-list)
      (progn
        (if old (he-reset-string)) ())
    (progn
      (he-substitute-string (car he-expand-list))
      (setq he-expand-list (cdr he-expand-list))
      t)))

(defun try-expand-flx-collect (str)
  "Find and collect all words that flex-match str, and sort by flx score"
  (let ((coll '())
        (regexp (try-expand-flx-regexp str)))
    (save-excursion
      (goto-char (point-min))
      (while (search-forward-regexp regexp nil t)
        (setq coll (cons (thing-at-point 'symbol) coll))))
    (setq coll (sort coll #'(lambda (a b)
                              (> (car (flx-score a str))
                                 (car (flx-score b str))))))
    coll))

(defun try-expand-flx-regexp (str)
  "Generate regexp for flexible matching of str."
  (concat "\\b" (mapconcat (lambda (x) (concat "\\w*-*" (list x))) str "")
          "\\w*-*" "\\b"))

(defun try-expand-flexible-abbrev (old)
  "Try to complete word using flexible matching.

Flexible matching works by taking the search string and then
interspersing it with a regexp for any character. So, if you try
to do a flexible match for `foo' it will match the word
`findOtherOtter' but also `fixTheBoringOrange' and
`ifthisisboringstopreadingnow'.

The argument OLD has to be nil the first call of this function, and t
for subsequent calls (for further possible completions of the same
string).  It returns t if a new completion is found, nil otherwise."
       (if (not old)
           (progn
             (he-init-string (he-lisp-symbol-beg) (point))
             (if (not (he-string-member he-search-string he-tried-table))
                 (setq he-tried-table (cons he-search-string he-tried-table)))
             (setq he-expand-list
                   (and (not (equal he-search-string ""))
                        (he-flexible-abbrev-collect he-search-string)))))
       (while (and he-expand-list
                   (he-string-member (car he-expand-list) he-tried-table))
         (setq he-expand-list (cdr he-expand-list)))
       (if (null he-expand-list)
           (progn
             (if old (he-reset-string))
             ())
         (progn
           (he-substitute-string (car he-expand-list))
           (setq he-expand-list (cdr he-expand-list))
           t)))

(defun he-flexible-abbrev-collect (str)
  "Find and collect all words that flex-matches STR.
See docstring for `try-expand-flexible-abbrev' for information
about what flexible matching means in this context."
       (let ((collection nil)
             (regexp (he-flexible-abbrev-create-regexp str)))
         (save-excursion
           (goto-char (point-min))
           (while (search-forward-regexp regexp nil t)
             ;; Is there a better or quicker way than using
             ;; `thing-at-point' here?
             (setq collection (cons (thing-at-point 'word) collection))))
         collection))

(defun he-flexible-abbrev-create-regexp (str)
  "Generate regexp for flexible matching of STR.
See docstring for `try-expand-flexible-abbrev' for information
about what flexible matching means in this context."
       (concat "\\b" (mapconcat (lambda (x) (concat "\\w*" (list x))) str "")
               "\\w*" "\\b"))

;; Color identifer
(defun tuhdo/conditional-color-identifier-mode ()
  (when (< (buffer-size) 1000000)
    (color-identifiers-mode)))

;; (add-hook 'prog-mode-hook 'tuhdo/conditional-color-identifier-mode)

(setq dcsh-command-list '("CommonInc" "TmplWhile" "" "TmplFor"
                          "DGet2To00" "DGetTC" "DGetN2" "SGetN1"
                          "Get1LineN" "TmplDFS" "TmplBFS" "dfsFunc" "bfsFunc"
                          "dijkFunc" "TmplGraph3" "graphin2" "Loop2Ptrs" "LoopIJ"))

(defun he-dcsh-command-beg ()
  (let ((p))
    (save-excursion
      (backward-word 1)
      (setq p (point)))
    p))

(defun try-expand-dcsh-command (old)
  (unless old
    (he-init-string (he-dcsh-command-beg) (point))
    (setq he-expand-list (sort
                          (all-completions he-search-string (mapcar 'list dcsh-command-list))
                          'string-lessp)))
  (while (and he-expand-list
              (he-string-member (car he-expand-list) he-tried-table))
    (setq he-expand-list (cdr he-expand-list)))
  (if (null he-expand-list)
      (progn
        (when old (he-reset-string))
        ())
    (he-substitute-string (car he-expand-list))
    (setq he-tried-table (cons (car he-expand-list) (cdr he-tried-table)))
    (setq he-expand-list (cdr he-expand-list))
    t))
;; expand-region
;; (defun er/add-cc-mode-expansions ()
;;   ;; (make-variable-buffer-local 'er/try-expand-list)
;;   (setq-local er/try-expand-list '(er/c-mark-fully-qualified-name
;;                                    er/mark-inside-quotes
;;                                    er/mark-outside-quotes
;;                                    er/mark-inside-pairs
;;                                    er/mark-outside-pairs
;;                                    er/mark-comment
;;                                    er/mark-method-call
;;                                    er/c-mark-statement
;;                                    er/mark-defun
;;                                    ;; er/try-expand-list
;;                                    )))


;; register marker faces
;; (make-variable-buffer-local 'register-alist) ; it's global by default
;; (defface register-marker-face '((t
;;                                  (:underline
;;                                   (:color "dark blue" :style line)
;;                                   :foreground "red")))
;;   "Used to mark register positions in a buffer."
;;   :group 'faces)
;; (defun set-register (register value)
;;   "Set Emacs register named REGISTER to VALUE.  Returns VALUE.
;;     See the documentation of `register-alist' for possible VALUE."
;;   (let ((aelt (assq register register-alist))
;;         (sovl (intern (concat "point-register-overlay-"
;;                               (single-key-description register))))
;;         )
;;     (when (not (boundp sovl))
;;       (set sovl (make-overlay (point)(point)))
;;       (overlay-put (symbol-value sovl) 'face 'register-marker-face)
;;       (overlay-put (symbol-value sovl) 'help-echo
;;                    (concat "Register: `"
;;                            (single-key-description register) "'")))
;;     (delete-overlay (symbol-value sovl))
;;     (if (markerp value)
;;         ;; I'm trying to avoid putting overlay on newline char
;;         (if (and (looking-at "$")(not (looking-back "^")))
;;             (move-overlay (symbol-value sovl) (1- value) value)
;;           (move-overlay (symbol-value sovl) value (1+ value)))) ;
;;     (if aelt
;;         (setcdr aelt value)
;;       (push (cons register value) register-alist))
;;     value))

(defconst
  prettify-greek-lower
  '(("alpha" . ?α)
    ("beta" . ?β)
    ("gamma" . ?γ)
    ("delta" . ?δ)
    ("epsilon" . ?ε)
    ("zeta" . ?ζ)
    ("eta" . ?η)
    ("theta" . ?θ)
    ;; ("iota" . ?ι)
    ("kappa" . ?κ)
    ("lambda" . ?λ)
    ;; ("mu" . ?μ)
    ;; ("nu" . ?ν)
    ;; ("xi" . ?ξ)
    ;; ("omicron" . ?ο)
    ;; ("pi" . ?π)
    ;; ("rho" . ?ρ)
    ;; ("sigma" . ?σ)
    ;; ("tau" . ?τ)
    ;; ("upsilon" . ?υ)
    ;; ("phi" . ?φ)
    ;; ("chi" . ?χ)
    ;; ("psi" . ?ψ)
    ("omega" . ?ω))
  "Prettify rules for lower case greek letters.")

(defconst
  prettify-greek-upper
  '(("Alpha" . ?𝚨)
    ("Beta" . ?𝚩)
    ("Gamma" . ?Γ)
    ("Delta" . ?Δ)
    ("Epsilon" . ?Ε)
    ("Zeta" . ?Ζ)
    ("Eta" . ?𝚮)
    ("Theta" . ?Θ)
    ;; ("Iota" . ?Ι)
    ;; ("Kappa" . ?Κ)
    ("Lambda" . ?Λ)
    ;; ("Mu" . ?𝚳)
    ;; ("Nu" . ?𝚴)
    ;; ("Xi" . ?Ξ)
    ;; ("Omicron" . ?Ο)
    ;; ("Pi" . ?Π)
    ;; ("Rho" . ?Ρ)
    ;; ("Sigma" . ?Σ)
    ;; ("Tau" . ?Τ)
    ;; ("Upsilon" . ?𝚼)
    ;; ("Phi" . ?Φ)
    ;; ("Chi" . ?𝚾)
    ;; ("Psi" . ?Ψ)
    ("Omega" . ?Ω))
  "Prettify rules for upper case greek letters.")
(defvar exist-symbols '("exist_map"
                        "exist_col"
                        "exist_set"
                        "exist_str"))
(defun cc-pretify ()
  ;; (push '("][" . ?,) prettify-symbols-alist)
  (push '("INF" . ?∞) prettify-symbols-alist)
  (push '("sqrt" . ?√) prettify-symbols-alist)
  (push '("NULL" . ?∅) prettify-symbols-alist)
  (push '("true" . ?⊨) prettify-symbols-alist)
  (push '("false" . ?⊭) prettify-symbols-alist)
  ;; (push '("unless" . ?⁈) prettify-symbols-alist)
  ;; (push '("if" . ?︖) prettify-symbols-alist)
  ;; (push '("while" . ?○) prettify-symbols-alist)
  ;; (push '("until" . ?●) prettify-symbols-alist)
  ;; (push '("loop" . ?ƪ) prettify-symbols-alist)
  (push '("->" . ?➛) prettify-symbols-alist)
  ;; (push '("=" . ?←) prettify-symbols-alist)
  (push '("!=" . ?≠) prettify-symbols-alist)
  (push '("&&" . ?⋏) prettify-symbols-alist)
  (push '("||" . ?⋎) prettify-symbols-alist)
  ;; (push '("repc" . ?∀) prettify-symbols-alist)
  ;; (push '("rep" . ?∀) prettify-symbols-alist)
  ;; (push '("for" . ?∀) prettify-symbols-alist)
  ;; (dolist (v exist-symbols)
  ;;   (push (cons v ?∃) prettify-symbols-alist))
  (dolist (v prettify-greek-lower)
    (push v prettify-symbols-alist))
  (dolist (v prettify-greek-upper)
    (push v prettify-symbols-alist))
  (push '("!" . ?¬) prettify-symbols-alist)
  (push '("!=" . ?≠) prettify-symbols-alist)
  ;; (push '("=" . ?←) prettify-symbols-alist)
  (push '(">=" . ?≥) prettify-symbols-alist)
  (push '("<=" . ?≤) prettify-symbols-alist)
  ;; (push '("==" . ?=) prettify-symbols-alist)
  (push '(">=" . ?≥) prettify-symbols-alist)
  (push '(">=" . ?≥) prettify-symbols-alist)
  )

(defun prettify-cc-modes ()
  (cc-pretify)
  (prettify-symbols-mode))

;; package.el

;; (defun tuhdo/init-all-the-icons-dired ()
;;   (use-package all-the-icons-dired
;;     :init
;;     (add-hook 'dired-mode-hook 'all-the-icons-dired-mode)))

;; (defun tuhdo/post-init-avy ()
;;   (require 'avy)
;;   (setq avy-background nil)
;;   (define-key isearch-mode-map (kbd "C-'") 'avy-isearch)

;;   ;; (global-unset-key (kbd "M-b"))
;;   (global-set-key (kbd "M-n M-l") 'avy-goto-line)
;;   (global-set-key (kbd "M-n M-c") 'avy-copy-line)
;;   (global-set-key (kbd "M-n M-r") 'avy-copy-region)
;;   (global-set-key (kbd "M-n M-n") 'avy-goto-word-1)
;;   (global-set-key (kbd "M-n M-d") 'avy-kill-region)
;;   (global-set-key (kbd "M-n M-m") 'avy-goto-char-in-line)
;;   (define-key evil-normal-state-map "gc" 'avy-goto-char-in-line)

;;   (defun avy-goto-conditional ()
;;     (interactive)
;;     (avy--generic-jump "\\_<\\(if\\|case\\|when\\|cond\\|unless\\)\\_>" nil 'pre))
;;   (defun avy-goto-loop ()
;;     (interactive)
;;     (avy--generic-jump "\\_<\\(loop\\|for\\|while\\|rep\\|repc\\|dwn\\|fori\\)\\_>" nil 'pre))
;;   (defun avy-goto-def ()
;;     (interactive)
;;     (avy--generic-jump "\\_<\\(def\\|struct\\)\\_>" nil 'pre))
;;   (defun avy-goto-set ()
;;     (interactive)
;;     (avy--generic-jump "\\_<\\(set\\)\\_>" nil 'pre))

;;   (global-set-key (kbd "M-n c") 'avy-goto-conditional)
;;   (global-set-key (kbd "M-n l") 'avy-goto-loop)
;;   (global-set-key (kbd "M-n d") 'avy-goto-def)
;;   (global-set-key (kbd "M-n s") 'avy-goto-set)

;;   (evil-leader/set-key
;;     "SPC" 'avy-goto-word-or-subword-1))

;; (defun tuhdo/init-company-flx ()
;;   (use-package company-flx
;;     :init
;;     (with-eval-after-load 'company
;;       (add-hook 'company-mode-hook (lambda ()
;;                                      (add-to-list 'company-backends 'company-capf)))
;;       (company-flx-mode +1))))

;; (defun tuhdo/post-init-auctex ()
;;   "docstring"
;;   (setq TeX-command-extra-options "--synctex=1"
;;         TeX-source-correlate-start-server nil)
;;   (setq TeX-view-program-selection '(((output-dvi has-no-display-manager)
;;                                       "dvi2tty")
;;                                      ((output-dvi style-pstricks)
;;                                       "dvips and gv")
;;                                      (output-dvi "xdvi")
;;                                      (output-pdf "PDF Tools")
;;                                      (output-html "xdg-open")))
;;   (add-hook 'LaTeX-mode-hook 'TeX-source-correlate-mode)
;;   (add-hook 'TeX-after-compilation-finished-functions #'TeX-revert-document-buffer))

;; (defun tuhdo/init-lispy ()
;;   (use-package lispy
;;     :init
;;     ;; (add-hook 'emacs-lisp-mode-hook #'lispy-mode)
;;     ;; (add-hook 'lisp-mode-hook #'lispy-mode)
;;     ;; (add-hook 'scheme-mode-hook #'lispy-mode)
;;     (setq lispy-completion-method 'helm)
;;     (with-eval-after-load 'lispy
;;       ;; (define-key lispy-mode-map-lispy (kbd "C-a") 'spacemacs/smart-move-beginning-of-line)
;;       (define-key lispy-mode-map-lispy (kbd "C-c [") 'lispy-backward)
;;       (define-key lispy-mode-map-lispy (kbd "C-c ]") 'lispy-forward)
;;       (define-key lispy-mode-map-lispy (kbd "M-<left>") 'ahs-backward)
;;       (define-key lispy-mode-map-lispy (kbd "M-<right>") 'ahs-forward)
;;       ;; (define-key lispy-mode-map-lispy (kbd "M-i") 'lispy-iedit)
;;       ;; (define-key lispy-mode-map-lispy (kbd "M-o") 'describe-number-at-point)
;;       (define-key lispy-mode-map-lispy (kbd "[") 'self-insert-command)
;;       (define-key lispy-mode-map-lispy (kbd "]") 'self-insert-command)
;;       (define-key lispy-mode-map-c-digits (kbd "C-9") 'corral-parentheses-backward)
;;       (lispy-define-key lispy-mode-map-special (kbd "j") 'lispy-backward)
;;       (lispy-define-key lispy-mode-map-special (kbd "l") 'lispy-forward)
;;       (lispy-define-key lispy-mode-map (kbd "I") 'lispy-knight-up)
;;       (lispy-define-key lispy-mode-map (kbd "K") 'lispy-knight-down)
;;       (lispy-define-key lispy-mode-map-special (kbd "k") 'lispy-down)
;;       (lispy-define-key lispy-mode-map-special (kbd "i") 'lispy-up)
;;       (lispy-define-key lispy-mode-map-special (kbd "j") 'lispy-ace-symbol)
;;       (lispy-define-key lispy-mode-map-special (kbd "h") 'lispy-tab))))

;; (defun tuhdo/init-puppet-mode ()
;;   (use-package puppet-mode
;;     :defer t))

;; (defun tuhdo/init-skeletor ()
;;   (use-package skeletor
;;     :defer t))

;; (defun tuhdo/init-vhdl-mode ()
;;   (use-package vhdl-mode
;;     :defer t
;;     :init
;;     (progn
;;       (add-to-list 'auto-mode-alist '("\\.hdl\\'" . vhdl-mode))
;;       (defun tuhdo/hdl-comment-syntax ()
;;         (when (equal (file-name-extension (buffer-file-name (current-buffer))) "hdl")
;;           ;; define comment for this style: “/* … */” and “//....”
;;           (setq comment-start "//")
;;           (setq comment-end "")))
;;       (add-hook 'vhdl-mode-hook #'tuhdo/hdl-comment-syntax))
;;     :config
;;     (progn
;;       (modify-syntax-entry ?\/ ". 124" vhdl-mode-syntax-table)
;;       (modify-syntax-entry ?* ". 23b" vhdl-mode-syntax-table)
;;       (modify-syntax-entry ?\n ">" vhdl-mode-syntax-table))))

;; (defun tuhdo/init-habitica ()
;;   (use-package habitica
;;     :defer t
;;     :init
;;     (setq habitica-uid "c04074ad-8a60-49ac-9c27-88c60c62f7ca")
;;     (setq habitica-token "5dcd042d-8102-43ed-a4b2-c3971fcd89ad")))

;; (defun tuhdo/post-init-helm-gtags ()
;;   (setenv "GTAGSLIBPATH" (concat (getenv "HOME") "/.gtags.d/")))

;; (defun tuhdo/post-init-ggtags ()
;;   (with-eval-after-load 'ggtags
;;     (define-key ggtags-mode-map (kbd "M-.") 'helm-gtags-dwim))
;;   (add-hook 'c-mode-hook (lambda ()
;;                            (helm-gtags-mode 1)))
;;   (add-hook 'c++-mode-hook (lambda ()
;;                              (helm-gtags-mode 1)))
;;   (add-hook 'd-mode-hook (lambda ()
;;                            (helm-gtags-mode 1)
;;                            (ggtags-mode 1)
;;                            (eldoc-mode 1))))


(defun tuhdo/init-iasm-mode ()
  (use-package iasm
    :defer t
    :init
    ;; (spacemacs/set-leader-keys-for-major-mode 'c-mode
    ;;   "id" 'iasm-disasm
    ;;   "il" 'iasm-ldd)
    ;; (spacemacs/set-leader-keys-for-major-mode 'c++-mode
    ;;   "id" 'iasm-disasm
    ;;   "il" 'iasm-ldd)
    ))

(defun tuhdo/init-srecode ()
  (use-package srecode
    :init
    (progn
      (setq srecode-prefix-key (kbd "M-m /"))
      (add-hook 'prog-mode-hook (lambda ()
                                  (require 'srecode)
                                  (srecode-minor-mode 1))))
    :config
    (progn
      (require 'compile)
      (require 'srecode/map)
      (require 'srecode/fields)
      (setq srecode-insert-ask-variable-method 'field)
      (defun tuhdo/srecode-field-exit-ask ()
        (interactive)
        (call-interactively 'srecode-field-exit-ask)
        (keyboard-quit))
      (define-key srecode-field-keymap (kbd "C-g") 'tuhdo/srecode-field-exit-ask)
      (add-to-list 'srecode-map-load-path (concat user-emacs-directory "private/tuhdo/srt/"))
      (add-to-list 'srecode-map-save-file (concat user-emacs-directory "private/tuhdo/srt/")))))


(defun tuhdo/post-init-nasm-mode ()
  (use-package nasm-mode
    :init
    (progn
      (add-hook 'nasm-mode-hook (lambda ()
                                  (setq indent-tabs-mode nil) ; use spaces to indent
                                  (setq tab-stop-list (number-sequence 2 60 2)) ; 2 spaces per tab
                                  (electric-indent-mode -1)
                                  (company-mode 1)))
      ;; (add-to-list 'auto-mode-alist '("\\.[n]*\\(asm\\)$" . nasm-mode))
      ;; (add-to-list 'auto-mode-alist '("\\.inc" . nasm-mode))
      )
    :config
    (progn
      (define-key nasm-mode-map (kbd "M-.") 'helm-etags-select)
      (define-key nasm-mode-map (kbd "C-j") 'newline)
      (define-key nasm-mode-map (kbd ";") 'self-insert-command)
      (define-key nasm-mode-map (kbd ":") 'self-insert-command)
      (define-key nasm-mode-map (kbd "<backtab>") 'company-complete))))

(defun tuhdo/init-register-channel ()
  (use-package register-channel
    :init
    (spacemacs/set-leader-keys
      "1" 'register-channel-save-point
      "2" 'register-channel-save-point
      "3" 'register-channel-save-point
      "4" 'register-channel-save-point
      "5" 'register-channel-save-point
      "6" 'register-channel-save-point
      "7" 'register-channel-save-point
      "8" 'register-channel-save-point
      "9" 'register-channel-save-point)
    (global-set-key (kbd "M-n 1") 'register-channel-dwim)
    (global-set-key (kbd "M-n 2") 'register-channel-dwim)
    (global-set-key (kbd "M-n 3") 'register-channel-dwim)
    (global-set-key (kbd "M-n 4") 'register-channel-dwim)
    (global-set-key (kbd "M-n 5") 'register-channel-dwim)
    (global-set-key (kbd "M-n 6") 'register-channel-dwim)
    (global-set-key (kbd "M-n 7") 'register-channel-dwim)
    (global-set-key (kbd "M-n 8") 'register-channel-dwim)
    (global-set-key (kbd "M-n 9") 'register-channel-dwim)
    (global-set-key (kbd "M-n 0") 'register-channel-dwim)
    )
  )

(defun tuhdo/init-evil-snipe ()
  (use-package evil-snipe
    :init
    (setq evil-snipe-override-evil-repeat-keys nil)
    (evil-define-key 'motion map "." 'evil-snipe-repeat)
    (evil-define-key 'motion map "," 'evil-snipe-repeat-reverse)
    (evil-snipe-override-mode)
    (push '(?\[ "[[{(]") evil-snipe-aliases)
    ))

(defun tuhdo/init-elscreen ()
  (use-package elscreen
    :bind (("M-1" . elscreen-goto-command-1)
           ("M-2" . elscreen-goto-command-2)
           ("M-3" . elscreen-goto-command-3)
           ("M-4" . elscreen-goto-command-4)
           ("M-5" . elscreen-goto-command-5)
           ("M-6" . elscreen-goto-command-6)
           ("M-7" . elscreen-goto-command-7)
           ("M-8" . elscreen-goto-command-8)
           ("M-9" . elscreen-goto-command-9)
           ("M-0" . elscreen-goto-command-0))
    :init
    (defmacro create-elscreen-goto-command (num)
      `(defun ,(intern (concat "elscreen-goto-command-" (number-to-string num))) ()
         (interactive)
         (unless (or (elscreen-screen-live-p ,num)
                     (> ,num (elscreen-get-number-of-screens)))
           (elscreen-clone  (elscreen-get-current-screen)))
         (elscreen-goto ,num)))

    (create-elscreen-goto-command 1)
    (create-elscreen-goto-command 2)
    (create-elscreen-goto-command 3)
    (create-elscreen-goto-command 4)
    (create-elscreen-goto-command 5)
    (create-elscreen-goto-command 6)
    (create-elscreen-goto-command 7)
    (create-elscreen-goto-command 8)
    (create-elscreen-goto-command 9)
    (create-elscreen-goto-command 0)
    ;; (add-hook after-init-hook (lambda () (call-interactively #'elscreen-start)))
    (global-set-key (kbd "M-g k") 'elscreen-kill)
    (setq elscreen-tab-display-kill-screen nil)))
;; (defun tuhdo/post-init-disaster ()
;;   (setq disaster-objdump "objdump -D -M intel -Sl")
;;   (defun disaster-extra ()
;;     (with-current-buffer disaster-buffer-assembly
;;       (setq show-trailing-whitespace nil)
;;       (iasm-mode)
;;       ;; (select-window (get-buffer-window disaster-buffer-assembly))
;;       ))
;;   (defun disaster-raw-dump (arg)
;;     (interactive "P")
;;     (require 'iasm-mode)
;;     (require 'disaster)
;;     (let* ((file (if arg
;;                      (buffer-file-name)
;;                    (read-file-name "File: ")))
;;            (disaster-objdump (concat "objdump -D intel " file)))

;;       (shell-command disaster-objdump disaster-buffer-assembly)
;;       (with-current-buffer disaster-buffer-assembly
;;         (setq buffer-file-name file))
;;       (disaster-extra)))

;;   (defun disaster-raw-redump ()
;;     (interactive)
;;     (disaster-raw-dump t))

;;   (spacemacs/set-leader-keys-for-major-mode 'c-mode
;;     "d" 'disaster-raw-dump)
;;   (spacemacs/set-leader-keys-for-major-mode 'dired-mode
;;     "d" 'disaster-raw-dump)
;;   (spacemacs/set-leader-keys-for-major-mode 'c++-mode
;;     "d" 'disaster-raw-dump)
;;   (with-eval-after-load 'iasm-mode
;;     (spacemacs/set-leader-keys-for-major-mode 'iasm-mode
;;       "d" 'disaster-raw-dump)
;;     (spacemacs/set-leader-keys-for-major-mode 'iasm-mode
;;       "r" 'disaster-raw-redump)
;;     (define-key iasm-mode-map (kbd "r") 'disaster-raw-redump))

;;   (advice-add 'disaster :after #'disaster-extra))

(defun tuhdo/init-asm-mode ()
  (use-package asm-mode
    :commands (asm-mode)
    :config
    (progn
      (add-hook 'asm-mode-hook (lambda ()
                                 (setq indent-tabs-mode nil) ; use spaces to indent
                                 (setq tab-stop-list (number-sequence 2 60 2)) ; 2 spaces per tab
                                 (electric-indent-mode -1)
                                 (company-mode 1)))
      (define-key asm-mode-map (kbd "C-j") 'newline)
      ;; when we press ':' character, it runs `asm-colon' command in asm-mode.
      ;; The command automatically removes any indentation, since every
      ;; non-whitespace character before a colon is a label in asm, and label
      ;; has to be at the beginning of a line. However, the problem is that when
      ;; deleting indentation, trailing spaces are left between the colon and
      ;; point.
      ;;
      ;; These functions solve that problem. First, check whether we have any
      ;; space or tab after point. If so, don't do anything becuase the spaces are
      ;; there intentionally. If not, we delete all trailing spaces between
      ;; point and colon.
      (defvar asm-colon-has-space nil)
      (defun asm-colon-check-space ()
        (setq asm-colon-has-space nil)
        (when (member (let ((c (char-after)))
                        (if c
                            (string c)
                          ""))
                      '(" " "\t"))
          (setq asm-colon-has-space t)))
      (defun asm-colon-delete-spaces ()
        (unless asm-colon-has-space
          (call-interactively 'delete-horizontal-space)))
      (advice-add 'asm-colon :before 'asm-colon-check-space)
      (advice-add 'asm-colon :after 'asm-colon-delete-spaces)
      (define-key asm-mode-map (kbd "M-.") 'helm-etags-select)
      (define-key asm-mode-map (kbd "<backtab>") 'company-complete))))

;; (defun tuhdo/init-csharp-mode ()
;;   (use-package csharp-mode
;;     :defer t
;;     :init
;;     (progn
;;       (add-to-list 'load-path (concat user-emacs-directory "private/tuhdo/extensions/"))
;;       (use-package wisent-csharp
;;         :defer t
;;         :init
;;         (add-hook 'csharp-mode-hook (lambda ()
;;                                       (require 'wisent-csharp)
;;                                       (wisent-csharp-default-setup)
;;                                       (semantic-mode)
;;                                       (flycheck-mode)))))))

;; (defun tuhdo/init-backward-forward ()
;;   (use-package backward-forward
;;     :init
;;     ;; (backward-forward-mode 1)
;;     (define-key evil-normal-state-map (kbd "C-i") 'backward-forward-previous-location)
;;     (define-key evil-normal-state-map (kbd "C-o") 'backward-forward-next-location)))

;; (defun tuhdo/init-caps-lock()
;;   (use-package caps-lock))

;; (defun tuhdo/init-vimish-fold ()
;;   (use-package vimish-fold
;;     :init
;;     (spacemacs/set-leader-keys
;;       "zv" 'vimish-fold
;;       "zm" 'my-fold-braces
;;       "zn" 'vimish-fold-refold
;;       "zb" 'vimish-fold-unfold))
;;   )

;; (defun tuhdo/post-init-neotree ()
;;   (with-eval-after-load 'neotree
;;     (define-key neotree-mode-map (kbd "u") 'neotree-select-up-node)
;;     (define-key neotree-mode-map (kbd "j") 'spacemacs/neotree-collapse-or-up)
;;     (define-key neotree-mode-map (kbd "l") 'spacemacs/neotree-expand-or-open)
;;     (define-key neotree-mode-map (kbd "i") 'neotree-previous-line)
;;     (define-key neotree-mode-map (kbd "k") 'neotree-next-line)
;;     (add-hook 'neotree-mode-hook 'evil-emacs-state))
;;   )


(defun tuhdo/init-yankpad ()
  (use-package yankpad
    :defer t
    :init
    (setq yankpad-file "~/Documents/wiki/C++.org")
    :config
    ;; (bind-key "<f7>" 'yankpad-map)
    (bind-key "C-f" 'yankpad-expand)
    ;; If you want to complete snippets using company-mode
    )
  )

;; (defun tuhdo/post-init-auto-yasnippet ()
;;   (setq aya-persist-snippets-dir (concat user-emacs-directory "private/tuhdo/snippets/"))
;;   (evil-leader/set-key
;;     "yy" 'aya-create
;;     "ye" 'aya-expand
;;     "ys" 'aya-persist-snippet))

(defun tuhdo/post-init-flycheck ()
  ;; (add-hook 'c-mode-hook (lambda ()
  ;;                          (flycheck-select-checker 'c/c++-gcc)))
  (flycheck-add-mode 'c/c++-cppcheck 'c++-mode)
  (flycheck-add-mode 'c/c++-clang 'c++-mode)
  (add-hook 'c++-mode-hook (lambda ()
                             (setq flycheck-clang-language-standard "c++11"))))

;; (defun tuhdo/init-flyspell-lazy ()
;;   (use-package flyspell-lazy
;;     :init
;;     (add-hook 'prog-mode-hook 'flyspell-lazy-mode))
;;   )

(defun tuhdo/pre-init-graphviz-dot-mode ()
  (setq default-tab-width 4)
  )

;; (defun tuhdo/init-folding ()
;;   (use-package folding
;;     :init
;;     (add-hook 'prog-mode-hook 'folding-mode)
;;     (setq folding-default-keys-function 'folding-bind-backward-compatible-keys)
;;     (setq folding-mode-prefix-key nil)
;;     (setq folding-goto-key "")
;;     ))

;; (defun tuhdo/init-firestarter ()
;;   (use-package firestarter))

;; (defun tuhdo/init-isearch-dabbrev ()
;;   (use-package isearch-dabbrev
;;     :init
;;     (define-key isearch-mode-map (kbd "M-/") 'isearch-dabbrev-expand)
;;     (define-key isearch-mode-map (kbd "TAB") 'isearch-dabbrev-expand)))

;; (defun tuhdo/init-irony ()
;;   (use-package irony
;;     :defer t
;;     :init
;;     (defun tuhdo/enable-irony ()
;;       (irony-mode 1)
;;       (semantic-idle-summary-mode -1)
;;       (add-to-list 'company-backends 'company-irony)
;;       (when (featurep 'flycheck)
;;         (flycheck-irony-setup)))

;;     (setq w32-pipe-read-delay 0)

;;     (add-hook 'c++-mode-hook 'tuhdo/enable-irony)
;;     (add-hook 'c-mode-hook 'tuhdo/enable-irony)
;;     (add-hook 'objc-mode-hook 'tuhdo/enable-irony)
;;     ;; replace the `completion-at-point' and `complete-symbol' bindings in
;;     ;; irony-mode's buffers by irony-mode's function
;;     (defun my-irony-mode-hook ()
;;       (define-key irony-mode-map [remap completion-at-point]
;;         'irony-completion-at-point-async)
;;       (define-key irony-mode-map [remap complete-symbol]
;;         'irony-completion-at-point-async))
;;     (add-hook 'irony-mode-hook 'my-irony-mode-hook)
;;     (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)))

;; (defun tuhdo/init-irony-eldoc ()
;;   (use-package irony-eldoc
;;     :defer t
;;     :init
;;     (add-hook 'c-mode-hook 'irony-eldoc)
;;     (add-hook 'c++-mode-hook 'irony-eldoc)))

;; (defun tuhdo/init-company-irony ()
;;   (use-package company-irony
;;     :defer t
;;     :init
;;     (add-hook 'irony-mode-hook 'company-irony-setup-begin-commands)))

;; (defun tuhdo/init-company-irony-c-headers ()
;;   (use-package company-irony-c-headers
;;     :defer t))

;; (defun tuhdo/init-flycheck-irony ()
;;   (use-package flycheck-irony
;;     :defer t))

;; (defun tuhdo/init-sx ()
;;   ;; (use-package sx
;;   ;;   :defer t
;;   ;;   :init
;;   ;;   (evil-leader/set-key
;;   ;;     "oss" 'sx-search
;;   ;;     "osg" 'sx-tab-all-questions
;;   ;;     "osi" 'sx-inbox
;;   ;;     "osa" 'sx-ask))
;;   )

(defun tuhdo/post-init-org ()
  (global-unset-key (kbd "C-c a"))
  (global-unset-key (kbd "C-c c"))
  (spacemacs/set-leader-keys
    "oa" 'org-agenda
    "oc" 'org-capture
    "or" 'org-refile
    "os" 'org-store-link)
  (with-eval-after-load 'org
    (define-key org-mode-map (kbd "C-<") 'org-begin-template)
    ;; (use-package org-pomodoro
    ;;   :config
    ;;   (setq org-pomodoro-length 30)
    ;;   (setq org-pomodoro-short-break-length 2)
    ;;   (setq org-pomodoro-audio-player "mplayer")
    ;;   (setq org-pomodoro-finished-sound-args "-volume 0.3")
    ;;   (setq org-pomodoro-long-break-sound-args "-volume 0.3")
    ;;   (setq org-pomodoro-short-break-sound-args "-volume 0.3"))

    ;; (use-package org-journal
    ;;   :bind (("C-c C-j" . org-journal))
    ;;   :init
    ;;   (setq org-journal-dir "~/Dropbox_home/journal/daily_logs/"))

    ;; (setq org-ellipsis " ▼ ")
    ;; (require 'org-drill)
    ;; (require 'org-depend)
    ;; (require 'ox-beamer)
    ;; (require 'org-habit)

    (add-to-list 'auto-mode-alist '("\\`[^.].*\\.org'\\|^.[0-9]+" . org-mode))
    (add-hook 'org-mode-hook 'auto-fill-mode)
    (add-hook 'org-mode-hook (lambda () (setq fill-column 80)))
    (define-key org-mode-map (kbd "C-c j") 'helm-org-in-buffer-headings)

    ;; (global-set-key (kbd "C-c a") 'org-agenda)
    ;; (global-set-key (kbd "C-c l") 'org-store-link)

    ;; (global-set-key "\C-cb" 'org-iswitchb)
    (setq org-log-done t)

    (add-hook 'org-mode-hook 'org-indent-mode)

    ;; (setq org-default-notes-file (concat org-directory "~/org-data/tasks.org"))
    (defvar custom--org-project-file-path "~/org-data/")

    ;; Capture templates for: TODO tasks, Notes, appointments, phone calls, meetings, and org-protocol
    (setq org-capture-templates
          (quote (("t" "todo" entry (file "~/org-data/refile.org")
                   "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
                  ("l" "link" entry (file "~/org-data/refile.org")
                   "* %?\n%A\n" :clock-in t :clock-resume t)
                  ("r" "respond" entry (file "~/org-data/refile.org")
                   "* NEXT Respond to %:from on %:subject\nSCHEDULED: %t\n%U\n%a\n" :clock-in t :clock-resume t :immediate-finish t)
                  ("n" "note" entry (file "~/org-data/refile.org")
                   "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
                  ("j" "Journal" entry (file+datetree "~/org-data/diary.org")
                   "* %?\n%U\n" :clock-in t :clock-resume t)
                  ("w" "org-protocol" entry (file "~/org-data/refile.org")
                   "* TODO Review %c\n%U\n" :immediate-finish t)
                  ("m" "Meeting" entry (file "~/org-data/refile.org")
                   "* MEETING with %? :MEETING:\n%U" :clock-in t :clock-resume t)
                  ("p" "Phone call" entry (file "~/org-data/refile.org")
                   "* PHONE %? :PHONE:\n%U" :clock-in t :clock-resume t)
                  ("h" "Habit" entry (file "~/org-data/refile.org")
                   "* NEXT %?\n%U\n%a\nSCHEDULED: %(format-time-string \"<%Y-%m-%d %a .+1d/3d>\")\n:PROPERTIES:\n:STYLE: habit\n:REPEAT_TO_STATE: NEXT\n:END:\n"))))

    (setq org-clock-persist 'history)
    (org-clock-persistence-insinuate)
    (setq org-replace-disputed-keys nil)

    (org-babel-do-load-languages
     'org-babel-load-languages
     '((sh . t)
       (lisp . t)))

    (setq org-agenda-files (list
                            "~/Documents/wiki/tasks.org"
                            "~/Dropbox_home/journal/tasks.org"
                            "~/Dropbox_home/books/todos.org"
                            ;; "~/Dropbox_home/journal/daily_logs/"
                            ))
    ;; (setq org-agenda-file-regexp "\\`[^.].*\\.org'\\|[0-9]+")
    (add-hook 'org-mode-hook
              (lambda ()
                (local-set-key "\M-\C-n" 'outline-next-visible-heading)
                (local-set-key "\M-\C-p" 'outline-previous-visible-heading)
                (local-set-key "\M-\C-u" 'outline-up-heading)
                ;; table
                (local-set-key "\M-\C-w" 'org-table-copy-region)
                (local-set-key "\M-\C-y" 'org-table-paste-rectangle)
                (local-set-key "\M-\C-l" 'org-table-sort-lines)
                ;; display images
                (local-set-key "\M-I" 'org-toggle-iimage-in-org)))

    (setq org-use-speed-commands t)
    (setq org-confirm-babel-evaluate nil)
    (setq org-src-fontify-natively nil)
    (setq org-src-tab-acts-natively t)

    (setq org-todo-keywords
          (quote ((sequence "TODO(t)" "NEXT(n)" "PROGRESS(p)" "|" "DONE(d)")
                  (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "CANCELLED(c@/!)" "PHONE" "MEETING"))))

    (setq org-todo-keyword-faces
          (quote (("TODO" :foreground "red" :weight bold)
                  ("NEXT" :foreground "blue" :weight bold)
                  ("PROGRESS" :foreground "dark blue" :weight bold)
                  ("DONE" :foreground "forest green" :weight bold)
                  ("HOLD" :foreground "magenta" :weight bold)
                  ("WAITING" :foreground "orange" :weight bold)
                  ("CANCELLED" :foreground "forest green" :weight bold)
                  ("MEETING" :foreground "forest green" :weight bold)
                  ("PHONE" :foreground "forest green" :weight bold))))

    ;; ,----
    ;; | Refilling Tasks
    ;; `----
    ;; Targets include this file and any file contributing to the agenda - up to 9 levels deep
    (setq org-refile-targets (quote ((nil :maxlevel . 9)
                                     (org-agenda-files :maxlevel . 9))))

    ;; Use full outline paths for refile targets - we file directly with IDO
    (setq org-refile-use-outline-path t)

    ;; Targets complete directly with IDO
    (setq org-outline-path-complete-in-steps nil)

    ;; Allow refile to create parent tasks with confirmation
    (setq org-refile-allow-creating-parent-nodes (quote confirm))

    ;; Use IDO for both buffer and file completion and ido-everywhere to t
    ;; (setq org-completion-use-ido t)
    ;; (setq ido-everywhere t)
    ;; (setq ido-max-directory-size 100000)
    ;; (ido-mode (quote both))
    ;; Use the current window when visiting files and buffers with ido
    ;; (setq ido-default-file-method 'selected-window)
    ;; (setq ido-default-buffer-method 'selected-window)
    ;; Use the current window for indirect buffer display
    ;; (setq org-indirect-buffer-display 'current-window)

;;;; Refile settings
    ;; Exclude DONE state tasks from refile targets
    (setq org-refile-target-verify-function 'bh/verify-refile-target)

    ;; ,----
    ;; | Agenda setup
    ;; `----
    ;; Do not dim blocked tasks
    (setq org-agenda-dim-blocked-tasks nil)

    ;; Compact the block agenda view
    (setq org-agenda-compact-blocks t)

    ;; Custom agenda command definitions
    (setq org-agenda-custom-commands
          (quote (("N" "Notes" tags "NOTE"
                   ((org-agenda-overriding-header "Notes")
                    (org-tags-match-list-sublevels t)))
                  ("h" "Habits" tags-todo "STYLE=\"habit\""
                   ((org-agenda-overriding-header "Habits")
                    (org-agenda-sorting-strategy
                     '(todo-state-down effort-up category-keep))))
                  (" " "Agenda"
                   ((agenda "" nil)
                    (tags "REFILE"
                          ((org-agenda-overriding-header "Tasks to Refile")
                           (org-tags-match-list-sublevels nil)))
                    (tags-todo "-CANCELLED/!"
                               ((org-agenda-overriding-header "Stuck Projects")
                                (org-agenda-skip-function 'bh/skip-non-stuck-projects)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-HOLD-CANCELLED/!"
                               ((org-agenda-overriding-header "Projects")
                                (org-agenda-skip-function 'bh/skip-non-projects)
                                (org-tags-match-list-sublevels 'indented)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-CANCELLED/!NEXT"
                               ((org-agenda-overriding-header (concat "Project Next Tasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-projects-and-habits-and-single-tasks)
                                (org-tags-match-list-sublevels t)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(todo-state-down effort-up category-keep))))
                    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                               ((org-agenda-overriding-header (concat "Project Subtasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-non-project-tasks)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-REFILE-CANCELLED-WAITING-HOLD/!"
                               ((org-agenda-overriding-header (concat "Standalone Tasks"
                                                                      (if bh/hide-scheduled-and-waiting-next-tasks
                                                                          ""
                                                                        " (including WAITING and SCHEDULED tasks)")))
                                (org-agenda-skip-function 'bh/skip-project-tasks)
                                (org-agenda-todo-ignore-scheduled bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-deadlines bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-todo-ignore-with-date bh/hide-scheduled-and-waiting-next-tasks)
                                (org-agenda-sorting-strategy
                                 '(category-keep))))
                    (tags-todo "-CANCELLED+WAITING|HOLD/!"
                               ((org-agenda-overriding-header "Waiting and Postponed Tasks")
                                (org-agenda-skip-function 'bh/skip-stuck-projects)
                                (org-tags-match-list-sublevels nil)
                                (org-agenda-todo-ignore-scheduled t)
                                (org-agenda-todo-ignore-deadlines t)))
                    (tags "-REFILE/"
                          ((org-agenda-overriding-header "Tasks to Archive")
                           (org-agenda-skip-function 'bh/skip-non-archivable-tasks)
                           (org-tags-match-list-sublevels nil))))
                   nil))))

    (add-hook 'org-mode-hook (lambda () (semantic-mode -1)))
    (add-hook 'org-mode-hook 'flyspell-mode)

    ;; Make windmove work in org-mode:
    (add-hook 'org-shiftup-final-hook 'windmove-up)
    (add-hook 'org-shiftleft-final-hook 'windmove-left)
    (add-hook 'org-shiftdown-final-hook 'windmove-down)
    (add-hook 'org-shiftright-final-hook 'windmove-right)

    ;; (add-hook 'org-mode-hook
    ;;           (lambda ()
    ;;             (add-hook 'before-save-hook 'my/org-add-ids-to-headlines-in-file nil 'local)))

    ;; (setq org-hide-emphasis-markers t)

    (require 'ox-html)
    ;; (require 'ox-rs)			;
    (require 'ox-publish)

    (setq org-publish-project-alist
          '(("blog-content"
             :base-directory "~/Public/emacs-tutor/emacs-tutor/"
             :html-extension "html"
             :base-extension "org"
             :publishing-directory "~/Public/emacs-tutor/"
             :publishing-function (org-html-publish-to-html)
             :recursive t               ; descend into sub-folders?
             :section-numbers nil       ; don't create numbered sections
             :with-toc t                ; don't create a table of contents
             :with-latex t              ; do use MathJax for awesome formulas!
             :html-preamble
             (lambda (info)
               (concat "<!-- Start of StatCounter Code for Default Guide -->
<script type=\"text/javascript\">
var sc_project=9874755;
var sc_invisible=1;
var sc_security=\"c2028bb7\";
var scJsHost = ((\"https:\" == document.location.protocol) ?
\"https://secure.\" : \"http://www.\");
document.write(\"<sc\"+\"ript type='text/javascript' src='\" + scJsHost+
\"statcounter.com/counter/counter.js'></\"+\"script>\");
</script>
<noscript><div class=\"statcounter\"><a title=\"hit counter\"
href=\"http://statcounter.com/free-hit-counter/\" target=\"_blank\"><img
class=\"statcounter\" src=\"http://c.statcounter.com/9874755/0/c2028bb7/1/\"
alt=\"hit counter\"></a></div></noscript>
<!-- End of StatCounter Code for Default Guide -->"
                (cond ((and (listp (plist-get info :title))
                            (or (string= (car (plist-get info :title)) "Table of Contents")
                                (string= (car (plist-get info :title)) "Contents")))
                       "")
                      (t "
<h2><a href=\"index.html\">Back to Table of Contents</a></h2>
"))))                                   ; this stuff is put before your post
             :html-postamble
             (lambda (info)
               (cond ((and (listp (plist-get info :title))
                           (or (string= (car (plist-get info :title)) "Table of Contents")
                               (string= (car (plist-get info :title)) "Contents")))
                      "")
                     (t "
    <div id=\"disqus_thread\"></div>
    <script type=\"text/javascript\">
  /* * * CONFIGURATION VARIABLES: EDIT BEFORE PASTING INTO YOUR WEBPAGE * * */
  var disqus_shortname = 'emacs-tutor'; // required: replace example with your forum shortname

  /* * * DON'T EDIT BELOW THIS LINE * * */
  (function() {
      var dsq = document.createElement('script'); dsq.type = 'text/javascript'; dsq.async = true;
      dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
      (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
  })();
    </script>
    <noscript>Please enable JavaScript to view the <a href=\"http://disqus.com/?ref_noscript\">comments powered by Disqus.</a></noscript>
    <a href=\"http://disqus.com\" class=\"dsq-brlink\">comments powered by <span class=\"logo-disqus\">Disqus</span></a>
")
                     ;; (t "<div id=\"archive\"><a href=\"index.html\">index</a></div>")
                     ))
             :html-head-extra "<link rel=\"stylesheet\" href=\"./static/worg.css\">"
             ;; :auto-sitemap t
             ;; :sitemap-filename "index.org"
             ;; :sitemap-title "Table of Content"
             ;; :sitemap-sort-files chronologically
             ;; :sitemap-style list
             ;; :makeindex t
             :html-head-extra "<link rel=\"stylesheet\" href=\"./static/worg.css\">")
            ("blog-static"
             :base-directory "~/Public/emacs-tutor/emacs-tutor/static/"
             :base-extension "gif\\|png\\|jpg\\|css"
             :publishing-directory "~/Public/emacs-tutor/static"
             :recursive t
             :publishing-function org-publish-attachment)
            ("blog"
             :components ("blog-content" "blog-static"))))

    (setcar (nthcdr 2 org-emphasis-regexp-components) " \t\r\n\"'")
    (org-set-emph-re 'org-emphasis-regexp-components org-emphasis-regexp-components)

    (setq org-use-speed-commands t)
    (add-to-list 'org-structure-template-alist '("n" "#+NAME:"))))

(defun tuhdo/post-init-omnisharp ()
  (setq omnisharp-server-executable-path "/usr/local/bin/omnisharp"))

(defun tuhdo/init-pdf-tools ()
  (use-package pdf-tools
    :init
    (pdf-tools-install)))

(defun tuhdo/post-init-popwin ()
  (push '(".*wiki-summary.*"
          :regexp t
          :dedicated nil
          :position bottom
          :stick nil
          :noselect nil
          :height 0.4)
        popwin:special-display-config)

  (defun tuhdo/popwin-local-key ()
    (local-set-key (kbd "C-g") (lambda ()
                                 (interactive)
                                 (cond
                                  ((string-match "wiki-summary" (buffer-name))
                                   (kill-buffer-and-window))
                                  (t (call-interactively 'keyboard-quit))))))
  (add-hook 'popwin:after-popup-hook #'tuhdo/popwin-local-key))

;; (defun tuhdo/init-prolog ()
;;   (use-package prolog
;;     :defer t
;;     :init
;;     (setq prolog-system 'swi)
;;     (setq auto-mode-alist (append '(("\\.pl$" . prolog-mode)
;;                                     ("\\.m$" . mercury-mode))
;;                                   auto-mode-alist)))
;;   )

;; (defun tuhdo/init-pamparam ()
;;   (use-package pamparam
;;     :defer t
;;     :config
;;     (use-package worf)
;;     :bind (("C-c s s" . pam-sync)
;;            ("C-c s p" . pam-pull)
;;            ("C-c s d" . pam-drill))
;;     ))

(defun tuhdo/init-pomidor ()
  (use-package pomidor
    :bind (("<f12>" . pomidor))))

;; (defun tuhdo/post-init-semantic ()
;;   (add-hook 'java-mode-hook 'semantic-mode)
;;   (eval-after-load "semantic"
;;     (lambda ()
;;       (semantic-add-system-include "c:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/um/." 'c-mode)
;;       (semantic-add-system-include "c:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/ucrt" 'c-mode)
;;       (semantic-add-system-include "~/workspace/linux/kernel" 'c-mode)
;;       (semantic-add-system-include "~/workspace/linux/include" 'c-mode)
;;       (semantic-add-system-include "c:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/um/." 'c++-mode)
;;       (semantic-add-system-include "c:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/ucrt/" 'c++-mode)
;;       (semantic-add-system-include "c:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/winrt/" 'c++-mode)
;;       (semantic-add-system-include "c:/Program Files (x86)/Windows Kits/10/Include/10.0.10586.0/shared/" 'c++-mode)
;;       (semantic-add-system-include "~/workspace/linux/kernel" 'c++-mode)
;;       (semantic-add-system-include "~/workspace/linux/include" 'c++-mode)))

;;   (setq semantic-idle-scheduler-max-buffer-size 2000000))

;; (defun tuhdo/init-quickrun ()
;;   (use-package quickrun
;;     :defer t
;;     :init
;;     (defun run-tcframe-and-test()
;;       (interactive)
;;       (compile "tcframe build && ./runner"))
;;     (evil-leader/set-key
;;       "tq" 'quickrun-autorun-mode
;;       ;; "oR" 'quickrun-with-arg
;;       ;; "or" 'quickrun
;;       )
;;     (global-set-key (kbd "<f5>") 'quickrun-with-arg)
;;     (global-set-key (kbd "S-<f5>") 'quickrun-with-shell)
;;     (global-set-key (kbd "<f6>") 'quickrun-compile-only)
;;     ;; (with-eval-after-load 'cc-mode
;;     ;;   (exec-path-from-shell-copy-env "TCFRAME_HOME")
;;     ;;   (define-key c++-mode-map (kbd "<f7>") 'run-tcframe-and-test))
;;     (defun quick-run-multiples-redirect (buf-name &rest args-list)
;;       (let* ((cur-buf (if buf-name (get-buffer buf-name) (current-buffer)))
;;              (cur-window (get-buffer-window cur-buf))
;;              (result))
;;         (dolist (arg (hif-flatten args-list))
;;           (with-current-buffer cur-buf
;;             (select-window cur-window)
;;             (delete-other-windows)
;;             (select-window cur-window)
;;             (quickrun-with-arg (concat "< " arg))
;;             (sit-for 1)
;;             (with-current-buffer quickrun--buffer-name
;;               (setq result (concat result (propertize arg 'face 'bold) ": \n"  (buffer-string))))))
;;         (with-current-buffer (generate-new-buffer quickrun--buffer-name)
;;           (erase-buffer)
;;           (insert result)
;;           (quickrun--mode)
;;           (switch-to-buffer (current-buffer)))))
;;     :config
;;     (progn
;;       ;; Use this parameter as C++ default
;;       (quickrun-add-command "d"
;;                             `((:command . "dmd")
;;                               (:exec    . ("%c %o -oftmp-d.exe %s" "tmp-d.exe"))
;;                               (:description . "Compile D language file and execute"))
;;                             )
;;       (quickrun-add-command "c++11"
;;                             '((:command . "g++")
;;                               (:exec    . ("%c -std=c++11 %o -o %e %s" "%e %a"))
;;                               (:compile-only . "%c -std=c++11 -Wall -Werror %o -o solution %s")
;;                               (:description . "Compile C++ file with g++ and execute")
;;                               (:remove "%e"))
;;                             :default "c++")))
;;   )

;; (defun tuhdo/init-wiki-summary ()
;;   (use-package wiki-summary
;;     :defer t
;;     :init
;;     (evil-leader/set-key
;;       "ow" 'wiki-summary)))

(defun tuhdo/init-shell-pop ()
  (use-package shell-pop
    :bind (("C-c '" . shell-pop))
    :config
    (setq shell-pop-window-position "bottom"
          shell-pop-window-size     30
          shell-pop-full-span       t)
    (shell-pop--set-shell-type 'shell-pop-shell-type
                               '("eshell" "*eshell*" (lambda nil  (eshell))))))

(defun tuhdo/init-eshell-prompt-extras ()
  (use-package eshell-prompt-extras
    :commands epe-theme-lambda
    :init
    (setq eshell-highlight-prompt nil
          eshell-prompt-function 'epe-theme-lambda)
    :config
    (require 'cl)))

;; (defun tuhdo/post-init-slime ()
;;   (add-hook 'slime-mode-hook #'smartparens-mode)

;;   (defvar slime-repl-font-lock-keywords lisp-font-lock-keywords-2)
;;   (defun slime-repl-font-lock-setup ()
;;     (setq font-lock-defaults
;;           '(slime-repl-font-lock-keywords
;;             ;; From lisp-mode.el
;;             nil nil (("+-*/.<>=!?$%_&~^:@" . "w")) nil
;;             (font-lock-syntactic-face-function
;;              . lisp-font-lock-syntactic-face-function))))

;;   (add-hook 'slime-repl-mode-hook 'slime-repl-font-lock-setup)

;;   (defadvice slime-repl-insert-prompt (after font-lock-face activate)
;;     (let ((inhibit-read-only t))
;;       (add-text-properties
;;        slime-repl-prompt-start-mark (point)
;;        '(font-lock-face
;;          slime-repl-prompt-face
;;          rear-nonsticky
;;          (slime-repl-prompt read-only font-lock-face intangible)))))

;;   (defadvice eww-render (around eww-render-popwin activate)
;;     (save-window-excursion ad-do-it)
;;     (unless (get-buffer-window "*eww*")
;;       (pop-to-buffer "*eww*")))

;;   (defun eww-slime-hyperspec-lookup (symbol-name)
;;     (interactive (list (common-lisp-hyperspec-read-symbol-name
;;                         (slime-symbol-at-point))))
;;     (let ((browse-url-browser-function 'eww-browse-url)
;;           (popwin:special-display-config nil))
;;       (push '(eww-mode :position bottom :noselect t :height 0.5) popwin:special-display-config)
;;       (hyperspec-lookup symbol-name)))

;;   (setq common-lisp-hyperspec-root (concat "file://" (file-truename user-emacs-directory) "private/tuhdo/HyperSpec/"))
;;   (add-hook 'lisp-mode-hook (lambda ()
;;                               (semantic-default-elisp-setup)
;;                               (semantic-mode 1)
;;                               (semantic-idle-summary-mode -1)))

;;   (with-eval-after-load 'slime
;;     (define-key slime-repl-mode-map (kbd "M-m h d") 'eww-slime-hyperspec-lookup)
;;     (define-key slime-mode-map (kbd "M-m h d") 'eww-slime-hyperspec-lookup)
;;     (use-package slime-company
;;       :init
;;       (slime-setup '(slime-company))
;;       (setq slime-company-completion 'fuzzy))
;;     (when (featurep 'lispy)
;;       (define-key slime-mode-map (kbd "C-M-SPC") 'special-lispy-space))))

(defun tuhdo/init-syntax-subword ()
  (use-package syntax-subword
    :init
    (global-syntax-subword-mode)))

;; (defun tuhdo/init-slime-company ())

;; (defun tuhdo/init-x86-lookup ()
;;   (use-package x86-lookup
;;     :init
;;     (setq x86-lookup-pdf "~/Downloads/64-ia-32-architectures-software-developer-instruction-set-reference-manual-325383.pdf")
;;     (with-eval-after-load 'asm-mode
;;       (define-key asm-mode-map [remap zeal-at-point] 'x86-lookup))
;;     :config
;;     (progn
;;       (when (package-installed-p 'pdf-tools)
;;         (defun x86-lookup-browse-pdf-view-emacs (pdf page)
;;           "View PDF at PAGE using Emacs' `doc-view-mode' and `display-buffer'."
;;           (with-selected-window (display-buffer (find-file-noselect pdf :nowarn))
;;             (pdf-view-goto-page page)))
;;         (setq x86-lookup-browse-pdf-function 'x86-lookup-browse-pdf-view-emacs)))))

;; (defun tuhdo/init-goto-chg ()
;;   (use-package goto-chg
;;     :init
;;     (evil-leader/set-key
;;       "<" 'goto-last-change
;;       ">" 'goto-last-change-reverse)))

;; (defun tuhdo/init-god-mode ()
;;   (use-package god-mode
;;     :init
;;     (add-hook 'prog-mode-hook 'god-mode)
;;     (add-hook 'git-commit-mode-hook 'god-mode)
;;     (add-hook 'text-mode-hook 'god-mode)
;;     (require 'god-mode-isearch)
;;     (defun my-god-mode-self-insert ()
;;       (interactive)
;;       (if (and (bolp)
;;                (eq major-mode 'org-mode))
;;           (call-interactively 'org-self-insert-command)
;;         (call-interactively 'god-mode-self-insert)))
;;     (defun my-update-cursor ()
;;       (setq cursor-type (if (or god-local-mode buffer-read-only)
;;                             'box
;;                           'bar)))
;;     (add-hook 'god-mode-enabled-hook 'my-update-cursor)
;;     (add-hook 'god-mode-disabled-hook 'my-update-cursor)
;;     :config
;;     (define-key god-local-mode-map (kbd "z") 'repeat)
;;     (define-key god-local-mode-map (kbd ".") 'repeat)
;;     (define-key god-local-mode-map (kbd "i") 'god-local-mode)
;;     (global-set-key (kbd "<escape>") 'god-local-mode)
;;     (global-set-key (kbd "C-x C-1") 'delete-other-windows)
;;     (global-set-key (kbd "C-x C-2") 'split-window-below)
;;     (global-set-key (kbd "C-x C-3") 'split-window-right)
;;     (global-set-key (kbd "C-x C-0") 'delete-window)
;;     (define-key isearch-mode-map (kbd "<escape>") 'god-mode-isearch-activate)
;;     (define-key god-mode-isearch-map (kbd "<escape>") 'god-mode-isearch-disable)
;;     (define-key god-local-mode-map [remap self-insert-command] 'my-god-mode-self-insert)))

;; (defun tuhdo/post-init-nasm-mode ()
;;   (with-eval-after-load 'asm-mode
;;     (define-key nasm-mode-map (kbd "C-c d .") 'x86-lookup)))

;; (defun tuhdo/init-jdee ()
;;   (use-package java-mode
;;     :defer t
;;     :init
;;     (add-to-list 'load-path "/usr/local/jdee/lisp/")
;;     (setq bsh-jar "/usr/local/jdee/java/lib/bsh.jar")
;;     (setq jde-complete-function 'jde-complete-minibuf)
;;     (with-eval-after-load 'jde
;;       (define-key jde-mode-map (kbd "TAB") 'jde-complete))
;;     (add-hook 'java-mode-hook (lambda ()
;;                                 (unless (featurep 'jde)
;;                                   (condition-case nil
;;                                       (progn
;;                                         (require 'jde)
;;                                         (jde-mode))
;;                                     (error nil)))))))

;; (defun tuhdo/init-verilog-mode ()
;;   (use-package verilog-mode
;;     :defer t
;;     :init
;;     (progn
;;       (with-eval-after-load 'verilog-mode
;;         (with-eval-after-load 'company-keywords
;;           (setq verilog-auto-newline nil)
;;           (add-to-list 'company-keywords-alist (cons 'verilog-mode verilog-keywords))))

;;       (defun verilog-compile ()
;;         (interactive)
;;         (setq verilog-compiler (concat "iverilog -o " (file-name-base (buffer-file-name)) " "))
;;         (setq verilog-tool 'verilog-compiler)
;;         (verilog-set-compile-command)
;;         (verilog-auto-save-compile))

;;       (defun verilog-simulator ()
;;         (interactive)
;;         (setq verilog-simulator (concat "vvp ./" (file-name-base (buffer-file-name)) " "))
;;         (async-shell-command verilog-simulator))

;;       (defun verilog-gtkwave ()
;;         (interactive)
;;         (async-shell-command (concat "gtkwave ./" (file-name-base (buffer-file-name)) ".vcd")))

;;       (evil-leader/set-key-for-mode 'verilog-mode
;;         "mc" 'verilog-compile
;;         "ms" 'verilog-simulator
;;         "mv" 'verilog-gtkwave)

;;       (push 'company-capf company-backends-verilog-mode)
;;       (add-hook 'verilog-mode-hook 'flycheck-mode)
;;       (spacemacs|add-company-hook verilog-mode))))

(defun tuhdo/init-org-wiki ()
  (use-package org-wiki
    :bind (("C-c o p" . org-wiki-panel)
           ("C-c o w" . org-wiki-index)
           ("C-c o l" . org-wiki-link)
           ("C-c o i" . org-wiki-insert)
           ("C-c o h" . org-wiki-header)
           ("C-c o j" . org-wiki-helm)
           ("C-c o d" . org-wiki-dired)
           ("C-c o D" . org-wiki-dired-all)
           ("C-c o A" . org-wiki-asset-insert))
    :config
    (setq org-wiki-location "~/Documents/wiki")))

(defun tuhdo/init-org-recipes ()
  (use-package org-recipes
    :bind (("C-c o r" . org-recipes)
           ("C-c o f" . org-recipes-dwim)
           )
    :config
    (use-package org-wiki)))

(defun tuhdo/init-mmix-mode ()
  (use-package mmix-mode
    :defer t
    :init
    (autoload 'mmix-mode "mmix-mode" "Major mode for editing MMIX files" t)
    (setq auto-mode-alist (cons '("\\.mms" . mmix-mode)
                                auto-mode-alist))))

;; (defun tuhdo/init-wordsmith-mode ()
;;   (use-package wordsmith-mode
;;     :defer t))
(defun tuhdo/init-rebox2 ()
  (use-package rebox2
    :defer t
    :init
    (global-set-key [(meta q)] 'rebox-dwim)
    (global-set-key [(shift meta q)] 'rebox-cycle)
    (defun c-rebox-style ()
      (setq rebox-style-loop '(223 241 245)))
    (add-hook 'c-mode-hook #'c-rebox-style)
    (add-hook 'c++-mode-hook #'c-rebox-style)
    )
  )

(defun tuhdo/init-redo+ ()
  (use-package redo+))


;; (defun tuhdo/init-prettify-utils ()
;;   (use-package prettify-utils
;;     :init
;;     (defun my-init-prettify-symbols ()
;;       (setq prettify-symbols-alist (append prettify-symbols-alist
;;                                            (prettify-utils-generate
;;                                             ;; ("unless"  "⁈")
;;                                             ("if" "?")
;;                                             ;; ("while"  "")
;;                                             ;; ("until"  "")
;;                                             ("transform" " ⇒ ")
;;                                             ("mapc" " ⇒ ")
;;                                             ;; ("]["  ", ")
;;                                             ("exist_set" "s∃")
;;                                             ("exist_col" "c∃")
;;                                             ("exist_map" "m∃")
;;                                             ("exist_str" "str∃")
;;                                             ("else" "|⇒")
;;                                             ("else if" "|⇒ ?")
;;                                             ("repc" "ƪc")
;;                                             ("repi" "ƪi →")
;;                                             ("repj" "ƪj →")
;;                                             ("repk" "ƪk →")
;;                                             ("all" "∀")
;;                                             ("loop" "ƪ⊨")
;;                                             ("rep" "ƪ")
;;                                             ("return" "⮑")
;;                                             ))))
;;     (add-hook 'c++-mode-hook 'my-init-prettify-symbols)
;;     )
;;   )

(defun tuhdo/init-key-chord ()
  (use-package key-chord
    :init
    (key-chord-mode 1)
    (mapc
     (lambda (keyscommand)
       (key-chord-define-global
        (car keyscommand) (cdr keyscommand)))
     '(

     ;; (";\'" . helm-M-x)
     ("g7" . insert-and-op)
     ("j\\" . insert-or-op)
     ("2[" . insert-2sqr-brackets)
     ("3[" . insert-3sqr-brackets)
     ("4[" . insert-4sqr-brackets)
     ;; (";i" . insert-inf)
     ;; (";e" . insert-existency-symbols)
     ;; (";w" . insert-while)
     ;; (";c" . insert-if)
     ;; ("`j" . "~")
     ;; ("1j" . "!")
     ;; ("2j" . "@")
     ;; ("3j" . "#")
     ;; ("4j" . "$")
     ;; ("5j" . "%")
     ;; ("6f" . "^")
     ;; ("7f" . "&")
     ;; ("8f" . "*")
     ;; ("9f" . "(")
     ;; ("0f" . ")")
     ;; ("=j" . "+")
     ;; ("-f" . "_")
     ;; (";f" . ":")
     ;; ("[f" . "{")
     ;; ("]f" . "}")
     ;; ("/j" . "?");lo
     ;; ("\\j" . "|")
     )))
  )
